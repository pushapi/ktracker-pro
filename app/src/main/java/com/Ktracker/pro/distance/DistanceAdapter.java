package com.Ktracker.pro.distance;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;


import java.util.List;

import com.Ktracker.pro.R;
import com.Ktracker.pro.response.distance.DistanceResponse;

public class DistanceAdapter extends RecyclerView.Adapter<DistanceAdapter.ViewHolder> {
    Context context;
    List<DistanceResponse> itemList;
    DistanceActivity activity;


    public DistanceAdapter(Context context, List<DistanceResponse> itemList) {
        this.context = context;
        this.itemList = itemList;
        activity = (DistanceActivity) context;
    }



    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_list_distance_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {

        final DistanceResponse item = itemList.get(position);

        if(position==0)
        {
            holder.distance.setText("KM");
            holder.date.setText("Date");
            holder.linearLayoutHeader.setVisibility(View.VISIBLE);
            holder.linearLayout.setVisibility(View.GONE);
        }

      else if(position+1 == itemList.size())
      {
          holder.distance.setText(""+item.getTotal());
          holder.date.setText("Total");
          holder.linearLayoutHeader.setVisibility(View.GONE);
          holder.linearLayout.setVisibility(View.VISIBLE);
          changeColor(holder,context.getResources().getColor(R.color.colorAccent));
      }
      else
      {
          holder.linearLayoutHeader.setVisibility(View.GONE);
          holder.linearLayout.setVisibility(View.VISIBLE);
          holder.distance.setText(""+item.getKm());
          holder.date.setText(""+item.getDate());
          changeColor(holder,context.getResources().getColor(R.color.black));
      }




    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private TextView date,distance;
        private LinearLayout linearLayout,linearLayoutHeader;

        public ViewHolder(View itemView) {
            super(itemView);
            date = itemView.findViewById(R.id.date);
            distance = itemView.findViewById(R.id.distance);
            linearLayout = itemView.findViewById(R.id.linearLayout);
            linearLayoutHeader = itemView.findViewById(R.id.linearLayoutHeader);

        }

    }
    public void changeColor(ViewHolder holder, int color)

    {
        holder.distance.setTextColor(color);
        holder.date.setTextColor(color);



    }
}
