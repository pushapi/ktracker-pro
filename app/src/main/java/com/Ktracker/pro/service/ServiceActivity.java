package com.Ktracker.pro.service;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.Ktracker.pro.Constants;
import com.Ktracker.pro.Http.HttpCallback;
import com.Ktracker.pro.Http.HttpRequestSingleton;
import com.Ktracker.pro.Http.HttpRequestSingletonPostParams;
import com.Ktracker.pro.NetworkUtil;
import com.Ktracker.pro.R;
import com.Ktracker.pro.base.BaseActivity;
import com.Ktracker.pro.response.service.Datum;
import com.Ktracker.pro.response.service.ServiceResponse;
import com.Ktracker.pro.storage.SharedPreferenceUtil;
import com.android.volley.VolleyError;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

public class ServiceActivity extends BaseActivity {
    private RecyclerView recyclerViewList;
    private TextView noList;
    private LinearLayoutManager linearLayoutManager;
    private FloatingActionButton fab;
    private ServiceAdapter childAdapter;
    private Context context;
    private boolean edit=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service);
        setActionBar(true);
        setBackButtonVisibility(true);
        context = this;
        assignViews();
        setActionBarTitle("Service Book");


    }

    private void assignViews() {
        recyclerViewList = (RecyclerView) findViewById(R.id.recyclerViewList);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        noList = (TextView) findViewById(R.id.noList);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,AddServiceActivity.class);
                startActivity(intent);
            }
        });
    }

    private void setAdapter(List<Datum> childList) {
        linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        recyclerViewList.setLayoutManager(linearLayoutManager);
        recyclerViewList.setItemAnimator(new DefaultItemAnimator());
        childAdapter = new ServiceAdapter(context, childList);
        recyclerViewList.setAdapter(childAdapter);
        if(childList.size()>0)
        {
            noList.setVisibility(View.GONE);
        }
        else
        {
            noList.setVisibility(View.VISIBLE);
        }
    }

    private void callServiceApi() {
        if (NetworkUtil.isOnline(context)) {
            HashMap<String, String> params = new HashMap<>();
            params.put("show", SharedPreferenceUtil.getString(Constants.SP_USER_KEY, ""));
            params.put("cmd", "all");
            new HttpRequestSingleton(context, getString(R.string.api_base_url) + getString(R.string.api_service_api), params, 100, new HttpCallback() {
                @Override
                public void onResponse(String response, int action) {
                    if (response != null) {
                        Log.i("response", "" + response.toString());
                        switch (action) {
                            case 100:
                                ObjectMapper mapper = new ObjectMapper();
                                try {
                                    ServiceResponse vehicleResponse = mapper.readValue(response, ServiceResponse.class);
                                    setAdapter(vehicleResponse.getData());


                                } catch (JsonProcessingException e) {
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }

                                break;
                            default:
                        }

                    } else {
//                        Toast.makeText(context, "Something Went Wrong", Toast.LENGTH_LONG);
                    }
                }

                @Override
                public void onError(VolleyError error, int action) {

                }
            });

        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        callServiceApi();
    }
    public void confirmDialog(final String serviceNumber) {
        new AlertDialog.Builder(this)
                .setMessage("Are you sure you want to Delete?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        callDeleteApi(serviceNumber);
                    }
                })
                .setNegativeButton("No", null)
                .show();
    }


    public void callDeleteApi(String serviceNumber) {
        if (NetworkUtil.isOnline(context)) {
            showProgress("Loading...");
            HashMap<String, String> params = new HashMap<>();


            params.put("delete", serviceNumber);


            new HttpRequestSingletonPostParams(context, getString(R.string.api_base_url) + getString(R.string.api_service_api), params, 101, new HttpCallback() {
                @Override
                public void onResponse(String response, int action) {
                    stopProgress();
                    if (response != null) {
                        Log.i("response", "" + response.toString());
                        switch (action) {
                            case 101:
                                try {
                                    JSONObject object = new JSONObject(response);
                                    String success = object.getString("success");
                                    String message = object.getString("message");
                                    if (success.equalsIgnoreCase("true")) {
                                        Toast.makeText(context, "" + message, Toast.LENGTH_SHORT).show();
                                        callServiceApi();
                                    } else {
                                        Toast.makeText(context, "" + message, Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    Toast.makeText(context, "Something went wrong", Toast.LENGTH_SHORT).show();
                                    e.printStackTrace();
                                }


                                break;
                            default:
                        }

                    } else {
                        Toast.makeText(context, "Something Went Wrong", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onError(VolleyError error, int action) {

                }
            });

        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_LONG).show();
        }
    }
}