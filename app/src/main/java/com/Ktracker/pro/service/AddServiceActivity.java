package com.Ktracker.pro.service;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.Ktracker.pro.Constants;
import com.Ktracker.pro.Http.HttpCallback;
import com.Ktracker.pro.Http.HttpRequestSingleton;
import com.Ktracker.pro.Http.HttpRequestSingletonPostParams;
import com.Ktracker.pro.NetworkUtil;
import com.Ktracker.pro.R;
import com.Ktracker.pro.base.BaseActivity;
import com.Ktracker.pro.replay.CustomSpinnerAdapter;
import com.Ktracker.pro.response.vehicle.Datum;
import com.Ktracker.pro.response.vehicle.VehicleResponse;
import com.Ktracker.pro.storage.SharedPreferenceUtil;
import com.android.volley.VolleyError;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class AddServiceActivity extends BaseActivity implements View.OnClickListener {
    private TextView txtStartDate;
    private EditText edtName;
    private DatePickerDialog.OnDateSetListener date;
    final Calendar myCalendar = Calendar.getInstance();
    final Calendar myCalendarEnd = Calendar.getInstance();
    private boolean isStartDateSelected = false;
    private boolean edit = false;
    private String selectedVehicleIMEINO = "";
    private Spinner spinner;
    private long selectedStartDate;
    private Context context;
    private LayoutInflater inflater;
    private View view;
    private Button save;

    private TimePickerDialog mTimePicker;
    private TimePickerDialog.OnTimeSetListener time;
    private LinearLayoutManager linearLayoutManager;
    private List<Datum> listAllVehicles = new ArrayList<>();
    private com.Ktracker.pro.response.service.Datum datum;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_service);
        setActionBar(true);
        setBackButtonVisibility(true);
        context = this;
        assignView();
        setListeners();
        callVehicleApi();
        if (getIntent().hasExtra("EDIT")) {
            edit = true;
            datum = (com.Ktracker.pro.response.service.Datum) getIntent().getSerializableExtra("EDIT");
            setActionBarTitle("Edit Service Book");
            setParams(datum);
        } else {
            edit = false;
            setActionBarTitle("Add Service Book");
        }

    }

    private void setParams(com.Ktracker.pro.response.service.Datum datum) {
        edtName.setText(datum.getServiceName());
        txtStartDate.setText(datum.getServiceDate());
        isStartDateSelected=true;
    }


    private void callVehicleApi() {
        if (NetworkUtil.isOnline(context)) {
            showProgress("Loading Vehicles");
            HashMap<String, String> params = new HashMap<>();
            params.put("key", SharedPreferenceUtil.getString(Constants.SP_USER_KEY, ""));
            params.put("cmd", "ALL,*");
            new HttpRequestSingleton(context, getString(R.string.api_base_url) + getString(R.string.api_vehicle_api), params, 100, new HttpCallback() {
                @Override
                public void onResponse(String response, int action) {
                    stopProgress();
                    if (response != null) {
                        Log.i("response", "" + response.toString());
                        switch (action) {
                            case 100:
                                ObjectMapper mapper = new ObjectMapper();
                                try {
                                    VehicleResponse vehicleResponse = mapper.readValue(response, VehicleResponse.class);
                                    sortResponse(vehicleResponse.getData());


                                } catch (JsonProcessingException e) {
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }

                                break;
                            default:
                        }

                    } else {
                        Toast.makeText(context, "Something Went Wrong", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onError(VolleyError error, int action) {

                }
            });

        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_LONG).show();
        }
    }

    private void sortResponse(List<Datum> allList) {
        List<Datum> list = new ArrayList<>();


        for (Datum vehicle : allList
        ) {
            if (vehicle.getActive().equalsIgnoreCase("true")) {
                list.add(vehicle);
            } else {

            }

        }
        listAllVehicles.addAll(list);
        setCustomeSpinner(list);


    }


    private void setCustomeSpinner(List<Datum> list) {
        CustomSpinnerAdapter adapter = new CustomSpinnerAdapter(this,
                R.layout.spinner_item, R.id.titleText, list);

        spinner.setAdapter(adapter);
        if (edit) {

            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).getImei().equalsIgnoreCase(datum.getImei())) {
                    spinner.setSelection(i);
                }

            }
            spinner.setEnabled(false);

        } else {
            spinner.setEnabled(true);
        }
    }


    private DatePickerDialog.OnDateSetListener setDateListener(final TextView textView) {
        date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                updateLabel(textView);

            }

        };
        return date;

    }

    private void setListeners() {


        txtStartDate.setOnClickListener(this);
        save.setOnClickListener(this);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                selectedVehicleIMEINO = listAllVehicles.get(position).getImei();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void assignView() {
        txtStartDate = (TextView) findViewById(R.id.txtStartDate);
        spinner = (Spinner) findViewById(R.id.spinner);
        edtName = (EditText) findViewById(R.id.edtName);
        save = (Button) findViewById(R.id.save);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtStartDate:
                if (edit) {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-DD", Locale.ENGLISH);
                    try {
                        myCalendar.setTime(sdf.parse(datum.getServiceDate()));// all done
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                }
                DatePickerDialog start = new DatePickerDialog(AddServiceActivity.this, setDateListener(txtStartDate), myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH));
                start.getDatePicker();
                start.show();

                break;


            case R.id.save:
                if (isStartDateSelected) {
                    if (edtName.getText().length() > 0)

                        callServiceListApi();
                    else
                        Toast.makeText(context, "Please Enter Name", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(context, "Please select date", Toast.LENGTH_LONG).show();
                }

                break;
            default:
                break;
        }

    }


    private void updateLabel(TextView textView) {
        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        textView.setText(sdf.format(myCalendar.getTime()));
        isStartDateSelected = true;
    }




    private void callServiceListApi() {
        if (NetworkUtil.isOnline(context)) {
            showProgress("Loading...");
            HashMap<String, String> params = new HashMap<>();

            if (edit) {
                params.put("update", selectedVehicleIMEINO);
                params.put("serviceid", datum.getServiceId());

            } else
                params.put("insert", selectedVehicleIMEINO);
            params.put("dt", txtStartDate.getText().toString());
            params.put("name", edtName.getText().toString());


            new HttpRequestSingletonPostParams(context, getString(R.string.api_base_url) + getString(R.string.api_service_api), params, 101, new HttpCallback() {
                @Override
                public void onResponse(String response, int action) {
                    stopProgress();
                    if (response != null) {
                        Log.i("response", "" + response.toString());
                        switch (action) {
                            case 101:
                                try {
                                    JSONObject object = new JSONObject(response);
                                    String success = object.getString("success");
                                    String message = object.getString("message");
                                    if (success.equalsIgnoreCase("true")) {
                                        Toast.makeText(context, "" + message, Toast.LENGTH_SHORT).show();
                                        finish();
                                    } else {
                                        Toast.makeText(context, "" + message, Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    Toast.makeText(context, "Something went wrong", Toast.LENGTH_SHORT).show();
                                    e.printStackTrace();
                                }


                                break;
                            default:
                        }

                    } else {
                        Toast.makeText(context, "Something Went Wrong", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onError(VolleyError error, int action) {

                }
            });

        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_LONG).show();
        }
    }


}
