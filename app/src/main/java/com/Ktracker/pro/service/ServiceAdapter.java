package com.Ktracker.pro.service;

import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.Ktracker.pro.R;
import com.Ktracker.pro.response.service.Datum;

import java.io.Serializable;
import java.util.List;

public class ServiceAdapter extends RecyclerView.Adapter<ServiceAdapter.ViewHolder> {
    Context context;
    List<Datum> itemList;
    ServiceActivity activity;


    public ServiceAdapter(Context context, List<Datum> itemList) {
        this.context = context;
        this.itemList = itemList;
        activity = (ServiceActivity) context;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_service_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {

        final Datum item = itemList.get(position);
        holder.textName.setText(item.getName());

        if(item.getOverdue()==null)
        {
            holder.textDays.setText("Remaining "+item.getRemaining());
            holder.textDays.setTextColor(context.getResources().getColor(R.color.green));
        }
        else
        {
            holder.textDays.setText("Overdue "+item.getOverdue());
            holder.textDays.setTextColor(context.getResources().getColor(R.color.red));
        }


        holder.textDate.setText(item.getServiceDate());
        holder.textServiceName.setText(item.getServiceName());

        holder.txtViewRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.confirmDialog(item.getServiceId());

            }
        });
        holder.txtViewEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,AddServiceActivity.class);
                intent.putExtra("EDIT", (Serializable) item);
                context.startActivity(intent);
            }
        });


    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private CardView card_view;
        private TextView txtViewRemove,
                txtViewEdit,
                textDays,
                textDate,
                textServiceName, textName;
        private ImageView imgExpired, imgchecked, imgcar;

        public ViewHolder(View itemView) {
            super(itemView);

            card_view = (CardView) itemView.findViewById(R.id.card_view);
            textName = (TextView) itemView.findViewById(R.id.textName);
            textServiceName = (TextView) itemView.findViewById(R.id.textServiceName);
            textDate = (TextView) itemView.findViewById(R.id.textDate);
            textDays = (TextView) itemView.findViewById(R.id.textDays);
            txtViewEdit = (TextView) itemView.findViewById(R.id.txtViewEdit);
            txtViewRemove = (TextView) itemView.findViewById(R.id.txtViewRemove);
            imgcar = (ImageView) itemView.findViewById(R.id.imgcar);

        }

    }
}
