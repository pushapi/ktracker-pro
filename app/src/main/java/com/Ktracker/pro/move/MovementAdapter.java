package com.Ktracker.pro.move;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import com.Ktracker.pro.Constants;
import com.Ktracker.pro.R;
import com.Ktracker.pro.Utill;
import com.Ktracker.pro.location.CustomVO;
import com.Ktracker.pro.location.LocationActivity;
import com.Ktracker.pro.response.movement.Datum;

public class MovementAdapter extends RecyclerView.Adapter<MovementAdapter.ViewHolder> {
    Context context;
    List<Datum> itemList;
    MovementActivity activity;
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_FOOTER = 0;


    public MovementAdapter(Context context, List<Datum> itemList) {
        this.context = context;
        this.itemList = itemList;
        activity = (MovementActivity) context;
    }



    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {


        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_list_movement_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

        final Datum item = itemList.get(position);

        if(position==0)
        {
            holder.linearLayoutHeader.setVisibility(View.VISIBLE);
            holder.linearLayout.setVisibility(View.GONE);

            holder.date.setText("Start Time");
            holder.dateend.setText("End Time");
            holder.duration.setText("Duration");
            holder.length.setText("Length");
            changeColor(holder,context.getResources().getColor(R.color.black));
        }

      else if(position+1 == itemList.size())
      {
          holder.linearLayoutHeader.setVisibility(View.GONE);
          holder.linearLayout.setVisibility(View.VISIBLE);
          holder.dateend.setText("Total");
          holder.date.setText("Count="+item.getCount());
          holder.duration.setText(""+item.getTotalDuration());
          holder.length.setText(""+item.getRouteLengthCount());
          changeColor(holder,context.getResources().getColor(R.color.colorAccent));
      }
      else
      {
          holder.linearLayoutHeader.setVisibility(View.GONE);
          holder.linearLayout.setVisibility(View.VISIBLE);
          holder.date.setText(""+item.getDtStart());
          holder.dateend.setText(""+item.getDtEnd());
          holder.duration.setText(""+item.getMovingDuration());
          holder.length.setText(""+item.getRouteLength());
          changeColor(holder,context.getResources().getColor(R.color.black));
      }

      holder.linearLayout.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {


              if(position!=0 && position+1 !=itemList.size()) {
                  CustomVO customVO = new CustomVO();
                  customVO.setLat(Utill.convertStringToDouble(item.getLat()));
                  customVO.setLng(Utill.convertStringToDouble(item.getLng()));
                  String styledText = "" +
                          "<font color='#008CA7'>From:  </font>"+ item.getDtStart() + " " +
                          "<br><font color='#008CA7'>To:  </font>" + item.getDtEnd() +
                          "<br><font color='#008CA7'>Duration:  </font>" + item.getMovingDuration();
                  customVO.setText(styledText);
                  Intent intent = new Intent(context, LocationActivity.class);
                  intent.putExtra(Constants.INTENT_OBJECT, customVO);
                  context.startActivity(intent);
              }
          }
      });




    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private TextView date,dateend,length,duration;
        private LinearLayout linearLayout,linearLayoutHeader;

        public ViewHolder(View itemView) {
            super(itemView);
            date = itemView.findViewById(R.id.date);
            dateend = itemView.findViewById(R.id.dateend);
            duration = itemView.findViewById(R.id.duration);
            length = itemView.findViewById(R.id.lenght);
            linearLayout = itemView.findViewById(R.id.linearLayout);
            linearLayoutHeader = itemView.findViewById(R.id.linearLayoutHeader);

        }

    }

    public void changeColor(ViewHolder holder,int color)

    {
        holder.dateend.setTextColor(color);
        holder.date.setTextColor(color);
        holder.duration.setTextColor(color);
        holder.length.setTextColor(color);


    }
}
