package com.Ktracker.pro.summary;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.Ktracker.pro.R;
import com.Ktracker.pro.Utill;
import com.Ktracker.pro.response.distance.DistanceResponse;

import java.util.List;

public class SummaryAdapter extends RecyclerView.Adapter<SummaryAdapter.ViewHolder> {
    Context context;
    List<DistanceResponse> itemList;
    SummaryActivity activity;


    public SummaryAdapter(Context context, List<DistanceResponse> itemList) {
        this.context = context;
        this.itemList = itemList;
        activity = (SummaryActivity) context;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_list_summary_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {

        final DistanceResponse item = itemList.get(position);


        if (position + 1 == itemList.size()) {
            holder.txtViewDate.setText("Total");
            holder.textKm.setText("Total");
            holder.distance.setText("" + item.getTotal());
        } else {
            holder.txtViewDate.setText(item.getDate());
            holder.distance.setVisibility(View.VISIBLE);
            holder.textKm.setVisibility(View.VISIBLE);
            holder.distance.setText("" + item.getKm());
            holder.textKm.setText("km");

        }

        if (item.getTopSpeed() == 0) {
            holder.textViewTopSpeed.setText("-");
            holder.textViewAvgSpeed.setText("-");
        } else {
            holder.textViewTopSpeed.setText("" + item.getTopSpeed() + " km/h");
            holder.textViewAvgSpeed.setText("" + item.getAvgSpeed() + " km/h");
        }

        if (item.getFuelConsumption() == 0) {
            holder.textViewFuelConsumption.setText("-");
            holder.textViewFuelCost.setText("-");
            holder.llfuelCost.setVisibility(View.GONE);
            holder.llFuelUsage.setVisibility(View.GONE);
        } else {
            holder.llfuelCost.setVisibility(View.VISIBLE);
            holder.llFuelUsage.setVisibility(View.VISIBLE);

            holder.textViewFuelConsumption.setText("" + item.getFuelConsumption() + " Liter(s)");
            holder.textViewFuelCost.setText("₹" + item.getFuelCost());
        }
        holder.textViewStopDuration.setText("" + Utill.convertIntoDigitalClockFormat(item.getStopsDurationTime()));
        holder.textViewDriveDuration.setText("" + Utill.convertIntoDigitalClockFormat(item.getDrivesDurationTime()));
        holder.textViewEID.setText("" + Utill.convertIntoDigitalClockFormat(item.getEngineIdleTime()));



    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView txtViewDate, distance;
        private TextView textViewVehicleTotal, textViewVehicleTravel,
                textViewEwd,
                textViewEID,
                textViewDriveDuration,
                textViewStopDuration,
                textViewFuelCost,
                textViewFuelConsumption,
                textViewAvgSpeed,textKm,
                textViewTopSpeed;
        private LinearLayout llfuelCost, llFuelUsage;

        public ViewHolder(View itemView) {
            super(itemView);
            txtViewDate = itemView.findViewById(R.id.txtViewDate);
            llFuelUsage = itemView.findViewById(R.id.llFuelUsage);
            llfuelCost = itemView.findViewById(R.id.llfuelCost);
            textViewEwd = itemView.findViewById(R.id.textViewEwd);
            textViewEID = itemView.findViewById(R.id.textViewEID);
            textViewDriveDuration = itemView.findViewById(R.id.textViewDriveDuration);
            textViewStopDuration = itemView.findViewById(R.id.textViewStopDuration);
            textViewFuelCost = itemView.findViewById(R.id.textViewFuelCost);
            textViewFuelConsumption = itemView.findViewById(R.id.textViewFuelConsumption);
            textViewAvgSpeed = itemView.findViewById(R.id.textViewAvgSpeed);
            textViewTopSpeed = itemView.findViewById(R.id.textViewTopSpeed);
            distance = itemView.findViewById(R.id.distance);
            textKm = itemView.findViewById(R.id.textKm);

        }

    }


}
