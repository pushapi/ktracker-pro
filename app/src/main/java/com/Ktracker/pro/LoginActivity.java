package com.Ktracker.pro;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

import com.Ktracker.pro.Http.HttpCallback;
import com.Ktracker.pro.Http.HttpRequestSingleton;
import com.Ktracker.pro.Http.HttpRequestSingletonPost;
import com.Ktracker.pro.home.MainActivity;
import com.Ktracker.pro.response.login.Login;
import com.Ktracker.pro.storage.SharedPreferenceUtil;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity {


    private ProgressDialog dialog;
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        context = this;
        if (SharedPreferenceUtil.getBoolean(Constants.SP_IS_USER_LOGGED_IN, false))
        {
            Intent intent = new Intent(context, MainActivity.class);
            startActivity(intent);
            finish();
        }
        // Set up the login form.
        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);
        mEmailView.setText(SharedPreferenceUtil.getString(Constants.SP_USERNAME, ""));

        dialog = new ProgressDialog(this);

        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordView.setText(SharedPreferenceUtil.getString(Constants.SP_PASSWORD, ""));
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

    }


    private void attemptLogin() {


        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            callLoginApi(email, password);
        }
    }

    private void deviceTokenApi()
    {
        if (NetworkUtil.isOnline(context)) {

            dialog.show();
            HashMap<String, String> params = new HashMap<>();
            params.put("user_id",SharedPreferenceUtil.getString(Constants.SP_USER_ID,""));
            params.put("user",mEmailView.getText().toString());
            params.put("pass",mPasswordView.getText().toString());
            params.put("number","test");
            params.put("message","test");
            params.put("device_token","test");
            params.put("firebase_token",SharedPreferenceUtil.getString(Constants.SP_FIREBASE_TOKEN,""));
            params.put("model",Build.BRAND);
            params.put("version",Build.VERSION.RELEASE);
            params.put("api_level",Build.VERSION.RELEASE);
            params.put("company_name",Build.MANUFACTURER);
//            params.put("firebase_apikey",context.getString(R.string.firebase_api_key));
            params.put("type","android");
            params.put("imei",SharedPreferenceUtil.getString(Constants.SP_IMEI,""));
            params.put("function_name","deviceinfo");


            new HttpRequestSingletonPost(context, getString(R.string.api_token_api), params, Constants.API_TOKEN, new HttpCallback() {
                @Override
                public void onResponse(String response, int action) {

                    Log.i("response",response.toString());
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    Intent intent = new Intent(context, MainActivity.class);
                    startActivity(intent);
                    finish();


                }


                @Override
                public void onError(VolleyError error, int action) {

                }
            });
        } else {
            Toast.makeText(context, getString(R.string.internet_not_available), Toast.LENGTH_SHORT);

        }

    }

    private void callLoginApi(String email, final String password) {

        if (NetworkUtil.isOnline(context)) {

            dialog.show();
            HashMap<String, String> params = new HashMap<>();
            params.put(getString(R.string.api_request_param_username), email.trim().toLowerCase());
            params.put(getString(R.string.api_request_param_password), password.trim());


            new HttpRequestSingleton(context,getString(R.string.api_base_url)+ getString(R.string.api_login), params, Constants.API_SIGN_IN, new HttpCallback() {
                @Override
                public void onResponse(String response, int action) {
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }

                    ObjectMapper mapper = new ObjectMapper();
                    Login data;
                    try {
                        data = mapper.readValue(response, Login.class);
                        int position = 0;

                        if (data.getData().get(position).getLogin().equalsIgnoreCase("true")) {

                            SharedPreferenceUtil.putValue(Constants.SP_USERNAME, data.getData().get(position).getUser());
                            SharedPreferenceUtil.putValue(Constants.SP_USER_ID, data.getData().get(position).getId());
                            SharedPreferenceUtil.putValue(Constants.SP_USER_KEY, data.getData().get(position).getKey());
                            SharedPreferenceUtil.putValue(Constants.SP_IS_USER_LOGGED_IN, true);
                            SharedPreferenceUtil.save();
                            deviceTokenApi();

                        } else {
                           HashMap<String, Object> additionalProperties= (HashMap<String, Object>) data.getData().get(position).getAdditionalProperties();
                            Toast.makeText(context, ""+ additionalProperties.get("issue"), Toast.LENGTH_SHORT).show();
                        }

                    } catch (JsonParseException e1) {
                        e1.printStackTrace();
                    } catch (JsonMappingException e1) {
                        e1.printStackTrace();
                    } catch (IOException e1) {
                        e1.printStackTrace();


                    }
                }


                @Override
                public void onError(VolleyError error, int action) {

                }
            });
        } else {
            Toast.makeText(context, getString(R.string.internet_not_available), Toast.LENGTH_SHORT);

        }
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return true;
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 1;
    }

    private boolean checkUserAllowedToLogin(String to, String from) {

//        12:00am

        Date c = Calendar.getInstance().getTime();
        System.out.println("Currentd time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        df.setTimeZone(TimeZone.getDefault());
        String currentDate = df.format(c);

        Long toInMillis = new Long(convertDateInMillis(currentDate + " " + to));
        Long fromInMillis = new Long(convertDateInMillis(currentDate + " " + from));
        Long currentTimeInmillis = new Long(Calendar.getInstance().getTimeInMillis());

        int tocompare = currentTimeInmillis.compareTo(toInMillis);
        int fromcompare = currentTimeInmillis.compareTo(fromInMillis);

        if (tocompare > 0 && fromcompare < 0)
            return true;
        else
            return false;

    }


    private Long convertDateInMillis(String givenDateString) {
        Long timeInMilliseconds = 0l;
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm a");

        sdf.setTimeZone(TimeZone.getDefault());
        //        String givenDateString = "Tue Apr 23 16:08:28 GMT+05:30 2013";
//        SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");
        try {
            Date mDate = sdf.parse(givenDateString);
            timeInMilliseconds = mDate.getTime();
            return timeInMilliseconds;

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return timeInMilliseconds;
    }


}

