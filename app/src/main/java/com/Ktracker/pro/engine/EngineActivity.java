package com.Ktracker.pro.engine;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.Ktracker.pro.Constants;
import com.Ktracker.pro.Http.HttpCallback;
import com.Ktracker.pro.Http.HttpRequestSingleton;
import com.Ktracker.pro.NetworkUtil;
import com.Ktracker.pro.R;
import com.Ktracker.pro.base.BaseActivity;
import com.Ktracker.pro.response.engine.Datum;
import com.Ktracker.pro.response.engine.EngineResponse;
import com.Ktracker.pro.storage.SharedPreferenceUtil;

public class EngineActivity extends BaseActivity {
    private RecyclerView recyclerViewList;
    private LinearLayoutManager linearLayoutManager;
    private EngineAdapter childAdapter;
    private Context context;

    public SharedPreferences keyValues;
    public SharedPreferences.Editor keyValuesEditor;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_engine);
        context = this;
        assignViews();
        setActionBar(true);
        setBackButtonVisibility(true);


        keyValues = context.getSharedPreferences("name_icons_list" + SharedPreferenceUtil.getString(Constants.SP_USERNAME, ""), Context.MODE_PRIVATE);
        keyValuesEditor = keyValues.edit();

        callEngineApi();
    }
    private void assignViews() {
        recyclerViewList = (RecyclerView)findViewById(R.id.recyclerViewList);
    }

    private void setAdapter(List<Datum> childList) {
        linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        recyclerViewList.setLayoutManager(linearLayoutManager);
        recyclerViewList.setItemAnimator(new DefaultItemAnimator());
        childAdapter = new EngineAdapter(context, childList);
        recyclerViewList.setAdapter(childAdapter);
    }
    private void callEngineApi() {
        if (NetworkUtil.isOnline(context)) {
            showProgress("Loading...");
            HashMap<String, String> params = new HashMap<>();
            params.put("key", SharedPreferenceUtil.getString(Constants.SP_USER_KEY, ""));
            params.put("cmd", "All,*");
            new HttpRequestSingleton(context,getString(R.string.api_base_url)+ getString(R.string.api_engine_api), params, 100, new HttpCallback() {
                @Override
                public void onResponse(String response, int action) {
                    stopProgress();
                    if (response != null) {
                        Log.i("response", "" + response.toString());
                        switch (action) {
                            case 100:
                                ObjectMapper mapper = new ObjectMapper();

                                try {
                                    EngineResponse engineResponse = mapper.readValue(response, EngineResponse.class);


                                    sortResponse(engineResponse.getData());
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }








                                break;
                            default:
                        }

                    } else {
                        Toast.makeText(context, "Something Went Wrong", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onError(VolleyError error, int action) {

                }
            });

        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_LONG).show();
        }
    }


    public void callEngineAction(String Url, Datum datum) {
        if (NetworkUtil.isOnline(context)) {
            showProgress("Loading...");
            HashMap<String, String> params = new HashMap<>();
            new HttpRequestSingleton(context, Url, params, 100, new HttpCallback() {
                @Override
                public void onResponse(String response, int action) {
                    stopProgress();
                    if (response != null) {
                        Log.i("response", "" + response.toString());
                        switch (action) {
                            case 100:
                              Toast.makeText(context,response.toString(),Toast.LENGTH_SHORT).show();


                                break;
                            default:
                        }

                    } else {
                        Toast.makeText(context, "Something Went Wrong", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onError(VolleyError error, int action) {

                }
            });

        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_LONG).show();
        }
    }

    private void sortResponse(List<Datum> allList) {
        List<Datum> list = new ArrayList<>();

        for (Datum vehicle : allList
                ) {
            if (vehicle.getActive().equalsIgnoreCase("true")) {
                list.add(vehicle);
            } else {

            }

        }

        setAdapter(list);
    }
}
