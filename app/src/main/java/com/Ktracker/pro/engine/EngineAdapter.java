package com.Ktracker.pro.engine;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import com.Ktracker.pro.R;
import com.Ktracker.pro.response.engine.Datum;

public class EngineAdapter extends RecyclerView.Adapter<EngineAdapter.ViewHolder> {
    Context context;
    List<Datum> itemList;
    EngineActivity activity;


    public EngineAdapter(Context context, List<Datum> itemList) {
        this.context = context;
        this.itemList = itemList;
        activity = (EngineActivity) context;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_engine_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {

        final Datum item = itemList.get(position);
        holder.name.setText(item.getName());

        holder.time.setText(item.getDtTracker());

        if(item.getSupported().equalsIgnoreCase("yes"))
        {
            holder.linearButtons.setVisibility(View.VISIBLE);
            holder.linearStatus.setVisibility(View.GONE);
        }
        else
        {
            holder.linearButtons.setVisibility(View.GONE);
            holder.linearStatus.setVisibility(View.VISIBLE);
        }


        if(activity.keyValues.getString(String.valueOf(item.getImei()),"def").equalsIgnoreCase("1"))
        {
            holder.btnstart.setText("✔ Started");
            holder.btnstop.setText("Stop");
        }
        else if(activity.keyValues.getString(String.valueOf(item.getImei()),"def").equalsIgnoreCase("0"))
        {
            holder.btnstart.setText("Start");
            holder.btnstop.setText("✔ Stopped");
        }
        else
        {
            holder.btnstart.setText("Start");
            holder.btnstop.setText("Stop");
        }



        holder.btnstart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                activity.callEngineAction(item.getOn(),item);
                activity.keyValuesEditor.putString(String.valueOf(item.getImei()),"1");
                activity.keyValuesEditor.commit();
                holder.btnstart.setText("✔ started");
                holder.btnstop.setText("stop");


            }
        });

        holder.btnstop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                activity.callEngineAction(item.getOff(),item);


                activity.keyValuesEditor.putString(String.valueOf(item.getImei()),"0");
                activity.keyValuesEditor.commit();

                holder.btnstart.setText("start");
                holder.btnstop.setText("✔ stopped");


            }
        });


    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private CardView card_view;
        private TextView time, name;
        private LinearLayout linearStatus,linearButtons;

        private AppCompatButton btnstop,btnstart;
        public ViewHolder(View itemView) {
            super(itemView);

            card_view = (CardView) itemView.findViewById(R.id.card_view);
            name = (TextView) itemView.findViewById(R.id.name);
            time = (TextView) itemView.findViewById(R.id.time);
            btnstart = (AppCompatButton) itemView.findViewById(R.id.btnstart);
            btnstop = (AppCompatButton) itemView.findViewById(R.id.btnstop);
            linearButtons = (LinearLayout)itemView.findViewById(R.id.linearButtons);
            linearStatus = (LinearLayout)itemView.findViewById(R.id.linearStatus);

        }

    }
}
