package com.Ktracker.pro;

public class Constants {
    public static final Integer API_SIGN_IN = 100;
    public static final Integer API_TOKEN = 200;
    public static final Integer API_FORGOT = 500;
    public static final String KEY = "}@p]&b6,N2}YW=D-";
    public static final String SP_USERNAME = "SP_USERNAME";
    public static final String SP_USER_KEY = "SP_USER_KEY";
    public static final String SP_USER_ID = "SP_USER_ID";
    public static final String SP_PASSWORD = "SP_PASSWORD";
    public static final String SP_FIREBASE_TOKEN = "SP_FIREBASE_TOKEN";
    public static final String SP_IMEI = "SP_IMEI";
    public static final String SP_URL = "SP_URL";
    public static final String SP_TIME_TO = "SP_TIME_TO";
    public static final String SP_TIME_FROM = "SP_TIME_FROM";
    public static final String SP_IS_USER_LOGGED_IN = "SP_IS_USER_LOGGED_IN";
    public static final String SP_IS_USER_PREFERENCE_LIVE = "SP_IS_USER_PREFERENCE_LIVE";
    public static final String SP_IS_USER_PREFERENCE_DASHBOARD = "SP_IS_USER_PREFERENCE_DASHBOARD";
    public static final String SP_IS_USER_PREFERENCE_NOTIFICATION = "SP_IS_USER_PREFERENCE_NOTIFICATION";



    public static final String INTENT_START_TIME = "INTENT_START_TIME";
    public static final String INTENT_END_TIME = "INTENT_END_TIME";
    public static final String INTENT_START_DATE = "INTENT_START_DATE";
    public static final String INTENT_END_DATE = "INTENT_END_DATE";
    public static final String INTENT_IMEI = "INTENT_IMEI";
    public static final String INTENT_OBJECT = "INTENT_OBJECT";
    public static final String INTENT_LIST = "INTENT_LIST";

    public static final Integer PREFERENCE_LIVE_DEFAULT=10;
    public static final Integer PREFERENCE_DASHBOARD_DEFAULT=30;

}
