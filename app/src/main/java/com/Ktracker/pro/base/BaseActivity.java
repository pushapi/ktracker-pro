package com.Ktracker.pro.base;

import android.app.ProgressDialog;
import android.os.Bundle;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.Ktracker.pro.R;

public class BaseActivity extends AppCompatActivity {

    public ProgressDialog pd;
    public Toolbar myToolbar;
    public ActionBar ab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
        myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
    }

    public void setActionBar(Boolean isActive) {
        if (isActive) {
            getSupportActionBar().show();
        }
        else
        {getSupportActionBar().hide();}
    }

    public void setBackButtonVisibility(boolean show) {

        getSupportActionBar().setDisplayShowHomeEnabled(show);
        getSupportActionBar().setDisplayHomeAsUpEnabled(show);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(show);
        getSupportActionBar().setHomeButtonEnabled(show);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return false;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    public void setActionBarTitle(String title) {
        getSupportActionBar().setTitle(title);
    }

    public void showProgress(String msg) {
        try {
            if (pd == null) {
                pd = new ProgressDialog(this);
                pd.setCancelable(true);
            }
            pd.setMessage(msg);
            pd.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void stopProgress() {
        try {
            if (pd != null) {
                pd.cancel();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
