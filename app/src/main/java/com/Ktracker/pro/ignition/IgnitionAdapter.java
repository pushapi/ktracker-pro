package com.Ktracker.pro.ignition;

import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.Ktracker.pro.Constants;
import com.Ktracker.pro.R;
import com.Ktracker.pro.Utill;
import com.Ktracker.pro.location.CustomVO;
import com.Ktracker.pro.location.LocationActivity;
import com.Ktracker.pro.response.ignition.Datum;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class IgnitionAdapter extends RecyclerView.Adapter<IgnitionAdapter.ViewHolder> {
    Context context;
    List<Datum> itemList;
    IgnitionActivity activity;
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_FOOTER = 0;


    public IgnitionAdapter(Context context, List<Datum> itemList) {
        this.context = context;
        this.itemList = itemList;
        activity = (IgnitionActivity) context;
    }



    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {


        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_list_ignition_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

        final Datum item = itemList.get(position);

        if(position==0)
        {
            holder.linearLayoutHeader.setVisibility(View.VISIBLE);
            holder.linearLayout.setVisibility(View.GONE);

            holder.date.setText("Start Time");
            holder.dateend.setText("End Time");
            holder.duration.setText("Duration");
            changeColor(holder,context.getResources().getColor(R.color.black));
        }

      else if(position+1 == itemList.size())
      {
          holder.linearLayoutHeader.setVisibility(View.GONE);
          holder.linearLayout.setVisibility(View.VISIBLE);
          holder.dateend.setText("Total");
          holder.date.setText("Count="+item.getCount());
          holder.duration.setText(""+item.getTotalDuration());
          changeColor(holder,context.getResources().getColor(R.color.colorAccent));
      }
      else
      {
          holder.linearLayoutHeader.setVisibility(View.GONE);
          holder.linearLayout.setVisibility(View.VISIBLE);
          holder.date.setText(""+item.getStartdate());
          holder.dateend.setText(""+item.getEnddate());
          holder.duration.setText(""+item.getDuration());
          changeColor(holder,context.getResources().getColor(R.color.black));
      }

      holder.linearLayout.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {


              if(position!=0 && position+1 !=itemList.size()) {

                  List<CustomVO> list = new ArrayList<>();
                  CustomVO startLat = new CustomVO();
                  startLat.setLat(Utill.convertStringToDouble(item.getStartlat()));
                  startLat.setLng(Utill.convertStringToDouble(item.getStartlng()));
                  String styledText = "" +
                          "<font color='#008CA7'>From:  </font>"+ item.getStartdate() + " " +
                          "<br><font color='#008CA7'>To:  </font>" + item.getEnddate() +
                          "<br><font color='#008CA7'>Duration:  </font>" + item.getDuration();
                  startLat.setText(styledText);
                  list.add(startLat);
                   startLat = new CustomVO();
                  startLat.setLat(Utill.convertStringToDouble(item.getEndlat()));
                  startLat.setLng(Utill.convertStringToDouble(item.getEndlng()));
                   styledText = "" +
                          "<font color='#008CA7'>From:  </font>"+ item.getStartdate() + " " +
                          "<br><font color='#008CA7'>To:  </font>" + item.getEnddate() +
                          "<br><font color='#008CA7'>Duration:  </font>" + item.getDuration();
                  startLat.setText(styledText);
                  list.add(startLat);
                  Collections.reverse(list);
                  Intent intent = new Intent(context, LocationActivity.class);
                  intent.putExtra(Constants.INTENT_LIST, (Serializable) list);
                  context.startActivity(intent);
              }
          }
      });




    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private TextView date,dateend,duration;
        private LinearLayout linearLayout,linearLayoutHeader;

        public ViewHolder(View itemView) {
            super(itemView);
            date = itemView.findViewById(R.id.date);
            dateend = itemView.findViewById(R.id.dateend);
            duration = itemView.findViewById(R.id.duration);
            linearLayout = itemView.findViewById(R.id.linearLayout);
            linearLayoutHeader = itemView.findViewById(R.id.linearLayoutHeader);

        }

    }

    public void changeColor(ViewHolder holder,int color)

    {
        holder.dateend.setTextColor(color);
        holder.date.setTextColor(color);
        holder.duration.setTextColor(color);


    }
}
