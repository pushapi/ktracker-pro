package com.Ktracker.pro;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import androidx.annotation.NonNull;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.Ktracker.pro.Http.HttpCallback;
import com.Ktracker.pro.Http.HttpRequestSingleton;
import com.Ktracker.pro.base.BaseActivity;
import com.Ktracker.pro.replay.ReplayMapActivity;
import com.Ktracker.pro.response.Track;
import com.Ktracker.pro.response.distance.DistanceResponse;
import com.Ktracker.pro.storage.SharedPreferenceUtil;
import com.android.volley.VolleyError;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.mancj.slideup.SlideUp;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Timer;

public class MapsActivity extends BaseActivity implements OnMapReadyCallback {
    private GoogleMap mMap;
    private Polyline line;
    private ArrayList<LatLng> locations = new ArrayList();
    private Timer timer;
    private static Integer TIME_INTERVAL;
    private LatLng lastknownLocation = new LatLng(0, 0);
    private String selectedVehicleIMEINO = "";
    private List<LatLng> pointsGreen = new ArrayList<>();

    private LinearLayout llInfo, bottom_sheet;
    private TextView textViewVehicleno;
    private TextView textViewVehicleSpeed;
    private TextView textViewOdoMeter;
    private TextView textViewVehicleDateandTime;
    private TextView textViewVehicleRunning;
    private TextView textViewVehicleAddress;
    private TextView textViewVehicleTotal, textViewVehicleTravel,
            textViewEwd,
            textViewEID,
            textViewDriveDuration,
            textViewStopDuration,
            textViewFuelCost,
            textViewFuelConsumption,
            textViewAvgSpeed,
            textViewTopSpeed;
    private TextView textViewVehicleSince, txtPWR, txtAC, txtGPS, txtKEY;
    private RelativeLayout relativeMapType, relativeShare, relativeTraffic,relativeHistory;
    private List<LatLng> points = new ArrayList<>();
    private ArrayList<Track> bufferList = new ArrayList<>();
    private Marker marker;
    private Context context;
    private boolean isStarted = false;
    private Runnable runnable;
    private Handler handler = new Handler();
    private SlideUp slideUp;
    private LinearLayout slideView;
    private ImageView imgSupply, imgIgnition, imgLoc, imgAc,imagePlace;
    BottomSheetBehavior sheetBehavior;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        setActionBar(true);
        setBackButtonVisibility(true);



        TIME_INTERVAL = SharedPreferenceUtil.getInt(Constants.SP_IS_USER_PREFERENCE_LIVE, Constants.PREFERENCE_LIVE_DEFAULT) * 1000;

        runnable = new Runnable() {
            @Override
            public void run() {
                callApi();
                handler.postDelayed(this, TIME_INTERVAL);
            }
        };
        context = this;


        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        assignViews();
        timer = new Timer();
        startTimer(TIME_INTERVAL);


        relativeMapType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(MapsActivity.this, "");

            }
        });


    }

    private void updateTrack(LatLng lastknownLocation) {
        points.add(lastknownLocation);
        int size = points.size();

        if(size>10)
        {



            pointsGreen = new ArrayList<>();
            pointsGreen.add(points.get(size-10));
            pointsGreen.add(lastknownLocation);
            line.setPoints(pointsGreen);

        }
        else
        {
            line.setPoints(points);
        }





    }


    private void assignViews() {
        llInfo = (LinearLayout) findViewById(R.id.llInfo);
        textViewVehicleSpeed = (TextView) findViewById(R.id.textViewVehicleSpeed);
        textViewVehicleDateandTime = (TextView) findViewById(R.id.textViewVehicleDateandTime);
        textViewVehicleRunning = (TextView) findViewById(R.id.textViewVehicleRunning);
        textViewVehicleAddress = (TextView) findViewById(R.id.textViewVehicleAddress);
        textViewOdoMeter = (TextView) findViewById(R.id.textViewOdoMeter);
        txtPWR = (TextView) findViewById(R.id.txtPWR);
        txtKEY = (TextView) findViewById(R.id.txtKEY);
        txtAC = (TextView) findViewById(R.id.txtAC);
        txtGPS = (TextView) findViewById(R.id.txtGPS);

        textViewVehicleSince = (TextView) findViewById(R.id.textViewVehicleSince);
        textViewVehicleTravel = (TextView) findViewById(R.id.textViewVehicleTravel);
        relativeMapType = (RelativeLayout) findViewById(R.id.relativeMapType);
        relativeShare = (RelativeLayout) findViewById(R.id.relativeShare);
        relativeHistory = (RelativeLayout) findViewById(R.id.relativeHistory);
        relativeTraffic = (RelativeLayout) findViewById(R.id.relativeTraffic);
        imgLoc = (ImageView) findViewById(R.id.imgLoc);
        imgIgnition = (ImageView) findViewById(R.id.imgIgnition);
        imgSupply = (ImageView) findViewById(R.id.imgSupply);
        imgAc = (ImageView) findViewById(R.id.imgAc);
        bottom_sheet = (LinearLayout) findViewById(R.id.bottom_sheet);
        sheetBehavior = BottomSheetBehavior.from(bottom_sheet);
        textViewEwd = (TextView) findViewById(R.id.textViewEwd);
        textViewEID = (TextView) findViewById(R.id.textViewEID);
        textViewDriveDuration = (TextView) findViewById(R.id.textViewDriveDuration);
        textViewStopDuration = (TextView) findViewById(R.id.textViewStopDuration);
        textViewFuelCost = (TextView) findViewById(R.id.textViewFuelCost);
        textViewFuelConsumption = (TextView) findViewById(R.id.textViewFuelConsumption);
        textViewAvgSpeed = (TextView) findViewById(R.id.textViewAvgSpeed);
        textViewTopSpeed = (TextView) findViewById(R.id.textViewTopSpeed);
        imagePlace = (ImageView) findViewById(R.id.imagePlace);
        sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

        sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View view, int i) {
                if (i == BottomSheetBehavior.STATE_HIDDEN) {
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(lastknownLocation, mMap.getCameraPosition().zoom));
                } else {
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(lastknownLocation, mMap.getCameraPosition().zoom));
                }
            }

            @Override
            public void onSlide(@NonNull View view, float v) {

            }
        });

    }


    private void callfirstTimeAPi() {
        if (NetworkUtil.isOnline(getApplicationContext())) {
            HashMap<String, String> params = new HashMap<>();
            params.put("imei", SharedPreferenceUtil.getString(Constants.SP_IMEI, ""));
            params.put("userkey", SharedPreferenceUtil.getString(Constants.SP_USER_KEY, ""));
            String url = getString(R.string.api_base_url) + getString(R.string.api_tracking_api);

            new HttpRequestSingleton(getApplicationContext(), url, params, 0, new HttpCallback() {
                @Override
                public void onResponse(String response, int action) {
                    ObjectMapper mapper = new ObjectMapper();
                    Track data;
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONArray dataArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject obj = (JSONObject) dataArray.get(i);
                            data = mapper.readValue(obj.toString(), Track.class);
                            bufferList.add(data);
                            updateUI(data);
                            lastknownLocation = new LatLng(Double.parseDouble(data.getLat()), Double.parseDouble(data.getLng()));
                            updateTrack(lastknownLocation);
                            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(lastknownLocation, 17.0f));


                        }


                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onError(VolleyError error, int action) {

                }
            });
        }

    }

    private void callApi() {
        if (NetworkUtil.isOnline(getApplicationContext())) {
            HashMap<String, String> params = new HashMap<>();

            params.put("imei", SharedPreferenceUtil.getString(Constants.SP_IMEI, ""));
            params.put("userkey", SharedPreferenceUtil.getString(Constants.SP_USER_KEY, ""));
            String url = getString(R.string.api_base_url) + getString(R.string.api_tracking_api);

            new HttpRequestSingleton(getApplicationContext(), url, params, 0, new HttpCallback() {
                @Override
                public void onResponse(String response, int action) {

                    ObjectMapper mapper = new ObjectMapper();
                    Track data = null;
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONArray dataArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject obj = (JSONObject) dataArray.get(i);
                            data = mapper.readValue(obj.toString(), Track.class);

                            lastknownLocation = new LatLng(Double.parseDouble(data.getLat()), Double.parseDouble(data.getLng()));
                            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(lastknownLocation, mMap.getCameraPosition().zoom));
                            updateUI(data);
                            updateTrack(lastknownLocation);

                        }
                        bufferList.add(data);


//                        switch (bufferList.size()) {
//                            case 0:
//                                bufferList.add(0,data);
//                                break;
//                            case 1:
//                                bufferList.add(1,data);
//
//
//                                break;
//                            case 2:
//                                bufferList.add(2,data);
//                                break;
//                            case 3:
//                                bufferList.add(2,data);
//
//                                break;
//                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onError(VolleyError error, int action) {

                }
            });
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMaxZoomPreference(19.0f);
        line = mMap.addPolyline(new PolylineOptions()
                .width(10)
                .color(Color.GREEN));
        mMap.getUiSettings().setMapToolbarEnabled(true);
        mMap.setInfoWindowAdapter(new MyInfoWindowAdapter());


        relativeTraffic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mMap.isTrafficEnabled()) {
                    mMap.setTrafficEnabled(false);
                } else {
                    mMap.setTrafficEnabled(true);
                }

            }
        });


        callfirstTimeAPi();


    }


    private void setCustomMap(int mapType) {
        if (mMap != null) {


//            mMap.moveCamera(CameraUpdateFactory.newLatLng(lastknownLocation));

            animateMarker(marker, lastknownLocation, false);
            line = mMap.addPolyline(new PolylineOptions()
                    .width(5)
                    .color(Color.GREEN));

            mMap.setMapType(mapType);


        }

    }

    private void startTimer(int timeInterval) {

        if (!isStarted) {
            isStarted = true;
            handler.postDelayed(runnable, timeInterval);
        }

    }


    @Override
    public void onPause() {
        super.onPause();
        isStarted = false;
        handler.removeCallbacks(runnable);

    }

    @Override

    public void onResume() {
        super.onResume();

        startTimer(TIME_INTERVAL);
    }


    private void updateUI(final Track data) {

        selectedVehicleIMEINO = data.getImei();

        final Location location = new Location(LocationManager.GPS_PROVIDER);
        location.setLatitude(Double.parseDouble(data.getLat()));
        location.setLongitude(Double.parseDouble(data.getLng()));

        imagePlace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openGoogleMap(location);
            }
        });
        setActionBarTitle(data.getName());
        LatLng latLng = new LatLng(Double.parseDouble(data.getLat()), Double.parseDouble(data.getLng()));




        if (marker != null) {

            callgetLocationApi(location);
            marker.setRotation(Float.valueOf(data.getAngle()));
            marker.setIcon(BitmapDescriptorFactory.fromResource(Utill.getMapIconAndColor(data.getIcon(), data.getStatus())));


            if (!marker.getPosition().equals(latLng))


                animateMarkerNew(location, marker);
        } else {
            MarkerOptions markerOptions = new MarkerOptions()
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_car)).position(latLng);


            marker = mMap.addMarker(markerOptions);
            marker.setIcon(BitmapDescriptorFactory.fromResource(Utill.getMapIconAndColor(data.getIcon(), data.getStatus())));
        }


        String since = "";

        if (data.getStatus().equalsIgnoreCase("offline")) {
            imgLoc.setImageResource(R.drawable.loc_valid_0);
            txtGPS.setTextColor(getResources().getColor(R.color.grey_icon));
        } else if (data.getLocValid().equalsIgnoreCase("1")) {

            imgLoc.setImageResource(R.drawable.loc_valid_1);
            txtGPS.setTextColor(getResources().getColor(R.color.green_icon));
        } else {
            imgLoc.setImageResource(R.drawable.loc_valid_1);
            txtGPS.setTextColor(getResources().getColor(R.color.green_icon));

        }

        if (data.getStatus().equalsIgnoreCase("idle")) {
            imgIgnition.setImageResource(R.drawable.ignition_idle);
            txtKEY.setTextColor(getResources().getColor(R.color.yellow_icon));
        } else if (data.getIgnition().equalsIgnoreCase("on")) {
            imgIgnition.setImageResource(R.drawable.ignition_on);
            txtKEY.setTextColor(getResources().getColor(R.color.green_icon));
        } else if (data.getIgnition().equalsIgnoreCase("idle")) {
            imgIgnition.setImageResource(R.drawable.ignition_idle);
            txtKEY.setTextColor(getResources().getColor(R.color.yellow_icon));

        } else {
            imgIgnition.setImageResource(R.drawable.ignition_off);
            txtKEY.setTextColor(getResources().getColor(R.color.grey_icon));

        }

        if (data.getSupply().equalsIgnoreCase("on")) {
            imgSupply.setImageResource(R.drawable.supply_on);
            txtPWR.setTextColor(getResources().getColor(R.color.green_icon));
        } else if (data.getSupply().equalsIgnoreCase("off")) {

            imgSupply.setImageResource(R.drawable.supply_off);
            txtPWR.setTextColor(getResources().getColor(R.color.red_icon));
        } else {
            imgSupply.setImageResource(R.drawable.supply_null);
            txtPWR.setTextColor(getResources().getColor(R.color.grey_icon));


        }

        if (data.getAc().equalsIgnoreCase("on")) {
            imgAc.setImageResource(R.drawable.ac_on);
            txtAC.setTextColor(getResources().getColor(R.color.green_icon));

        } else if (data.getAc().equalsIgnoreCase("off")) {

            imgAc.setImageResource(R.drawable.ac_off);
            txtAC.setTextColor(getResources().getColor(R.color.red_icon));
        } else {
            imgAc.setImageResource(R.drawable.ac_null);
            txtAC.setTextColor(getResources().getColor(R.color.grey_icon));


        }

        textViewVehicleSpeed.setText(data.getSpeed() + " km/h");
        textViewOdoMeter.setText(data.getOdometer());
        textViewVehicleDateandTime.setText(data.getDtTracker());

        if (data.getSince().getSeconds() > 0)
            since = since + data.getSince().getSeconds() + " second";
        if (data.getSince().getMinutes() > 0)
            since = data.getSince().getMinutes() + " Minute " + since;
        if (data.getSince().getHours() > 0)
            since = data.getSince().getHours() + " Hour " + since;
        if (data.getSince().getDays() > 0)
            since = data.getSince().getDays() + " days " + since;
        if (data.getSince().getMonths() > 0)
            since = data.getSince().getMonths() + " Months " + since;
        if (data.getSince().getYears() > 0)
            since = data.getSince().getYears() + " years " + since;


        textViewVehicleSince.setText(since);
        textViewVehicleRunning.setText(data.getStatus().toString().toUpperCase());
        textViewVehicleAddress.setText(data.getAddress());



        relativeShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT,
                        "" + data.getUrl());
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
            }
        });

        relativeHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Calendar cal = Calendar.getInstance();
                SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
                format1.format(cal.getTime());
               Intent intent = new Intent(context, ReplayMapActivity.class);
                intent.putExtra(Constants.INTENT_START_DATE, format1.format(cal.getTime()));
                intent.putExtra(Constants.INTENT_END_DATE, format1.format(cal.getTime()));
                intent.putExtra(Constants.INTENT_START_TIME, "00:00:00");
                intent.putExtra(Constants.INTENT_END_TIME, "23:59:00");
                intent.putExtra(Constants.INTENT_IMEI, selectedVehicleIMEINO);
                startActivity(intent);
            }
        });
        callDistanceApi(data);
    }

    private void callgetLocationApi(Location location) {
        if (NetworkUtil.isOnline(context)) {
            HashMap<String, String> params = new HashMap<>();
            params.put("lat", String.valueOf(String.valueOf(location.getLatitude())));
            params.put("long", String.valueOf(String.valueOf(location.getLongitude())));
            new HttpRequestSingleton(context, getString(R.string.api_base_url) + getString(R.string.api_live_api), params, 100, new HttpCallback() {
                @Override
                public void onResponse(String response, int action) {
                    if (response != null) {
                        Log.i("response", "" + response.toString());

                        marker.setTitle(response.toString());
                        if (marker.isInfoWindowShown())
                            marker.showInfoWindow();


                    } else {
                        Toast.makeText(context, "Something Went Wrong", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onError(VolleyError error, int action) {

                }
            });

        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_LONG).show();
        }
    }


    public void animateMarker(final Marker marker, final LatLng toPosition,
                              final boolean hideMarker) {
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        Projection proj = mMap.getProjection();
        Point startPoint = proj.toScreenLocation(marker.getPosition());
        final LatLng startLatLng = proj.fromScreenLocation(startPoint);
        final long duration = 11000;

        final Interpolator interpolator = new LinearInterpolator();

        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed
                        / duration);
                double lng = t * toPosition.longitude + (1 - t)
                        * startLatLng.longitude;
                double lat = t * toPosition.latitude + (1 - t)
                        * startLatLng.latitude;
                marker.setPosition(new LatLng(lat, lng));

                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                } else {
                    if (hideMarker) {
                        marker.setVisible(false);
                    } else {
                        marker.setVisible(true);
                    }
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
//        new AlertDialog.Builder(this)
//                .setMessage("Are you sure you want to exit?")
//                .setCancelable(false)
//                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int id) {
//                        MapsClusterActivity.this.finish();
//                    }
//                })
//                .setNegativeButton("No", null)
//                .show();
    }


    public void showDialog(Activity activity, String msg) {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        Window window = dialog.getWindow();
        window.setGravity(Gravity.TOP);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.layout_custom);
        RadioGroup radioGroup;

        radioGroup = (RadioGroup) dialog.findViewById(R.id.radioGroup);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton rb = (RadioButton) group.findViewById(checkedId);
                if (null != rb && checkedId > -1) {

                    switch (rb.getText().toString().toLowerCase()) {
                        case "normal":
                            setCustomMap(GoogleMap.MAP_TYPE_NORMAL);
                            break;
                        case "satellite":
                            setCustomMap(GoogleMap.MAP_TYPE_SATELLITE);
                            break;
                        case "hybrid":
                            setCustomMap(GoogleMap.MAP_TYPE_HYBRID);
                            break;
                        default:
                            setCustomMap(GoogleMap.MAP_TYPE_NORMAL);
                            break;
                    }
                    dialog.cancel();
//                    Toast.makeText(MainActivity.this, rb.getText(), Toast.LENGTH_SHORT).show();
                }

            }
        });


        dialog.show();

    }


    private void animateMarkerNew(final Location destination, final Marker marker) {

        if (marker != null) {

            final LatLng startPosition = marker.getPosition();
            final LatLng endPosition = new LatLng(destination.getLatitude(), destination.getLongitude());

            final float startRotation = marker.getRotation();
            final LatLngInterpolatorNew latLngInterpolator = new LatLngInterpolatorNew.LinearFixed();

            ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, 1);
            valueAnimator.setDuration(0); // duration 3 second
            valueAnimator.setInterpolator(new LinearInterpolator());
            valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    try {
                        float v = animation.getAnimatedFraction();
                        LatLng newPosition = latLngInterpolator.interpolate(v, startPosition, endPosition);
                        marker.setPosition(newPosition);


                        marker.setRotation(getBearing(startPosition, new LatLng(destination.getLatitude(), destination.getLongitude())));
                    } catch (Exception ex) {
                        //I don't care atm..
                    }
                }
            });
            valueAnimator.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);


                    // if (mMarker != null) {
                    // mMarker.remove();
                    // }
                    // mMarker = googleMap.addMarker(new MarkerOptions().position(endPosition).icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_car)));

                }
            });
            valueAnimator.start();
        }
    }

    private interface LatLngInterpolatorNew {
        LatLng interpolate(float fraction, LatLng a, LatLng b);

        class LinearFixed implements LatLngInterpolatorNew {
            @Override
            public LatLng interpolate(float fraction, LatLng a, LatLng b) {
                double lat = (b.latitude - a.latitude) * fraction + a.latitude;
                double lngDelta = b.longitude - a.longitude;
                // Take the shortest path across the 180th meridian.
                if (Math.abs(lngDelta) > 180) {
                    lngDelta -= Math.signum(lngDelta) * 360;
                }
                double lng = lngDelta * fraction + a.longitude;
                return new LatLng(lat, lng);
            }
        }
    }


    //Method for finding bearing between two points
    private float getBearing(LatLng begin, LatLng end) {
        double lat = Math.abs(begin.latitude - end.latitude);
        double lng = Math.abs(begin.longitude - end.longitude);

        if (begin.latitude < end.latitude && begin.longitude < end.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)));
        else if (begin.latitude >= end.latitude && begin.longitude < end.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 90);
        else if (begin.latitude >= end.latitude && begin.longitude >= end.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)) + 180);
        else if (begin.latitude < end.latitude && begin.longitude >= end.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 270);
        return -1;
    }

    private void callDistanceApi(Track data) {
        String km = "0";
        if (NetworkUtil.isOnline(context)) {
            HashMap<String, String> params = new HashMap<>();
//            params.put("imei", "000007065017907");
//            params.put("userkey", "A29DB84D21FA82EAF564251D777007E1");
//            params.put("startdate", "2018-10-01");
//            params.put("enddate", "2018-10-31");

            String myFormat = "yyyy-MM-dd"; //In which you need put here
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
            Calendar calendar = Calendar.getInstance();

            params.put("imei", data.getImei());
            params.put("userkey", SharedPreferenceUtil.getString(Constants.SP_USER_KEY, ""));
            params.put("startdate", sdf.format(calendar.getTime()));
            params.put("enddate", sdf.format(calendar.getTime()));

            new HttpRequestSingleton(context, getString(R.string.api_base_url) + getString(R.string.api_distance_api), params, 101, new HttpCallback() {
                @Override
                public void onResponse(String response, int action) {
                    if (response != null) {
                        Log.i("response", "" + response.toString());
                        switch (action) {
                            case 101:
                                ObjectMapper mapper = new ObjectMapper();
                                try {

                                    JSONArray array = new JSONArray(response);
                                    List<DistanceResponse> listResponse = new ArrayList<>();
                                    DistanceResponse header = new DistanceResponse();
                                    listResponse.add(header);

                                    for (int i = 0; i < array.length(); i++) {
                                        DistanceResponse distanceResponse = mapper.readValue(array.get(i).toString(), DistanceResponse.class);
                                        listResponse.add(distanceResponse);

                                    }
                                    populateBottomSheet(listResponse.get(listResponse.size() - 1));
                                    textViewVehicleTravel.setText(String.valueOf(listResponse.get(listResponse.size() - 1).getTotal()) + " KM");


                                } catch (JsonProcessingException e) {
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                break;
                            default:
                        }

                    }
                }

                @Override
                public void onError(VolleyError error, int action) {

                }
            });

        }
    }

    private void populateBottomSheet(DistanceResponse distanceResponse) {
        textViewTopSpeed.setText("" + distanceResponse.getTopSpeed() + " km/h");
        textViewAvgSpeed.setText("" + distanceResponse.getAvgSpeed() + " km/h");
        textViewFuelConsumption.setText("" + Utill.convertTwoDecimal(distanceResponse.getFuelConsumption())+ " Liter(s)");
        textViewFuelCost.setText("₹" + Utill.convertTwoDecimal(distanceResponse.getFuelCost()));
        textViewStopDuration.setText("" + distanceResponse.getStopsDuration());
        textViewDriveDuration.setText("" + distanceResponse.getDrivesDuration());
        textViewEID.setText("" + distanceResponse.getEngineIdle());
        textViewEwd.setText("" + distanceResponse.getEngineWork());
    }

    public class MyInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

        private final View myContentsView;

        MyInfoWindowAdapter() {
            myContentsView = getLayoutInflater().inflate(R.layout.custom_marker_info, null);
        }

        @Override
        public View getInfoContents(Marker marker) {

            TextView text = ((TextView) myContentsView.findViewById(R.id.text));
            text.setText(marker.getTitle());

            return myContentsView;
        }

        @Override
        public View getInfoWindow(Marker marker) {
            // TODO Auto-generated method stub
            return null;
        }

    }


    public void openGoogleMap(Location location)
    {
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                Uri.parse("http://maps.google.com/maps?daddr="+location.getLatitude()+","+location.getLongitude()+""));
        startActivity(intent);

    }


}




