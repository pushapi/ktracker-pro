package com.Ktracker.pro.tracking;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.fragment.app.FragmentActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.Ktracker.pro.Utill;
import com.android.volley.VolleyError;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterItem;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.google.maps.android.ui.IconGenerator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.Ktracker.pro.Constants;
import com.Ktracker.pro.Http.HttpCallback;
import com.Ktracker.pro.Http.HttpRequestSingleton;
import com.Ktracker.pro.MapsActivity;
import com.Ktracker.pro.NetworkUtil;
import com.Ktracker.pro.R;
import com.Ktracker.pro.response.vehicle.Datum;
import com.Ktracker.pro.response.vehicle.VehicleResponse;
import com.Ktracker.pro.storage.SharedPreferenceUtil;

public class MapsClusterActivity extends FragmentActivity implements OnMapReadyCallback, ClusterManager.OnClusterClickListener<Datum>,
        ClusterManager.OnClusterInfoWindowClickListener<Datum>,
        ClusterManager.OnClusterItemClickListener<Datum>,
        ClusterManager.OnClusterItemInfoWindowClickListener<Datum> {

    private GoogleMap mMap;
    private ClusterManager<Datum> mClusterManager;
    private Context context;
    private Datum clickedClusterItem;
    private RelativeLayout relativeMapType;
    private ArrayList<Marker> arrayList = new ArrayList<>();
    private ArrayList<Datum> arrayListItem = new ArrayList<>();
    private ArrayList<String> arrayListExpanded = new ArrayList<>();
    private ArrayList<Marker> arrayListExpandedMarkers = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps2);
        context = this;
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        callVehicleApi();
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        relativeMapType = (RelativeLayout) findViewById(R.id.relativeMapType);
        relativeMapType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(MapsClusterActivity.this, "");

            }
        });


    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera

    }

    private void setUpCluster(List<Datum> list) {
        // Position the map.
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Double.parseDouble(list.get(0).getLat()), Double.parseDouble(list.get(0).getLng())), 5));

        // Initialize the manager with the context and the map.
        // (Activity extends context, so we can pass 'this' in the constructor.)
        mClusterManager = new ClusterManager<Datum>(this, mMap);

        // Point the map's listeners at the listeners implemented by the cluster
        // manager.
        mMap.setOnCameraIdleListener(mClusterManager);
        mMap.setOnMarkerClickListener(mClusterManager);
        mClusterManager.setRenderer(new CarRender(mMap));
        mClusterManager.setOnClusterClickListener(this);
        mClusterManager.setOnClusterInfoWindowClickListener(this);
        mMap.setInfoWindowAdapter(mClusterManager.getMarkerManager());
        mMap.setOnInfoWindowClickListener(mClusterManager); //added
        mClusterManager.setOnClusterItemClickListener(this);
        mClusterManager.setOnClusterItemInfoWindowClickListener(this);
        mClusterManager.getMarkerCollection().setOnInfoWindowAdapter(
                new MyCustomAdapterForItems());

        // Add cluster items (markers) to the cluster manager.
        addItems(list);
    }

    private void addItems(List<Datum> list) {
        for (int i = 0; i < list.size(); i++) {
            mClusterManager.addItem(list.get(i));
        }

    }


    private void callVehicleApi() {
        if (NetworkUtil.isOnline(context)) {
            HashMap<String, String> params = new HashMap<>();
            params.put("key", SharedPreferenceUtil.getString(Constants.SP_USER_KEY, ""));
            params.put("cmd", "ALL,*");
            new HttpRequestSingleton(context,getString(R.string.api_base_url)+ getString(R.string.api_vehicle_api), params, 100, new HttpCallback() {
                @Override
                public void onResponse(String response, int action) {
                    if (response != null) {
                        Log.i("response", "" + response.toString());
                        switch (action) {
                            case 100:
                                ObjectMapper mapper = new ObjectMapper();
                                try {
                                    VehicleResponse vehicleResponse = mapper.readValue(response, VehicleResponse.class);
                                    setUpCluster(sortResponse(vehicleResponse));


                                } catch (JsonProcessingException e) {
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }

                                break;
                            default:
                        }

                    } else {
                        Toast.makeText(context, "Something Went Wrong", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onError(VolleyError error, int action) {

                }
            });

        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_LONG).show();
        }
    }

    private List<Datum> sortResponse(VehicleResponse response) {
        List<Datum> list = new ArrayList<>();

        for (Datum vehicle : response.getData()
                ) {
            if (vehicle.getActive().equalsIgnoreCase("true")) {
                list.add(vehicle);

            } else {

            }

        }

        return list;
    }

    @Override
    public boolean onClusterClick(Cluster<Datum> cluster) {

        // Show a toast with some info when the cluster is clicked.
        String firstName = cluster.getItems().iterator().next().getName();
        Toast.makeText(this, cluster.getSize() + " (including " + firstName + ")", Toast.LENGTH_SHORT).show();

        // Zoom in the cluster. Need to create LatLngBounds and including all the cluster items
        // inside of bounds, then animate to center of the bounds.

        // Create the builder to collect all essential cluster items for the bounds.
        LatLngBounds.Builder builder = LatLngBounds.builder();
        for (ClusterItem item : cluster.getItems()) {
            builder.include(item.getPosition());
        }
        // Get the LatLngBounds
        final LatLngBounds bounds = builder.build();

        // Animate camera to the bounds
        try {
            mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return true;

    }

    @Override
    public void onClusterInfoWindowClick(Cluster<Datum> cluster) {

    }

    @Override
    public boolean onClusterItemClick(Datum datum) {
        clickedClusterItem = datum;
        SharedPreferenceUtil.putValue(Constants.SP_IMEI,datum.getImei());
        SharedPreferenceUtil.save();
        Intent intent = new Intent(context,MapsActivity.class);
        startActivity(intent);
        return false;
    }

    @Override
    public void onClusterItemInfoWindowClick(Datum datum) {





    }

    private class CarRender extends DefaultClusterRenderer<Datum> {
        private final IconGenerator mIconGenerator = new IconGenerator(getApplicationContext());
        private final IconGenerator mClusterIconGenerator = new IconGenerator(getApplicationContext());
        private final ImageView mImageView;
        private final ImageView mClusterImageView;
        private final int mDimension;

        public CarRender(GoogleMap map) {
            super(getApplicationContext(), map, mClusterManager);

            View multiProfile = getLayoutInflater().inflate(R.layout.multi_profile, null);
            mClusterIconGenerator.setContentView(multiProfile);
            mClusterImageView = (ImageView) multiProfile.findViewById(R.id.image);

            mImageView = new ImageView(getApplicationContext());
            mDimension = (int) getResources().getDimension(R.dimen.custom_profile_image);
            mImageView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            int padding = (int) getResources().getDimension(R.dimen.custom_profile_padding);
            mImageView.setPadding(0, 0, 0, 0);
            mIconGenerator.setContentView(mImageView);

        }


        @Override
        protected void onBeforeClusterItemRendered(Datum person, MarkerOptions markerOptions) {
            // Draw a single person.
            // Set the info window to show their name.
            IconGenerator iconFactory = new IconGenerator(context);


            super.onBeforeClusterItemRendered(person, markerOptions);
//            mImageView.setImageResource(R.drawable.marker_car);
//            Bitmap icon = mIconGenerator.makeIcon();
//

//            markerOptions
//                    .icon(BitmapDescriptorFactory.fromBitmap(writeOnDrawable(context,R.drawable.marker_car,person.getName()))).title(person.getName());
            markerOptions
                    .icon(BitmapDescriptorFactory.fromBitmap(fillCustomizedMarker(person.getName(),person.getIcon(),person.getStatus()))).title(person.getName());
            markerOptions.rotation(Float.valueOf(person.getAngle()));
            LatLng latLng = new LatLng(Double.valueOf(Double.parseDouble(person.getLat())),Double.parseDouble(person.getLng()));


//
//            Marker marker1 =mMap.addMarker(new MarkerOptions().position(latLng).anchor(0.5f,0.5f));
//            marker1. setIcon(BitmapDescriptorFactory.fromBitmap(iconFactory.makeIcon(person.getName())));

        }

        @Override
        protected void onBeforeClusterRendered(Cluster<Datum> cluster, MarkerOptions markerOptions) {




            super.onBeforeClusterRendered(cluster, markerOptions);
            Log.i("onBeforeClusterRendered","called");

        }

        @Override
        protected boolean shouldRenderAsCluster(Cluster cluster) {
            // Always render clusters.

            return cluster.getSize() > 1;
        }

        @Override
        protected void onClusterRendered(Cluster<Datum> cluster, Marker marker) {
            super.onClusterRendered(cluster, marker);
            Log.i("onClusterRendered","called");

        }

        @Override
        protected void onClusterItemRendered(Datum clusterItem, Marker marker) {


            if(!arrayList.contains(marker))
            {
                arrayList.add(marker);
                arrayListItem.add(clusterItem);
            }





            Log.i("onClusterItemRendered","Called");
            clickedClusterItem =clusterItem;
            IconGenerator iconFactory = new IconGenerator(context);
//            for (int i = 0; i <arrayList.size() ; i++) {
//                Marker marker1 =mMap.addMarker(new MarkerOptions().position(arrayList.get(i).getPosition()).anchor(0.5f,0.5f));
//                marker1. setIcon(BitmapDescriptorFactory.fromBitmap(iconFactory.makeIcon(arrayListItem.get(i).getName())));
//                if(!arrayListExpanded.contains(arrayListItem.get(i).getName()))
//                {
//                    arrayListExpanded.add(arrayListItem.get(i).getName());
//                    arrayListExpandedMarkers.add(marker1);
//                }
//                else
//                {
//                    marker1.remove();
//                }
//
//
//
//
//            }

            super.onClusterItemRendered(clusterItem, marker);
        }
    }


    public void showDialog(Activity activity, String msg) {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        Window window = dialog.getWindow();
        window.setGravity(Gravity.TOP);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.layout_custom);
        RadioGroup radioGroup;

        radioGroup = (RadioGroup) dialog.findViewById(R.id.radioGroup);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton rb = (RadioButton) group.findViewById(checkedId);
                if (null != rb && checkedId > -1) {

                    switch (rb.getText().toString().toLowerCase()) {
                        case "normal":
                            setCustomMap(GoogleMap.MAP_TYPE_NORMAL);
                            break;
                        case "satellite":
                            setCustomMap(GoogleMap.MAP_TYPE_SATELLITE);
                            break;
                        case "hybrid":
                            setCustomMap(GoogleMap.MAP_TYPE_TERRAIN);
                            break;
                        default:
                            setCustomMap(GoogleMap.MAP_TYPE_NORMAL);
                            break;
                    }
                    dialog.cancel();
//                    Toast.makeText(MainActivity.this, rb.getText(), Toast.LENGTH_SHORT).show();
                }

            }
        });


        dialog.show();

    }

    private void setCustomMap(int mapType) {
        if (mMap != null) {

            mMap.setMapType(mapType);

        }

    }


    public class MyCustomAdapterForItems implements GoogleMap.InfoWindowAdapter {

        private final View myContentsView;

        MyCustomAdapterForItems() {
            myContentsView = getLayoutInflater().inflate(
                    R.layout.info_window, null);
        }
        @Override
        public View getInfoWindow(Marker marker) {



            return null;
        }

        @Override
        public View getInfoContents(Marker marker) {
//

            Log.i("Custom Marker","Called");
            TextView tvTitle = ((TextView) myContentsView
                    .findViewById(R.id.txtTitle));
            TextView tvSnippet = ((TextView) myContentsView
                    .findViewById(R.id.txtSnippet));

            tvTitle.setText(clickedClusterItem.getName());

            return myContentsView;
        }
    }

    public static BitmapDrawable writeOnDrawable(Context context, int drawableId, String text){

        Bitmap bm = BitmapFactory.decodeResource(context.getResources(), drawableId).copy(Bitmap.Config.ARGB_8888, true);

        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.WHITE);
        paint.setTextSize(30);
        paint.setTextAlign(Paint.Align.CENTER);
        Canvas canvas = new Canvas(bm);

        // Change the position of text here
        canvas.drawText(text,bm.getWidth()/2 //x position
                , bm.getHeight()/2  // y position
                , paint);
        return new BitmapDrawable(context.getResources(),bm);
    }

    private Bitmap fillCustomizedMarker(String text, String icon, String status) {
        //Log.d(TAG, "fillMarkerWithText ++");
        View markerLayout;


        markerLayout = this.getLayoutInflater().inflate(R.layout.custom_marker, null);
        markerLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));


        markerLayout.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        markerLayout.layout(0, 0, markerLayout.getMeasuredWidth(), markerLayout.getMeasuredHeight());
//
        ImageView markerImage = (ImageView) markerLayout.findViewById(R.id.icon);
        TextView timeText = (TextView) markerLayout.findViewById(R.id.text);
        timeText.setText(text);
        markerImage.setImageResource(Utill.getMapIconAndColor(icon,status));
//        markerCost.setText(time);

        final Bitmap bitmap = Bitmap.createBitmap(markerLayout.getMeasuredWidth(), markerLayout.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        markerLayout.draw(canvas);

        return bitmap;
    }

}
