package com.Ktracker.pro.settings;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.Ktracker.pro.Constants;
import com.Ktracker.pro.Http.HttpCallback;
import com.Ktracker.pro.Http.HttpRequestSingleton;
import com.Ktracker.pro.Http.HttpRequestSingletonPostParams;
import com.Ktracker.pro.NetworkUtil;
import com.Ktracker.pro.R;
import com.Ktracker.pro.Utill;
import com.Ktracker.pro.base.BaseActivity;
import com.Ktracker.pro.response.settings.icon.Datum;
import com.Ktracker.pro.response.settings.icon.VehicleIconResponse;
import com.Ktracker.pro.storage.SharedPreferenceUtil;
import com.android.volley.VolleyError;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

public class VehicleIconActivity extends BaseActivity {
    private RecyclerView recyclerViewList;
    private TextView noList;
    private LinearLayoutManager linearLayoutManager;
    private FloatingActionButton fab;
    private VehicleIconAdapter childAdapter;
    private Context context;
    private boolean edit = false;
    private TextInputLayout textInputAverage,
            textInputCost;
    private EditText average,
            cost;
    Button save;
    ImageView imgClose;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fuel);
        setActionBar(true);
        setBackButtonVisibility(true);
        context = this;
        assignViews();
        setActionBarTitle("Vehicle Icon Settings");


    }

    private void assignViews() {
        recyclerViewList = (RecyclerView) findViewById(R.id.recyclerViewList);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        noList = (TextView) findViewById(R.id.noList);

    }

    private void setAdapter(List<Datum> childList) {
        if (childList.size() > 0)
            childList.remove(childList.size() - 1);
        linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        recyclerViewList.setLayoutManager(linearLayoutManager);
        recyclerViewList.setItemAnimator(new DefaultItemAnimator());
        childAdapter = new VehicleIconAdapter(context, childList);
        recyclerViewList.setAdapter(childAdapter);
        if (childList.size() > 0) {
            noList.setVisibility(View.GONE);
        } else {
            noList.setVisibility(View.VISIBLE);
        }
    }

    private void callServiceApi() {
        if (NetworkUtil.isOnline(context)) {
            HashMap<String, String> params = new HashMap<>();
            params.put("key", SharedPreferenceUtil.getString(Constants.SP_USER_KEY, ""));
            params.put("cmd", "ALL,*");
            showProgress("Loading...");
            new HttpRequestSingleton(context, getString(R.string.api_base_url) + getString(R.string.api_icon_api), params, 100, new HttpCallback() {
                @Override
                public void onResponse(String response, int action) {
                   stopProgress();
                    if (response != null) {
                        Log.i("response", "" + response.toString());
                        switch (action) {
                            case 100:
                                ObjectMapper mapper = new ObjectMapper();
                                try {
                                    VehicleIconResponse vehicleIconResponse = mapper.readValue(response, VehicleIconResponse.class);
                                    setAdapter(vehicleIconResponse.getData());


                                } catch (JsonProcessingException e) {
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }

                                break;
                            default:
                        }

                    } else {
//                        Toast.makeText(context, "Something Went Wrong", Toast.LENGTH_LONG);
                    }
                }

                @Override
                public void onError(VolleyError error, int action) {

                }
            });

        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        callServiceApi();
    }


    public void callEditApi(Datum datum) {
        if (NetworkUtil.isOnline(context)) {
            showProgress("Loading...");
            HashMap<String, String> params = new HashMap<>();
            http:
//trackerapi.com/fuel.php?insert=358657100047484&average=23&rate=93


            params.put("insert", datum.getImei());
            params.put("icon", datum.getIcon());


            new HttpRequestSingletonPostParams(context, getString(R.string.api_base_url) + getString(R.string.api_icon_api), params, 101, new HttpCallback() {
                @Override
                public void onResponse(String response, int action) {
                    stopProgress();
                    if (response != null) {
                        Log.i("response", "" + response.toString());
                        switch (action) {
                            case 101:
                                try {
                                    JSONObject object = new JSONObject(response);
                                    String success = object.getString("success");
                                    String message = object.getString("message");
                                    if (success.equalsIgnoreCase("true")) {
                                        Toast.makeText(context, "" + message, Toast.LENGTH_SHORT).show();
                                        callServiceApi();
                                    } else {
                                        Toast.makeText(context, "" + message, Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    Toast.makeText(context, "Something went wrong", Toast.LENGTH_SHORT).show();
                                    e.printStackTrace();
                                }


                                break;
                            default:
                        }

                    } else {
                        Toast.makeText(context, "Something Went Wrong", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onError(VolleyError error, int action) {

                }
            });

        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_LONG).show();
        }
    }

    public void showEditDialog(final Datum item) {
        final Dialog dialog = new Dialog(context);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        dialog.setContentView(R.layout.icon_edit);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.TOP;
        wlp.y = 250;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
        window.setAttributes(wlp);

        TextView name;
        final Spinner spinner;




        imgClose = (ImageView) dialog.findViewById(R.id.imgClose);
        spinner = (Spinner) dialog.findViewById(R.id.spinner);
        name = (TextView) dialog.findViewById(R.id.name);
        save = (Button) dialog.findViewById(R.id.save);
        name.setText(item.getName());

        final List<Icon> list = Utill.getIconNameList();

            CustomIconSpinnerAdapter adapter = new CustomIconSpinnerAdapter(this,
                    R.layout.spinner_icon_item, R.id.titleText, list);
            spinner.setAdapter(adapter);
            spinner.setSelected(true);
        for (int i = 0; i <list.size() ; i++) {
            if(list.get(i).getName().equalsIgnoreCase(item.getIcon()))
            {
                spinner.setSelection(i);
            }

        }

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    dialog.dismiss();
                    item.setIcon(list.get(spinner.getSelectedItemPosition()).getName());

                    callEditApi(item);
            }
        });
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }



}