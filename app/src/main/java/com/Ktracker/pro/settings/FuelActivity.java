package com.Ktracker.pro.settings;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.Ktracker.pro.Constants;
import com.Ktracker.pro.Http.HttpCallback;
import com.Ktracker.pro.Http.HttpRequestSingleton;
import com.Ktracker.pro.Http.HttpRequestSingletonPostParams;
import com.Ktracker.pro.NetworkUtil;
import com.Ktracker.pro.R;
import com.Ktracker.pro.base.BaseActivity;
import com.Ktracker.pro.response.settings.fuel.Datum;
import com.Ktracker.pro.response.settings.fuel.FuelResponse;
import com.Ktracker.pro.storage.SharedPreferenceUtil;
import com.android.volley.VolleyError;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

public class FuelActivity extends BaseActivity {
    private RecyclerView recyclerViewList;
    private TextView noList;
    private LinearLayoutManager linearLayoutManager;
    private FloatingActionButton fab;
    private FuelAdapter childAdapter;
    private Context context;
    private boolean edit=false;
    private  TextInputLayout textInputAverage,
    textInputCost;
    private EditText average,
    cost;
    Button save;
    ImageView imgClose;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fuel);
        setActionBar(true);
        setBackButtonVisibility(true);
        context = this;
        assignViews();
        setActionBarTitle("Average & Fuel Rate");


    }

    private void assignViews() {
        recyclerViewList = (RecyclerView) findViewById(R.id.recyclerViewList);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        noList = (TextView) findViewById(R.id.noList);

    }

    private void setAdapter(List<Datum> childList) {
        linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        recyclerViewList.setLayoutManager(linearLayoutManager);
        recyclerViewList.setItemAnimator(new DefaultItemAnimator());
        childAdapter = new FuelAdapter(context, childList);
        recyclerViewList.setAdapter(childAdapter);
        if(childList.size()>0)
        {
            noList.setVisibility(View.GONE);
        }
        else
        {
            noList.setVisibility(View.VISIBLE);
        }
    }

    private void callServiceApi() {
        if (NetworkUtil.isOnline(context)) {
            showProgress("Loading...");
            HashMap<String, String> params = new HashMap<>();
            params.put("key", SharedPreferenceUtil.getString(Constants.SP_USER_KEY, ""));
            params.put("cmd", "ALL,*");
            new HttpRequestSingleton(context, getString(R.string.api_base_url) + getString(R.string.api_fuel_api), params, 100, new HttpCallback() {
                @Override
                public void onResponse(String response, int action) {
                    stopProgress();
                    if (response != null) {
                        Log.i("response", "" + response.toString());
                        switch (action) {
                            case 100:
                                ObjectMapper mapper = new ObjectMapper();
                                try {
                                    FuelResponse fuelResponse = mapper.readValue(response, FuelResponse.class);
                                    setAdapter(fuelResponse.getData());


                                } catch (JsonProcessingException e) {
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }

                                break;
                            default:
                        }

                    } else {
//                        Toast.makeText(context, "Something Went Wrong", Toast.LENGTH_LONG);
                    }
                }

                @Override
                public void onError(VolleyError error, int action) {

                }
            });

        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        callServiceApi();
    }


    public void callEditApi(Datum datum) {
        if (NetworkUtil.isOnline(context)) {
            showProgress("Loading...");
            HashMap<String, String> params = new HashMap<>();
            http://trackerapi.com/fuel.php?insert=358657100047484&average=23&rate=93


            params.put("insert", datum.getImei());
            params.put("average", average.getText().toString());
            params.put("rate", cost.getText().toString());

            new HttpRequestSingletonPostParams(context, getString(R.string.api_base_url) + getString(R.string.api_fuel_api), params, 101, new HttpCallback() {
                @Override
                public void onResponse(String response, int action) {
                    stopProgress();
                    if (response != null) {
                        Log.i("response", "" + response.toString());
                        switch (action) {
                            case 101:
                                try {
                                    JSONObject object = new JSONObject(response);
                                    String success = object.getString("success");
                                    String message = object.getString("message");
                                    if (success.equalsIgnoreCase("true")) {
                                        Toast.makeText(context, "" + message, Toast.LENGTH_SHORT).show();
                                        callServiceApi();
                                    } else {
                                        Toast.makeText(context, "" + message, Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    Toast.makeText(context, "Something went wrong", Toast.LENGTH_SHORT).show();
                                    e.printStackTrace();
                                }


                                break;
                            default:
                        }

                    } else {
                        Toast.makeText(context, "Something Went Wrong", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onError(VolleyError error, int action) {

                }
            });

        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_LONG).show();
        }
    }

    public  void showEditDialog(final Datum item)
    {
        final Dialog dialog = new Dialog(context);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        dialog.setContentView(R.layout.fuel_edit);
        TextView name;



        textInputAverage = (TextInputLayout) dialog.findViewById(R.id.textInputAverage);
        average = (TextInputEditText) dialog.findViewById(R.id.average);
        textInputCost = (TextInputLayout) dialog.findViewById(R.id.textInputCost);
        cost = (TextInputEditText) dialog.findViewById(R.id.cost);
        cost = (TextInputEditText) dialog.findViewById(R.id.cost);
        imgClose = (ImageView) dialog.findViewById(R.id.imgClose);
        name = (TextView) dialog.findViewById(R.id.name);
        save = (Button) dialog.findViewById(R.id.save);
        name.setText(item.getName());
        average.setText(item.getAverage());
        cost.setText(item.getRate());
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateFields()) {
                    dialog.dismiss();
                    callEditApi(item);
                }
            }
        });
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }
    private boolean validateFields(){

        textInputCost.setErrorEnabled(false);
        textInputAverage.setErrorEnabled(false);
            if (average.getText().toString().trim().length() == 0) {
                textInputAverage.setErrorEnabled(true);
                textInputAverage.setError("Field can not be blank");

                return false;
            }

            else if (cost.getText().toString().trim().length() == 0) {
                textInputCost.setErrorEnabled(true);
                textInputCost.setError("Field can not be blank");
                return false;
            }  else {
                return true;
            }
        }

}