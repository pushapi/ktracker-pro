package com.Ktracker.pro.settings;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.Ktracker.pro.R;
import com.Ktracker.pro.response.settings.fuel.Datum;

import java.util.List;

public class FuelAdapter extends RecyclerView.Adapter<FuelAdapter.ViewHolder> {
    Context context;
    List<Datum> itemList;
    FuelActivity activity;


    public FuelAdapter(Context context, List<Datum> itemList) {
        this.context = context;
        this.itemList = itemList;
        activity = (FuelActivity) context;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_fuel_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {

        final Datum item = itemList.get(position);
        holder.textName.setText(item.getName());
        holder.textLiter.setText("Average: "+item.getAverage()+" km");
    holder.textCost.setText("Cost: ₹"+item.getRate()+ "/liter");
    holder.edt.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            activity.showEditDialog(item);
        }
    });



    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {


        private CardView cardView;
        private TextView textName;
        private TextView textLiter;
        private TextView textCost;
        private Button edt;

        private void assignViews(View itemView) {
            cardView = (CardView) itemView.findViewById(R.id.card_view);
            textName = (TextView) itemView.findViewById(R.id.textName);
            textLiter = (TextView) itemView.findViewById(R.id.textLiter);
            textCost = (TextView) itemView.findViewById(R.id.textCost);
            edt = (Button) itemView.findViewById(R.id.edt);
        }



        public ViewHolder(View itemView) {
            super(itemView);
            assignViews(itemView);

        }

    }
}
