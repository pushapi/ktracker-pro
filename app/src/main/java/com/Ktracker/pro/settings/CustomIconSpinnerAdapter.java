package com.Ktracker.pro.settings;


import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.Ktracker.pro.R;
import com.Ktracker.pro.Utill;
import com.Ktracker.pro.response.vehicle.Datum;

import java.util.List;

public class CustomIconSpinnerAdapter extends ArrayAdapter<Icon> {

    LayoutInflater flater;

    public CustomIconSpinnerAdapter(Activity context, int resouceId, int textviewId, List<Icon> list){

        super(context,resouceId,textviewId, list);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        return rowview(convertView,position);
    }

    @Override
    public View getDropDownView(int position, View convertView,  ViewGroup parent) {

        return rowview(convertView,position);
    }

    private View rowview(View convertView , int position){

        Icon item = getItem(position);

        viewHolder holder ;
        View rowview = convertView;
        if (rowview==null) {

            holder = new viewHolder();
            flater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowview = flater.inflate(R.layout.spinner_icon_item, null, false);

            holder.txtTitle = (TextView) rowview.findViewById(R.id.titleText);
            holder.imageView = (ImageView) rowview.findViewById(R.id.icon);
            rowview.setTag(holder);
        }else{
            holder = (viewHolder) rowview.getTag();
        }

        holder.txtTitle.setText(item.getName());
        holder.imageView.setImageResource(item.icon);


        return rowview;
    }

    private class viewHolder{
        TextView txtTitle;
        ImageView imageView;
    }
}