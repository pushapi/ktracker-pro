package com.Ktracker.pro.settings;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.Toast;

import com.Ktracker.pro.Constants;
import com.Ktracker.pro.Http.HttpCallback;
import com.Ktracker.pro.Http.HttpRequestSingletonPost;
import com.Ktracker.pro.Http.HttpRequestSingletonPostParams;
import com.Ktracker.pro.LoginActivity;
import com.Ktracker.pro.NetworkUtil;
import com.Ktracker.pro.R;
import com.Ktracker.pro.base.BaseActivity;
import com.Ktracker.pro.home.MainActivity;
import com.Ktracker.pro.response.forgot.ForgotResponse;
import com.Ktracker.pro.storage.SharedPreferenceUtil;
import com.android.volley.VolleyError;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.HashMap;

public class SettingActivity extends BaseActivity implements View.OnClickListener {
    private EditText edtdash, edtlive;
    private Button save, changepwd;
    private Context context;
    private Switch switchNotification;

    private TextInputLayout textInputCurrentPwd;
    private TextInputEditText currentPwd;
    private TextInputLayout textInputNewPwd;
    private TextInputEditText newPwd;
    private TextInputLayout textInputConfirmPwd;
    private TextInputEditText confirmPwd;
    private ImageView imgClose;
    private LinearLayout llMialage, llicon;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        context = this;
        setActionBar(true);
        setBackButtonVisibility(true);
        assignViews();
        setListners();


    }

    private void setListners() {
        save.setOnClickListener(this);
        changepwd.setOnClickListener(this);
        llMialage.setOnClickListener(this);
        llicon.setOnClickListener(this);

    }

    private void assignViews() {
        edtlive = (EditText) findViewById(R.id.edtlive);
        edtdash = (EditText) findViewById(R.id.edtdash);
        llMialage = (LinearLayout) findViewById(R.id.llMialage);
        llicon = (LinearLayout) findViewById(R.id.llicon);
        save = (Button) findViewById(R.id.save);
        changepwd = (Button) findViewById(R.id.changepwd);
        switchNotification = (Switch) findViewById(R.id.switchNotification);
        if (SharedPreferenceUtil.getBoolean(Constants.SP_IS_USER_PREFERENCE_NOTIFICATION, true)) {
            switchNotification.setChecked(true);
        } else {
            switchNotification.setChecked(false);
        }

        edtdash.setText(String.valueOf(SharedPreferenceUtil.getInt(Constants.SP_IS_USER_PREFERENCE_DASHBOARD, Constants.PREFERENCE_DASHBOARD_DEFAULT)));
        edtlive.setText(String.valueOf(SharedPreferenceUtil.getInt(Constants.SP_IS_USER_PREFERENCE_LIVE, Constants.PREFERENCE_LIVE_DEFAULT)));

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.save:
                if (isValidField()) {
                    SharedPreferenceUtil.putValue(Constants.SP_IS_USER_PREFERENCE_LIVE, Integer.valueOf(edtlive.getText().toString()));
                    SharedPreferenceUtil.putValue(Constants.SP_IS_USER_PREFERENCE_DASHBOARD, Integer.valueOf(edtdash.getText().toString()));
                    if (switchNotification.isChecked()) {
                        SharedPreferenceUtil.putValue(Constants.SP_IS_USER_PREFERENCE_NOTIFICATION, true);
                        deviceTokenApiOn();

                    } else {
                        SharedPreferenceUtil.putValue(Constants.SP_IS_USER_PREFERENCE_NOTIFICATION, false);
                        deviceTokenApiOff();
                    }

                    SharedPreferenceUtil.save();
                    Toast.makeText(context, "Preferences Saved", Toast.LENGTH_SHORT).show();


                }

                break;

            case R.id.changepwd:

                showDialog();
                break;

            case R.id.llMialage:
                Intent intent = new Intent(context, FuelActivity.class);
                startActivity(intent);


                break;
            case R.id.llicon:
                intent = new Intent(context, VehicleIconActivity.class);
                startActivity(intent);


                break;
        }
    }

    private void showDialog() {

        final Dialog dialog = new Dialog(context);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        dialog.setContentView(R.layout.change_password);

        Button btnChangePassword;


        textInputCurrentPwd = (TextInputLayout) dialog.findViewById(R.id.textInputCurrentPwd);
        currentPwd = (TextInputEditText) dialog.findViewById(R.id.currentPwd);
        textInputNewPwd = (TextInputLayout) dialog.findViewById(R.id.textInputNewPwd);
        newPwd = (TextInputEditText) dialog.findViewById(R.id.newPwd);
        textInputConfirmPwd = (TextInputLayout) dialog.findViewById(R.id.textInputConfirmPwd);
        confirmPwd = (TextInputEditText) dialog.findViewById(R.id.confirmPwd);
        imgClose = (ImageView) dialog.findViewById(R.id.imgClose);
        btnChangePassword = (Button) dialog.findViewById(R.id.btnChangePassword);
        btnChangePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validatePassword()) {
                    callChangePasswordApi(dialog);
                }
            }
        });
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();


//

    }

    private void callChangePasswordApi(final Dialog dialog) {
        if (NetworkUtil.isOnline(context)) {
            HashMap<String, String> params = new HashMap<>();
            params.put("u", SharedPreferenceUtil.getString(Constants.SP_USERNAME, ""));
            params.put("cp", currentPwd.getText().toString().trim());
            params.put("np", newPwd.getText().toString().trim());
            new HttpRequestSingletonPostParams(context, getString(R.string.api_base_url) + getString(R.string.api_changepw), params, Constants.API_FORGOT, new HttpCallback() {
                @Override
                public void onResponse(String response, int action) {
                    stopProgress();
                    if (response != null) {
                        Log.i("response", "" + response.toString());
                        switch (action) {
                            case 500:
                                try {
                                    textInputCurrentPwd.setErrorEnabled(false);
                                    ObjectMapper obj = new ObjectMapper();
                                    ForgotResponse forgotResponse = obj.readValue(response, ForgotResponse.class);
                                    if (forgotResponse.getData().get(0).getLogin().equalsIgnoreCase("true")) {
                                        dialog.dismiss();

                                        doLogoutProcess();


                                    } else {
                                        Toast.makeText(context, "" + forgotResponse.getData().get(0).getIssue(), Toast.LENGTH_SHORT).show();
                                        textInputCurrentPwd.setErrorEnabled(true);
                                        textInputCurrentPwd.setError("Invalid Password");

                                    }


                                } catch (JsonParseException e) {
                                    e.printStackTrace();
                                } catch (JsonMappingException e) {
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }


                                break;
                            default:
                        }

                    } else {
                        Toast.makeText(context, "Something Went Wrong", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onError(VolleyError error, int action) {

                }


            });
        } else {
            Toast.makeText(context, getString(R.string.internet_not_available), Toast.LENGTH_SHORT);

        }
    }

    private void deviceTokenApiOff() {
        if (NetworkUtil.isOnline(context)) {

            HashMap<String, String> params = new HashMap<>();

            params.put("firebase_token", SharedPreferenceUtil.getString(Constants.SP_FIREBASE_TOKEN, ""));
            params.put("function_name", "notify");


            new HttpRequestSingletonPost(context, getString(R.string.api_token_api), params, 320, new HttpCallback() {
                @Override
                public void onResponse(String response, int action) {
                    if (action == 320)
                        Log.i("response", response.toString());
                    stopProgress();
                    Intent intent = new Intent(context, MainActivity.class);
                    startActivity(intent);
                    finish();

                }


                @Override
                public void onError(VolleyError error, int action) {

                }
            });
        } else {
            Toast.makeText(context, getString(R.string.internet_not_available), Toast.LENGTH_SHORT);

        }

    }

    private void doLogoutProcess() {
        String token = SharedPreferenceUtil.getString(Constants.SP_FIREBASE_TOKEN, "");
        SharedPreferenceUtil.clear();
        SharedPreferenceUtil.save();
        SharedPreferenceUtil.putValue(Constants.SP_FIREBASE_TOKEN, token);
        SharedPreferenceUtil.save();
        Toast.makeText(context, "Password changed Successfully. Please Re-Login", Toast.LENGTH_LONG).show();
        Intent intent = new Intent(context, LoginActivity.class);
        if (Build.VERSION.SDK_INT >= 11) {
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        } else {
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        }
        startActivity(intent);
    }

    private boolean validatePassword() {

        textInputCurrentPwd.setErrorEnabled(false);
        textInputNewPwd.setErrorEnabled(false);
        textInputConfirmPwd.setErrorEnabled(false);
        if (currentPwd.getText().toString().trim().length() == 0) {
            textInputCurrentPwd.setErrorEnabled(true);
            textInputCurrentPwd.setError("Please enter current password");

            return false;
        }

//        else if (!currentPwd.getText().toString().trim().matches(SharedPreferenceUtil.getString(Constants.SP_PASSWORD,""))) {
//            textInputCurrentPwd.setErrorEnabled(true);
//            textInputCurrentPwd.setError("Invalid Password");
//
//            return false;
//        }
        else if (newPwd.getText().toString().trim().length() == 0) {
            textInputNewPwd.setErrorEnabled(true);
            textInputNewPwd.setError("Please enter new password");
            return false;
        } else if (confirmPwd.getText().toString().trim().length() == 0) {
            textInputConfirmPwd.setErrorEnabled(true);
            textInputConfirmPwd.setError("Please enter current password");
            return false;
        } else if (!confirmPwd.getText().toString().trim().matches(newPwd.getText().toString().trim())) {
            textInputConfirmPwd.setErrorEnabled(true);
            textInputConfirmPwd.setError("Confirm password does not match");
            return false;
        } else {
            return true;
        }
    }


    private boolean isValidField() {
        if (edtlive.getText().length() == 0) {
            Toast.makeText(context, "Please Enter Live Track Update Time", Toast.LENGTH_LONG).show();
            return false;
        } else if (Long.valueOf(edtlive.getText().toString()) < 10) {
            Toast.makeText(context, "Live Track update time should be Greater than 10", Toast.LENGTH_LONG).show();
            return false;
        } else if (edtdash.getText().length() == 0) {
            Toast.makeText(context, "Please Enter Dashboard  Update Time", Toast.LENGTH_LONG).show();
            return false;
        } else if (Long.valueOf(edtdash.getText().toString()) < 30) {
            Toast.makeText(context, "Dashboard update time should be Greater than 30", Toast.LENGTH_LONG).show();
            return false;
        } else {
            return true;
        }

    }

    private void deviceTokenApiOn() {
        if (NetworkUtil.isOnline(context)) {

            showProgress("Loading..");
            HashMap<String, String> params = new HashMap<>();
            params.put("user_id", SharedPreferenceUtil.getString(Constants.SP_USER_ID, ""));
            params.put("user", SharedPreferenceUtil.getString(Constants.SP_USERNAME, ""));
            params.put("pass", SharedPreferenceUtil.getString(Constants.SP_PASSWORD, ""));
            params.put("number", "test");
            params.put("message", "test");
            params.put("device_token", "test");
            params.put("firebase_token", SharedPreferenceUtil.getString(Constants.SP_FIREBASE_TOKEN, ""));
            params.put("model", Build.BRAND);
            params.put("version", Build.VERSION.RELEASE);
            params.put("api_level", Build.VERSION.RELEASE);
            params.put("company_name", Build.MANUFACTURER);
            params.put("type", "android");
            params.put("imei", SharedPreferenceUtil.getString(Constants.SP_IMEI, ""));
            params.put("function_name", "deviceinfo");


            new HttpRequestSingletonPost(context, getString(R.string.api_token_api), params, 327, new HttpCallback() {
                @Override
                public void onResponse(String response, int action) {

                    if (action == 327) {

                        Log.i("response", response.toString());
                        stopProgress();
                        Intent intent = new Intent(context, MainActivity.class);
                        startActivity(intent);
                        finish();
                    }


                }


                @Override
                public void onError(VolleyError error, int action) {

                }
            });
        } else {
            Toast.makeText(context, getString(R.string.internet_not_available), Toast.LENGTH_SHORT).show();

        }

    }

}
