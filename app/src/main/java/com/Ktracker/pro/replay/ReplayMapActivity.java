package com.Ktracker.pro.replay;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.SystemClock;
import androidx.fragment.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.Ktracker.pro.Constants;
import com.Ktracker.pro.Http.HttpCallback;
import com.Ktracker.pro.Http.HttpRequestSingleton;
import com.Ktracker.pro.NetworkUtil;
import com.Ktracker.pro.R;
import com.Ktracker.pro.Utill;
import com.Ktracker.pro.location.CustomVO;
import com.Ktracker.pro.location.LocationActivity;
import com.Ktracker.pro.response.distance.DistanceResponse;
import com.Ktracker.pro.response.reply.Datum;
import com.Ktracker.pro.response.reply.ReplyResponse;
import com.Ktracker.pro.response.stop.StopResponse;
import com.Ktracker.pro.storage.SharedPreferenceUtil;
import com.android.volley.VolleyError;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ReplayMapActivity extends FragmentActivity implements OnMapReadyCallback, View.OnClickListener, GoogleMap.OnInfoWindowClickListener {

    private GoogleMap mMap;
    private Polyline line;
    private List<LatLng> points = new ArrayList<>();
    private List<Datum> listAll = new ArrayList<>();
    private List<Datum> listFromFastForward = new ArrayList<>();
    private Marker marker1;
    private Marker marker2;
    private Marker marker3;
    private Context context;
    private boolean isFastForWardRequested = false;
    private int positionWhenFastForwardRequested = 0;
    private Handler handler;

    private ImageView mRelativePlay;
    private ImageView mRelativePause;
    private TextView mRelative1X;
    private TextView mRelative2X;
    private TextView mRelative3X,
            distance,
            km,
            movement,
            stoppage,
            idle;
    private RelativeLayout relativeMapType;
    private TextView time,speed;
    private List<Runnable> listRun = new ArrayList<>();


//    1000 5000
//500 2500
//    250 1250


    private String startDate, endDate, startTime, endTime, imei;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_replay_map);
        getValuesFromPreviousActivity();
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        assignViews();
        mapFragment.getMapAsync(this);
        context = this;


        setListeners();
    }

    private void setListeners() {
        mRelative1X.setOnClickListener(this);
        mRelative2X.setOnClickListener(this);
        mRelative3X.setOnClickListener(this);
        mRelativePlay.setOnClickListener(this);
        mRelativePause.setOnClickListener(this);
        relativeMapType.setOnClickListener(this);
    }

    private void assignViews() {
        mRelativePlay = (ImageView) findViewById(R.id.relativePlay);
        mRelativePause = (ImageView) findViewById(R.id.relativePause);
        mRelative1X = (TextView) findViewById(R.id.relative1X);
        mRelative2X = (TextView) findViewById(R.id.relative2X);
        mRelative3X = (TextView) findViewById(R.id.relative3X);
        relativeMapType = (RelativeLayout) findViewById(R.id.relativeMapType);
        speed = (TextView) findViewById(R.id.speed);
        time = (TextView) findViewById(R.id.time);
        distance = (TextView) findViewById(R.id.distance);
        movement = (TextView) findViewById(R.id.movement);
        stoppage = (TextView) findViewById(R.id.stoppage);
        idle = (TextView) findViewById(R.id.idle);
    }

    private void getValuesFromPreviousActivity() {
        startDate = getIntent().getStringExtra(Constants.INTENT_START_DATE);
        endDate = getIntent().getStringExtra(Constants.INTENT_END_DATE);
        startTime = getIntent().getStringExtra(Constants.INTENT_START_TIME);
        endTime = getIntent().getStringExtra(Constants.INTENT_END_TIME);
        imei = getIntent().getStringExtra(Constants.INTENT_IMEI);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        callReplyApi();
        callStopApi();
        callDistanceApi();

        mMap.setOnInfoWindowClickListener(this);

        line = mMap.addPolyline(new PolylineOptions()
                .width(8)
                .color(Color.RED));
    }

    private void callStopApi() {
        if (NetworkUtil.isOnline(context)) {
            HashMap<String, String> params = new HashMap<>();
//           params.put("imei", "358511021846666");
//            params.put("userkey", "A29DB84D21FA82EAF564251D777007E1");
//            params.put("startdate", "2018-11-22");
//            params.put("starttime", "00:00:00");
//            params.put("enddate", "2018-11-22");
//            params.put("endtime", "23:59:59");

            params.put("imei", imei);
            params.put("userkey", SharedPreferenceUtil.getString(Constants.SP_USER_KEY, ""));
            params.put("startdate", startDate);
            params.put("starttime", startTime);
            params.put("enddate", endDate);
            params.put("endtime", endTime);

            new HttpRequestSingleton(context, getString(R.string.api_base_url) + getString(R.string.api_stop_api), params, 102, new HttpCallback() {
                @Override
                public void onResponse(String response, int action) {
                    if (response != null) {
                        Log.i("response", "" + response.toString());
                        switch (action) {
                            case 102:
                                ObjectMapper mapper = new ObjectMapper();
                                try {

                                    List<com.Ktracker.pro.response.stop.Datum> listResponse = new ArrayList<>();
                                    com.Ktracker.pro.response.stop.Datum header = new com.Ktracker.pro.response.stop.Datum();
                                    listResponse.add(header);

                                    StopResponse movementResponse = mapper.readValue(response, StopResponse.class);
                                    listResponse.addAll(movementResponse.getData());

                                    setStopMarkers(listResponse);


                                } catch (JsonProcessingException e) {
                                    finish();
                                    Toast.makeText(context, "Invalid Data", Toast.LENGTH_SHORT).show();
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    Toast.makeText(context, "Invalid Data", Toast.LENGTH_SHORT).show();
                                    e.printStackTrace();
                                }

                                break;
                            default:
                        }

                    } else {
                        Toast.makeText(context, "Something Went Wrong", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onError(VolleyError error, int action) {

                }
            });

        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_LONG).show();
        }
    }

    private void setStopMarkers(List<com.Ktracker.pro.response.stop.Datum> listResponse) {


        for (com.Ktracker.pro.response.stop.Datum item : listResponse
        ) {

            if (item.getLat() != null && item.getLng() != null) {
                MarkerOptions markerOptions = new MarkerOptions().title("Stop Time : " + item.getDuration())
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_stop_marker)).anchor(0.5f, 0.5f).position(new LatLng(Double.parseDouble(item.getLat()), Double.parseDouble(item.getLng())));
                mMap.addMarker(markerOptions);

                mMap.setInfoWindowAdapter(new MyInfoWindowAdapter());
            }

        }
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        CustomVO customVO = new CustomVO();
        customVO.setLat(marker.getPosition().latitude);
        customVO.setLng(marker.getPosition().longitude);
        Intent intent = new Intent(context, LocationActivity.class);
        intent.putExtra(Constants.INTENT_OBJECT, customVO);
        context.startActivity(intent);
    }

    public class MyInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

        private final View myContentsView;

        MyInfoWindowAdapter() {
            myContentsView = getLayoutInflater().inflate(R.layout.custom_marker_info, null);
        }

        @Override
        public View getInfoContents(final Marker marker) {


            final TextView text = ((TextView) myContentsView.findViewById(R.id.text));
            if (NetworkUtil.isOnline(context)) {
                HashMap<String, String> params = new HashMap<>();
                params.put("lat", String.valueOf(marker.getPosition().latitude));
                params.put("long", String.valueOf(marker.getPosition().longitude));
                new HttpRequestSingleton(context, getString(R.string.api_base_url) + getString(R.string.api_live_api), params, 100, new HttpCallback() {
                    @Override
                    public void onResponse(String response, int action) {
                        if (response != null) {
                            Log.i("response", "" + response.toString());
                            if (marker.getTitle() != null)
                                text.setText(marker.getTitle() + "\n" + response.toString());
                            else
                                text.setText(response.toString());
                            if (marker.isInfoWindowShown()) {
                                marker.showInfoWindow();
                            }


                        } else {
                            Toast.makeText(context, "Something Went Wrong", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onError(VolleyError error, int action) {

                    }
                });

            } else {
                Toast.makeText(context, "No Internet Connection", Toast.LENGTH_LONG).show();
            }


            return myContentsView;
        }

        @Override
        public View getInfoWindow(Marker marker) {
            // TODO Auto-generated method stub
            return null;
        }

    }

    private void callLocationApi(final LatLng latLng) {

    }

    private void callDistanceApi() {
        if (NetworkUtil.isOnline(context)) {
            HashMap<String, String> params = new HashMap<>();


            params.put("imei", imei);
            params.put("userkey", SharedPreferenceUtil.getString(Constants.SP_USER_KEY, ""));
            params.put("startdate", startDate);
            params.put("enddate", endDate);
            params.put("starttime", startTime);
            params.put("endtime", endTime);


            new HttpRequestSingleton(context, getString(R.string.api_base_url) + getString(R.string.api_distance_api), params, 104, new HttpCallback() {
                @Override
                public void onResponse(String response, int action) {
                    if (response != null) {
                        Log.i("response", "" + response.toString());
                        switch (action) {
                            case 104:
                                ObjectMapper mapper = new ObjectMapper();
                                try {

                                    JSONArray array = new JSONArray(response);

                                    DistanceResponse distanceResponse = null;
                                    if (array.length() > 0)
                                        distanceResponse = mapper.readValue(array.get(0).toString(), DistanceResponse.class);

                                    populateSummaryParams(distanceResponse);


                                } catch (JsonProcessingException e) {
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                break;
                            default:
                        }

                    } else {
                        Toast.makeText(context, "Something Went Wrong", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onError(VolleyError error, int action) {

                }
            });

        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_LONG).show();
        }
    }

    private void populateSummaryParams(DistanceResponse distanceResponse) {
        movement.setText(Utill.convertIntoDigitalClockFormat(distanceResponse.getDrivesDurationTime()));
        stoppage.setText(Utill.convertIntoDigitalClockFormat(distanceResponse.getStopsDurationTime()));
        idle.setText(Utill.convertIntoDigitalClockFormat(distanceResponse.getEngineIdleTime()));
        distance.setText(distanceResponse.getKm() + " km");


    }


    private void callReplyApi() {

        if (NetworkUtil.isOnline(getApplicationContext())) {
            HashMap<String, String> params = new HashMap<>();

            params.put("imei", imei);
            params.put("userkey", SharedPreferenceUtil.getString(Constants.SP_USER_KEY, ""));
            params.put("startdate", startDate);
            params.put("starttime", startTime);
            params.put("enddate", endDate);
            params.put("endtime", endTime);
            String url = getString(R.string.api_base_url) + getString(R.string.api_reply_api);

            new HttpRequestSingleton(getApplicationContext(), url, params, 0, new HttpCallback() {
                @Override
                public void onResponse(String response, int action) {
                    ObjectMapper mapper = new ObjectMapper();
                    ReplyResponse data;

                    try {
                        data = mapper.readValue(response.toString(), ReplyResponse.class);
                        setpolylinePoints(data);
                        listAll = data.getData();
                        listFromFastForward.addAll(listAll);
//                        runVehicleAtSpeed(data.getData(),1000);
                    } catch (IOException e) {
                        e.printStackTrace();
                        Toast.makeText(context, "No Data Found", Toast.LENGTH_LONG).show();
                        finish();
                    }


                }

                @Override
                public void onError(VolleyError error, int action) {

                }
            });
        }

    }

    private void setpolylinePoints(ReplyResponse data) {

        Datum firstPoint = new Datum();

        for (int i = 0; i < data.getData().size(); i++) {
            if (i == 0) {
                firstPoint = data.getData().get(i);
            }
            LatLng latLng = new LatLng(Double.parseDouble(data.getData().get(i).getLat()), Double.parseDouble(data.getData().get(i).getLng()));
            points.add(latLng);

        }
        putMarkerOnFirstPosition(firstPoint);
        line.setPoints(points);
    }

    private void putMarkerOnFirstPosition(Datum firstPoint) {
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Double.parseDouble(firstPoint.getLat()), Double.parseDouble(firstPoint.getLng())), 13));
        if (marker1 != null) {
            marker1.setRotation(Float.valueOf(firstPoint.getAngle()));
        } else {
            MarkerOptions markerOptions = new MarkerOptions()
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.replay_icon)).anchor(0.5f, 0.5f).position(new LatLng(Double.parseDouble(firstPoint.getLat()), Double.parseDouble(firstPoint.getLng())));
            marker1 = mMap.addMarker(markerOptions);
            marker1.setRotation(Float.valueOf(firstPoint.getAngle()));

        }

    }


    private void runVehicleAtSpeed(final List<Datum> list, int speed, Marker mark) {


        handler = new Handler();
        for (int a = 1; a < list.size(); a++) {

            Log.i("size", "a:" + list.size());


            final int finalA = a;
            final Marker finalMark = mark;
            final int finalA1 = a;
            Runnable r = new Runnable() {

                @Override
                public void run() {

                    if (isFastForWardRequested) {
                        Log.i("FastForward", "" + finalA);
                        isFastForWardRequested = false;

                    }
                    positionWhenFastForwardRequested++;
                    finalMark.setRotation(Float.valueOf(list.get(finalA).getAngle()));

                    LatLng currentPosition = new LatLng(Double.parseDouble(list.get(finalA).getLat()), Double.parseDouble(list.get(finalA).getLng()));


//                    animateMarker(finalMark, currentPosition, false);
                    updateUI(list.get(finalA));
                    Location location = new Location(LocationManager.GPS_PROVIDER);
                    location.setLatitude(currentPosition.latitude);
                    location.setLongitude(currentPosition.longitude);
                    animateMarkerNew(location, finalMark);
                }
            };

            handler.postDelayed(r, speed * a);
            listRun.add(r);
        }


    }


    private void updateUI(Datum datum) {
        speed.setText("Speed: " + datum.getSpeed() + " km/h " );
        time.setText("Time: " + datum.getDtTracker().toString());

    }

    public void animateMarker(final Marker marker, final LatLng toPosition,
                              final boolean hideMarker) {
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        Projection proj = mMap.getProjection();
        Point startPoint = proj.toScreenLocation(marker.getPosition());
        final LatLng startLatLng = proj.fromScreenLocation(startPoint);
        final long duration = 1000;

        final Interpolator interpolator = new LinearInterpolator();

        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed
                        / duration);
                double lng = t * toPosition.longitude + (1 - t)
                        * startLatLng.longitude;
                double lat = t * toPosition.latitude + (1 - t)
                        * startLatLng.latitude;
                marker.setPosition(new LatLng(lat, lng));

                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                } else {

                }
            }
        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.relativePlay:

                mRelativePlay.setSelected(false);
                mRelativePause.setSelected(true);
                break;
            case R.id.relativePause:
                isFastForWardRequested = true;
                mRelativePlay.setSelected(false);
                if (mRelativePause.isSelected()) {
                    mRelativePause.setSelected(false);
                    pauseMarker();
                } else {
                    mRelativePause.setSelected(true);
                    runVehicleAtSpeed(listFromFastForward, 250, marker1);
                    mRelative1X.setSelected(false);
                    mRelative2X.setSelected(false);
                    mRelative3X.setSelected(true);


                }

                break;
            case R.id.relative1X:
                mRelativePause.setSelected(true);
                mRelative1X.setSelected(true);
                mRelative2X.setSelected(false);
                mRelative3X.setSelected(false);
                isFastForWardRequested = true;
                new CountDownTimer(999, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        pauseMarker();
                        runVehicleAtSpeed(listFromFastForward, 1000, marker1);
                    }
                }.start();

                break;
            case R.id.relative2X:
                mRelativePause.setSelected(true);
                mRelative1X.setSelected(false);
                mRelative2X.setSelected(true);
                mRelative3X.setSelected(false);
                isFastForWardRequested = true;


                new CountDownTimer(499, 200) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        pauseMarker();
                        runVehicleAtSpeed(listFromFastForward, 500, marker1);
                    }
                }.start();
                pauseMarker();

                break;

            case R.id.relative3X:
                mRelativePause.setSelected(true);
                isFastForWardRequested = true;
                mRelative1X.setSelected(false);
                mRelative2X.setSelected(false);
                mRelative3X.setSelected(true);
                new CountDownTimer(249, 250) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        pauseMarker();
                        runVehicleAtSpeed(listFromFastForward, 250, marker1);
                    }
                }.start();

                break;
            case R.id.relativeMapType:

                relativeMapType.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showDialog(ReplayMapActivity.this, "");

                    }
                });
                break;


            default:
                break;
        }
    }


    private void pauseMarker() {
        for (int i = 0; i < listRun.size(); i++) {
            handler.removeCallbacks(listRun.get(i));
        }
        listFromFastForward = new ArrayList<>();
        listFromFastForward = listAll.subList(positionWhenFastForwardRequested, listAll.size() - 1);
        Log.i("size", "" + listFromFastForward.size());


    }

    public void showDialog(Activity activity, String msg) {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.layout_custom);
        RadioGroup radioGroup;

        radioGroup = (RadioGroup) dialog.findViewById(R.id.radioGroup);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton rb = (RadioButton) group.findViewById(checkedId);
                if (null != rb && checkedId > -1) {

                    switch (rb.getText().toString().toLowerCase()) {
                        case "normal":
                            setCustomMap(GoogleMap.MAP_TYPE_NORMAL);
                            break;
                        case "satellite":
                            setCustomMap(GoogleMap.MAP_TYPE_SATELLITE);
                            break;
                        case "hybrid":
                            setCustomMap(GoogleMap.MAP_TYPE_HYBRID);
                            break;
                        default:
                            setCustomMap(GoogleMap.MAP_TYPE_NORMAL);
                            break;
                    }
                    dialog.cancel();
//                    Toast.makeText(MainActivity.this, rb.getText(), Toast.LENGTH_SHORT).show();
                }

            }
        });


        dialog.show();

    }

    private void setCustomMap(int mapType) {
        if (mMap != null) {


            line = mMap.addPolyline(new PolylineOptions()
                    .width(20)
                    .color(Color.BLUE));
            mMap.setMapType(mapType);


        }

    }


    private void animateMarkerNew(final Location destination, final Marker marker) {

        if (marker != null) {

            final LatLng startPosition = marker.getPosition();
            final LatLng endPosition = new LatLng(destination.getLatitude(), destination.getLongitude());

            final float startRotation = marker.getRotation();
            final LatLngInterpolatorNew latLngInterpolator = new LatLngInterpolatorNew.LinearFixed();

            ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, 1);
            valueAnimator.setDuration(500); // duration 3 second
            valueAnimator.setInterpolator(new LinearInterpolator());
            valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    try {
                        float v = animation.getAnimatedFraction();
                        LatLng newPosition = latLngInterpolator.interpolate(v, startPosition, endPosition);
                        marker.setPosition(newPosition);


//                        marker.setRotation(getBearing(startPosition, new LatLng(destination.getLatitude(), destination.getLongitude())));
                    } catch (Exception ex) {
                        //I don't care atm..
                    }
                }
            });
            valueAnimator.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);

                    mMap.animateCamera(CameraUpdateFactory.newLatLng(new LatLng(destination.getLatitude(), destination.getLongitude())));

                    // if (mMarker != null) {
                    // mMarker.remove();
                    // }
                    // mMarker = googleMap.addMarker(new MarkerOptions().position(endPosition).icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_car)));

                }
            });
            valueAnimator.start();
        }
    }

    private interface LatLngInterpolatorNew {
        LatLng interpolate(float fraction, LatLng a, LatLng b);

        class LinearFixed implements LatLngInterpolatorNew {
            @Override
            public LatLng interpolate(float fraction, LatLng a, LatLng b) {
                double lat = (b.latitude - a.latitude) * fraction + a.latitude;
                double lngDelta = b.longitude - a.longitude;
                // Take the shortest path across the 180th meridian.
                if (Math.abs(lngDelta) > 180) {
                    lngDelta -= Math.signum(lngDelta) * 360;
                }
                double lng = lngDelta * fraction + a.longitude;
                return new LatLng(lat, lng);
            }
        }
    }

    //Method for finding bearing between two points
    private float getBearing(LatLng begin, LatLng end) {
        double lat = Math.abs(begin.latitude - end.latitude);
        double lng = Math.abs(begin.longitude - end.longitude);

        if (begin.latitude < end.latitude && begin.longitude < end.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)));
        else if (begin.latitude >= end.latitude && begin.longitude < end.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 90);
        else if (begin.latitude >= end.latitude && begin.longitude >= end.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)) + 180);
        else if (begin.latitude < end.latitude && begin.longitude >= end.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 270);
        return -1;
    }
}
