package com.Ktracker.pro.replay;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.widget.AppCompatButton;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.Ktracker.pro.Utill;
import com.android.volley.VolleyError;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import com.Ktracker.pro.Constants;
import com.Ktracker.pro.Http.HttpCallback;
import com.Ktracker.pro.Http.HttpRequestSingleton;
import com.Ktracker.pro.NetworkUtil;
import com.Ktracker.pro.R;
import com.Ktracker.pro.base.BaseActivity;
import com.Ktracker.pro.response.vehicle.Datum;
import com.Ktracker.pro.response.vehicle.VehicleResponse;
import com.Ktracker.pro.slidedatetimepicker.SlideDateTimeListener;
import com.Ktracker.pro.storage.SharedPreferenceUtil;

public class TimeSelctionActivity extends BaseActivity implements View.OnClickListener {
    private TextView txtEndTime, txtEndDate, txtStartTime, txtStartDate;
    private DatePickerDialog.OnDateSetListener date;
    private TimePickerDialog mTimePicker;
    private TimePickerDialog.OnTimeSetListener time;
    final Calendar myCalendar = Calendar.getInstance();
    final Calendar myCalendarEnd = Calendar.getInstance();
    private Spinner spinner;
    private long selectedStartDate;
    private Context context;
    private Intent intent;
    private boolean isStartDateSelected = true;
    private AppCompatButton btnWatchReplay;
    private String selectedVehicleIMEINO = "";
    private List<Datum> listAllVehicles = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time_selction);
        context = this;
        assignView();
        setListeners();
        callVehicleApi();
        setActionBar(true);
        setBackButtonVisibility(true);


    }

    private void callVehicleApi() {
        if (NetworkUtil.isOnline(context)) {
            showProgress("Loading Vehicles");
            HashMap<String, String> params = new HashMap<>();
            params.put("key", SharedPreferenceUtil.getString(Constants.SP_USER_KEY, ""));
            params.put("cmd", "ALL,*");
            new HttpRequestSingleton(context, getString(R.string.api_base_url)+getString(R.string.api_vehicle_api), params, 100, new HttpCallback() {
                @Override
                public void onResponse(String response, int action) {
                    stopProgress();
                    if (response != null) {
                        Log.i("response", "" + response.toString());
                        switch (action) {
                            case 100:
                                ObjectMapper mapper = new ObjectMapper();
                                try {
                                    VehicleResponse vehicleResponse = mapper.readValue(response, VehicleResponse.class);
                                    sortResponse(vehicleResponse.getData());


                                } catch (JsonProcessingException e) {
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }

                                break;
                            default:
                        }

                    } else {
                        Toast.makeText(context, "Something Went Wrong", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onError(VolleyError error, int action) {

                }
            });

        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_LONG).show();
        }
    }

    private void sortResponse(List<Datum> allList) {
        List<Datum> list = new ArrayList<>();


        for (Datum vehicle : allList
                ) {
            if (vehicle.getActive().equalsIgnoreCase("true")) {
                list.add(vehicle);
            } else {

            }

        }
        listAllVehicles.addAll(list);
        setCustomeSpinner(list);


    }


    private void setCustomeSpinner(List<Datum> list) {
        CustomSpinnerAdapter adapter = new CustomSpinnerAdapter(this,
                R.layout.spinner_item, R.id.titleText, list);
        spinner.setAdapter(adapter);
    }

    private TimePickerDialog.OnTimeSetListener setTimeListener(final TextView textView) {

        time = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                if (hourOfDay < 10 && minute < 10) {
                    textView.setText("0" + hourOfDay + ":0" + minute + ":00");
                } else if (hourOfDay < 10)
                    textView.setText("0" + hourOfDay + ":" + minute + ":00");
                else if (minute < 10)
                    textView.setText("" + hourOfDay + ":0" + minute + ":00");
                else
                    textView.setText("" + hourOfDay + ":" + minute + ":00");


            }
        };
        return time;

    }

    private DatePickerDialog.OnDateSetListener setDateListener(final TextView textView) {
        date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                updateLabel(textView);
                if (textView == txtStartDate) {
                    selectedStartDate = myCalendar.getTimeInMillis();
                    isStartDateSelected = true;
                    updateLabel(txtEndDate);
                }
            }

        };
        return date;

    }

    private void setListeners() {
        txtStartTime.setOnClickListener(this);
        txtEndTime.setOnClickListener(this);
        txtEndDate.setOnClickListener(this);
        txtStartDate.setOnClickListener(this);
        btnWatchReplay.setOnClickListener(this);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

               selectedVehicleIMEINO= listAllVehicles.get(position).getImei();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void assignView() {
        txtEndTime = (TextView) findViewById(R.id.txtEndTime);
        txtEndDate = (TextView) findViewById(R.id.txtEndDate);
        txtStartTime = (TextView) findViewById(R.id.txtStartTime);
        txtStartDate = (TextView) findViewById(R.id.txtStartDate);
        spinner = (Spinner) findViewById(R.id.spinner);
        btnWatchReplay = (AppCompatButton) findViewById(R.id.btnWatchReplay);
        txtStartTime.setText("00:00:00");
        txtEndTime.setText("23:59:59");

        Utill.setToday(txtStartDate);
        Utill.setToday(txtEndDate);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtStartDate:

//                new SlideDateTimePicker.Builder(getSupportFragmentManager())
//                        .setListener(listener)
//                        .setInitialDate(new Date())
//                        //.setMinDate(minDate)
//                        //.setMaxDate(maxDate)
//                        //.setIs24HourTime(true)
//                        //.setTheme(SlideDateTimePicker.HOLO_DARK)
//                        //.setIndicatorColor(Color.parseColor("#990000"))
//                        .build()
//                        .show();
                DatePickerDialog start = new DatePickerDialog(TimeSelctionActivity.this, setDateListener(txtStartDate), myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH));
                start.getDatePicker().setMaxDate(System.currentTimeMillis());
                start.show();
                break;

            case R.id.txtEndDate:
                DatePickerDialog end = new DatePickerDialog(TimeSelctionActivity.this, setDateListener(txtEndDate), myCalendarEnd
                        .get(Calendar.YEAR), myCalendarEnd.get(Calendar.MONTH),
                        myCalendarEnd.get(Calendar.DAY_OF_MONTH));
                end.getDatePicker().setMinDate(selectedStartDate);
                end.getDatePicker().setMaxDate(System.currentTimeMillis());

                end.show();
                break;
            case R.id.txtEndTime:
                new TimePickerDialog(TimeSelctionActivity.this, setTimeListener(txtEndTime), 23, 59, true).show();
                break;
            case R.id.txtStartTime:
                new TimePickerDialog(TimeSelctionActivity.this, setTimeListener(txtStartTime), 00, 00, true).show();
                break;

            case R.id.btnWatchReplay:
                if(isStartDateSelected) {
                    intent = new Intent(context, ReplayMapActivity.class);
                    intent.putExtra(Constants.INTENT_START_DATE, txtStartDate.getText().toString());
                    intent.putExtra(Constants.INTENT_END_DATE, txtEndDate.getText().toString());
                    intent.putExtra(Constants.INTENT_START_TIME, txtStartTime.getText().toString());
                    intent.putExtra(Constants.INTENT_END_TIME, txtEndTime.getText().toString());
                    intent.putExtra(Constants.INTENT_IMEI, selectedVehicleIMEINO);
                    startActivity(intent);
                }
                else
            {
                Toast.makeText(context,"Please select date",Toast.LENGTH_LONG).show();
            }
                break;
            default:
                break;
        }

    }

    private void updateLabel(TextView textView) {
        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        textView.setText(sdf.format(myCalendar.getTime()));
    }

    private void updateTime(TextView textView) {
        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        textView.setText(sdf.format(myCalendar.getTime()));
    }

    private SlideDateTimeListener listener = new SlideDateTimeListener() {

        @Override
        public void onDateTimeSet(Date date)
        {
            // Do something with the date. This Date object contains
            // the date and time that the user has selected.
        }

        @Override
        public void onDateTimeCancel()
        {
            // Overriding onDateTimeCancel() is optional.
        }
    };

}
