package com.Ktracker.pro.stop;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Bundle;
import androidx.appcompat.widget.AppCompatButton;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.Ktracker.pro.Utill;
import com.android.volley.VolleyError;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import com.Ktracker.pro.Constants;
import com.Ktracker.pro.Http.HttpCallback;
import com.Ktracker.pro.Http.HttpRequestSingleton;
import com.Ktracker.pro.NetworkUtil;
import com.Ktracker.pro.R;
import com.Ktracker.pro.base.BaseActivity;
import com.Ktracker.pro.replay.CustomSpinnerAdapter;
import com.Ktracker.pro.response.stop.StopResponse;
import com.Ktracker.pro.response.vehicle.Datum;
import com.Ktracker.pro.response.vehicle.VehicleResponse;
import com.Ktracker.pro.storage.SharedPreferenceUtil;

public class StopActivity extends BaseActivity implements View.OnClickListener {
    private TextView txtEndTime, txtEndDate, txtStartTime, txtStartDate;
    private DatePickerDialog.OnDateSetListener date;
    final Calendar myCalendar = Calendar.getInstance();
    final Calendar myCalendarEnd = Calendar.getInstance();
    private boolean isStartDateSelected = true;
    private String selectedVehicleIMEINO="";
    private Spinner spinner;
    private long selectedStartDate;
    private Context context;
    private LayoutInflater inflater;
    private View view;
    private TimePickerDialog mTimePicker;
    private TimePickerDialog.OnTimeSetListener time;
    private RecyclerView recyclerViewList;
    private LinearLayoutManager linearLayoutManager;
    private StopAdapter adapter;
    private AppCompatButton btnWatchReplay;
    private List<Datum> listAllVehicles = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stop);
        context = this;
        assignView();
        setListeners();
        callVehicleApi();
        setActionBar(true);
        setBackButtonVisibility(true);

    }

    private TimePickerDialog.OnTimeSetListener setTimeListener(final TextView textView) {

        time = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                if (hourOfDay < 10 && minute < 10) {
                    textView.setText("0" + hourOfDay + ":0" + minute + ":00");
                } else if (hourOfDay < 10)
                    textView.setText("0" + hourOfDay + ":" + minute + ":00");
                else if (minute < 10)
                    textView.setText("" + hourOfDay + ":0" + minute + ":00");
                else
                    textView.setText("" + hourOfDay + ":" + minute + ":00");


            }
        };
        return time;

    }


    private void callVehicleApi() {
        if (NetworkUtil.isOnline(context)) {

            showProgress("Loading Vehicles");
            HashMap<String, String> params = new HashMap<>();
            params.put("key", SharedPreferenceUtil.getString(Constants.SP_USER_KEY, ""));
            params.put("cmd", "ALL,*");
            new HttpRequestSingleton(context, getString(R.string.api_base_url)+getString(R.string.api_vehicle_api), params, 100, new HttpCallback() {
                @Override
                public void onResponse(String response, int action) {
                    stopProgress();
                    if (response != null) {
                        Log.i("response", "" + response.toString());
                        switch (action) {
                            case 100:
                                ObjectMapper mapper = new ObjectMapper();
                                try {
                                    VehicleResponse vehicleResponse = mapper.readValue(response, VehicleResponse.class);
                                    sortResponse(vehicleResponse.getData());


                                } catch (JsonProcessingException e) {
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }

                                break;
                            default:
                        }

                    } else {
                        Toast.makeText(context, "Something Went Wrong", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onError(VolleyError error, int action) {

                }
            });

        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_LONG).show();
        }
    }

    private void sortResponse(List<Datum> allList) {
        List<Datum> list = new ArrayList<>();


        for (Datum vehicle : allList
                ) {
            if (vehicle.getActive().equalsIgnoreCase("true")) {
                list.add(vehicle);
            } else {

            }

        }
        listAllVehicles.addAll(list);
        setCustomeSpinner(list);


    }


    private void setCustomeSpinner(List<Datum> list) {
        CustomSpinnerAdapter adapter = new CustomSpinnerAdapter(this,
                R.layout.spinner_item, R.id.titleText, list);
        spinner.setAdapter(adapter);
    }


    private DatePickerDialog.OnDateSetListener setDateListener(final TextView textView) {
        date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                updateLabel(textView);
                if (textView == txtStartDate) {
                    selectedStartDate = myCalendar.getTimeInMillis();
                    isStartDateSelected = true;
                    updateLabel(txtEndDate);
                }
            }

        };
        return date;

    }

    private void setListeners() {

        txtStartTime.setOnClickListener(this);
        txtEndTime.setOnClickListener(this);
        txtEndDate.setOnClickListener(this);
        txtStartDate.setOnClickListener(this);
        btnWatchReplay.setOnClickListener(this);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

               selectedVehicleIMEINO= listAllVehicles.get(position).getImei();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void assignView() {
        txtEndTime = (TextView) findViewById(R.id.txtEndTime);
        txtEndDate = (TextView) findViewById(R.id.txtEndDate);
        txtStartTime = (TextView) findViewById(R.id.txtStartTime);
        txtStartDate = (TextView) findViewById(R.id.txtStartDate);
        spinner = (Spinner) findViewById(R.id.spinner);
        btnWatchReplay = (AppCompatButton) findViewById(R.id.btnWatchReplay);
        recyclerViewList = (RecyclerView) findViewById(R.id.recyclerViewList);

        txtStartTime.setText("00:00:00");
        txtEndTime.setText("23:59:59");
        Utill.setToday(txtStartDate);
        Utill.setToday(txtEndDate);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtStartDate:
                DatePickerDialog start = new DatePickerDialog(StopActivity.this, setDateListener(txtStartDate), myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH));
                start.getDatePicker().setMaxDate(System.currentTimeMillis());
                start.show();
                break;

            case R.id.txtEndDate:
                DatePickerDialog end = new DatePickerDialog(StopActivity.this, setDateListener(txtEndDate), myCalendarEnd
                        .get(Calendar.YEAR), myCalendarEnd.get(Calendar.MONTH),
                        myCalendarEnd.get(Calendar.DAY_OF_MONTH));
                end.getDatePicker().setMinDate(selectedStartDate);
                end.getDatePicker().setMaxDate(System.currentTimeMillis());

                end.show();
                break;

            case R.id.txtEndTime:
                new TimePickerDialog(StopActivity.this, setTimeListener(txtEndTime), 23, 59, true).show();
                break;
            case R.id.txtStartTime:
                new TimePickerDialog(StopActivity.this, setTimeListener(txtStartTime), 00, 00, true).show();
                break;


            case R.id.btnWatchReplay:
                if(isStartDateSelected)
                {
                 callStopApi();
                }
                else
                {
                    Toast.makeText(context,"Please select date",Toast.LENGTH_LONG).show();
                }

                break;
            default:
                break;
        }

    }



    private void updateLabel(TextView textView) {
        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        textView.setText(sdf.format(myCalendar.getTime()));
    }


    private void callStopApi() {
        if (NetworkUtil.isOnline(context)) {
            showProgress("Loading...");
            HashMap<String, String> params = new HashMap<>();
//            params.put("imei", "000007065017907");
//            params.put("userkey", "A29DB84D21FA82EAF564251D777007E1");
//            params.put("startdate", "2018-10-01");
//            params.put("enddate", "2018-10-31");

            params.put("imei", selectedVehicleIMEINO);
            params.put("userkey", SharedPreferenceUtil.getString(Constants.SP_USER_KEY,""));
            params.put("startdate", txtStartDate.getText().toString());
            params.put("enddate", txtEndDate.getText().toString());
            params.put("starttime", txtStartTime.getText().toString());
            params.put("endtime", txtEndTime.getText().toString());

            new HttpRequestSingleton(context, getString(R.string.api_base_url)+getString(R.string.api_stop_api), params, 101, new HttpCallback() {
                @Override
                public void onResponse(String response, int action) {
                    stopProgress();
                    if (response != null) {
                        Log.i("response", "" + response.toString());
                        switch (action) {
                            case 101:
                                ObjectMapper mapper = new ObjectMapper();
                                try {

                                    List<com.Ktracker.pro.response.stop.Datum> listResponse = new ArrayList<>();
                                    com.Ktracker.pro.response.stop.Datum header = new com.Ktracker.pro.response.stop.Datum();
                                    listResponse.add(header);

                                        StopResponse movementResponse = mapper.readValue(response, StopResponse.class);
                                        listResponse.addAll(movementResponse.getData());

                                    setAdapter(listResponse);





                                } catch (JsonProcessingException e) {
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }

                                break;
                            default:
                        }

                    } else {
                        Toast.makeText(context, "Something Went Wrong", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onError(VolleyError error, int action) {

                }
            });

        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_LONG).show();
        }
    }


    private void setAdapter(List<com.Ktracker.pro.response.stop.Datum> list) {
        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerViewList.setLayoutManager(linearLayoutManager);
        recyclerViewList.setItemAnimator(new DefaultItemAnimator());
        adapter = new StopAdapter(context, list);
        recyclerViewList.setAdapter(adapter);
    }




}
