
package com.Ktracker.pro.response.overspeed;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "dt_tracker",
    "lat",
    "lng",
    "altitude",
    "angle",
    "speed",
    "params"
})
public class Datum implements Serializable
{

    @JsonProperty("dt_tracker")
    private String dtTracker;
    @JsonProperty("lat")
    private String lat;
    @JsonProperty("lng")
    private String lng;
    @JsonProperty("altitude")
    private String altitude;
    @JsonProperty("angle")
    private String angle;
    @JsonProperty("speed")
    private String speed;
    @JsonProperty("params")
    private Params params;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    public final static Creator<Datum> CREATOR = new Creator<Datum>() {


        @SuppressWarnings({
            "unchecked"
        })
        public Datum createFromParcel(Parcel in) {
            return new Datum(in);
        }

        public Datum[] newArray(int size) {
            return (new Datum[size]);
        }

    }
    ;
    private final static long serialVersionUID = 4434565771099246246L;

    protected Datum(Parcel in) {
        this.dtTracker = ((String) in.readValue((String.class.getClassLoader())));
        this.lat = ((String) in.readValue((String.class.getClassLoader())));
        this.lng = ((String) in.readValue((String.class.getClassLoader())));
        this.altitude = ((String) in.readValue((String.class.getClassLoader())));
        this.angle = ((String) in.readValue((String.class.getClassLoader())));
        this.speed = ((String) in.readValue((String.class.getClassLoader())));
        this.params = ((Params) in.readValue((Params.class.getClassLoader())));
        this.additionalProperties = ((Map<String, Object> ) in.readValue((Map.class.getClassLoader())));
    }

    public Datum() {
    }

    @JsonProperty("dt_tracker")
    public String getDtTracker() {
        return dtTracker;
    }

    @JsonProperty("dt_tracker")
    public void setDtTracker(String dtTracker) {
        this.dtTracker = dtTracker;
    }

    @JsonProperty("lat")
    public String getLat() {
        return lat;
    }

    @JsonProperty("lat")
    public void setLat(String lat) {
        this.lat = lat;
    }

    @JsonProperty("lng")
    public String getLng() {
        return lng;
    }

    @JsonProperty("lng")
    public void setLng(String lng) {
        this.lng = lng;
    }

    @JsonProperty("altitude")
    public String getAltitude() {
        return altitude;
    }

    @JsonProperty("altitude")
    public void setAltitude(String altitude) {
        this.altitude = altitude;
    }

    @JsonProperty("angle")
    public String getAngle() {
        return angle;
    }

    @JsonProperty("angle")
    public void setAngle(String angle) {
        this.angle = angle;
    }

    @JsonProperty("speed")
    public String getSpeed() {
        return speed;
    }

    @JsonProperty("speed")
    public void setSpeed(String speed) {
        this.speed = speed;
    }

    @JsonProperty("params")
    public Params getParams() {
        return params;
    }

    @JsonProperty("params")
    public void setParams(Params params) {
        this.params = params;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(dtTracker);
        dest.writeValue(lat);
        dest.writeValue(lng);
        dest.writeValue(altitude);
        dest.writeValue(angle);
        dest.writeValue(speed);
        dest.writeValue(params);
        dest.writeValue(additionalProperties);
    }

    public int describeContents() {
        return  0;
    }

}
