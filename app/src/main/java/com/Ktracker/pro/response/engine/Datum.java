
package com.Ktracker.pro.response.engine;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "imei",
    "name",
    "dt_tracker",
    "port",
    "active",
    "supported",
    "on",
    "off"
})
public class Datum implements Serializable
{

    @JsonProperty("imei")
    private String imei;
    @JsonProperty("name")
    private String name;
    @JsonProperty("dt_tracker")
    private String dtTracker;
    @JsonProperty("port")
    private String port;
    @JsonProperty("active")
    private String active;
    @JsonProperty("supported")
    private String supported;
    @JsonProperty("on")
    private String on;
    @JsonProperty("off")
    private String off;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private final static long serialVersionUID = 5292783660379988708L;

    @JsonProperty("imei")
    public String getImei() {
        return imei;
    }

    @JsonProperty("imei")
    public void setImei(String imei) {
        this.imei = imei;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("dt_tracker")
    public String getDtTracker() {
        return dtTracker;
    }

    @JsonProperty("dt_tracker")
    public void setDtTracker(String dtTracker) {
        this.dtTracker = dtTracker;
    }

    @JsonProperty("port")
    public String getPort() {
        return port;
    }

    @JsonProperty("port")
    public void setPort(String port) {
        this.port = port;
    }

    @JsonProperty("active")
    public String getActive() {
        return active;
    }

    @JsonProperty("active")
    public void setActive(String active) {
        this.active = active;
    }

    @JsonProperty("supported")
    public String getSupported() {
        return supported;
    }

    @JsonProperty("supported")
    public void setSupported(String supported) {
        this.supported = supported;
    }

    @JsonProperty("on")
    public String getOn() {
        return on;
    }

    @JsonProperty("on")
    public void setOn(String on) {
        this.on = on;
    }

    @JsonProperty("off")
    public String getOff() {
        return off;
    }

    @JsonProperty("off")
    public void setOff(String off) {
        this.off = off;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
