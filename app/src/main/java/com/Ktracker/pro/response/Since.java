
package com.Ktracker.pro.response;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "totalDays",
    "years",
    "months",
    "days",
    "hours",
    "minutes",
    "seconds"
})
public class Since implements Serializable
{

    @JsonProperty("totalDays")
    private Integer totalDays;
    @JsonProperty("years")
    private Integer years;
    @JsonProperty("months")
    private Integer months;
    @JsonProperty("days")
    private Integer days;
    @JsonProperty("hours")
    private Integer hours;
    @JsonProperty("minutes")
    private Integer minutes;
    @JsonProperty("seconds")
    private Integer seconds;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private final static long serialVersionUID = 7433247808525735616L;

    @JsonProperty("totalDays")
    public Integer getTotalDays() {
        return totalDays;
    }

    @JsonProperty("totalDays")
    public void setTotalDays(Integer totalDays) {
        this.totalDays = totalDays;
    }

    @JsonProperty("years")
    public Integer getYears() {
        return years;
    }

    @JsonProperty("years")
    public void setYears(Integer years) {
        this.years = years;
    }

    @JsonProperty("months")
    public Integer getMonths() {
        return months;
    }

    @JsonProperty("months")
    public void setMonths(Integer months) {
        this.months = months;
    }

    @JsonProperty("days")
    public Integer getDays() {
        return days;
    }

    @JsonProperty("days")
    public void setDays(Integer days) {
        this.days = days;
    }

    @JsonProperty("hours")
    public Integer getHours() {
        return hours;
    }

    @JsonProperty("hours")
    public void setHours(Integer hours) {
        this.hours = hours;
    }

    @JsonProperty("minutes")
    public Integer getMinutes() {
        return minutes;
    }

    @JsonProperty("minutes")
    public void setMinutes(Integer minutes) {
        this.minutes = minutes;
    }

    @JsonProperty("seconds")
    public Integer getSeconds() {
        return seconds;
    }

    @JsonProperty("seconds")
    public void setSeconds(Integer seconds) {
        this.seconds = seconds;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
