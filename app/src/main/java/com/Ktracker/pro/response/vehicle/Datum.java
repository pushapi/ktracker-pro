
        package com.Ktracker.pro.response.vehicle;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

        @JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "imei",
        "name",
        "dt_server",
        "dt_tracker",
        "lat",
        "lng",
        "port",
        "active",
        "object_expire_dt",
        "days",
        "notice",
        "number",
        "message",
        "altitude",
        "angle",
        "speed",
        "ignition",
        "supply",
        "status",
        "ac",
        "icon",
        "hours",
        "address",
        "params",
        "loc_valid"
})
public class Datum implements Serializable, Parcelable, ClusterItem
{

    @JsonProperty("imei")
    private String imei;
    @JsonProperty("name")
    private String name;
    @JsonProperty("dt_server")
    private String dtServer;
    @JsonProperty("dt_tracker")
    private String dtTracker;
    @JsonProperty("lat")
    private String lat;
    @JsonProperty("lng")
    private String lng;
    @JsonProperty("port")
    private String port;
    @JsonProperty("active")
    private String active;
    @JsonProperty("object_expire_dt")
    private String objectExpireDt;
    @JsonProperty("days")
    private String days;
    @JsonProperty("notice")
    private String notice;
    @JsonProperty("number")
    private String number;
    @JsonProperty("message")
    private String message;
    @JsonProperty("altitude")
    private String altitude;
    @JsonProperty("angle")
    private String angle;
    @JsonProperty("speed")
    private String speed;
    @JsonProperty("ignition")
    private String ignition;
    @JsonProperty("supply")
    private String supply;
    @JsonProperty("status")
    private String status;
    @JsonProperty("ac")
    private String ac;
    @JsonProperty("icon")
    private String icon;
    @JsonProperty("hours")
    private String hours;
    @JsonProperty("address")
    private String address;
    @JsonProperty("params")
    private Params params;
    @JsonProperty("loc_valid")
    private String locValid;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    public final static Parcelable.Creator<Datum> CREATOR = new Creator<Datum>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Datum createFromParcel(Parcel in) {
            return new Datum(in);
        }

        public Datum[] newArray(int size) {
            return (new Datum[size]);
        }

    }
            ;
    private final static long serialVersionUID = 6554595449555127782L;

    protected Datum(Parcel in) {
        this.imei = ((String) in.readValue((String.class.getClassLoader())));
        this.name = ((String) in.readValue((String.class.getClassLoader())));
        this.dtServer = ((String) in.readValue((String.class.getClassLoader())));
        this.dtTracker = ((String) in.readValue((String.class.getClassLoader())));
        this.lat = ((String) in.readValue((String.class.getClassLoader())));
        this.lng = ((String) in.readValue((String.class.getClassLoader())));
        this.port = ((String) in.readValue((String.class.getClassLoader())));
        this.active = ((String) in.readValue((String.class.getClassLoader())));
        this.objectExpireDt = ((String) in.readValue((String.class.getClassLoader())));
        this.days = ((String) in.readValue((String.class.getClassLoader())));
        this.notice = ((String) in.readValue((String.class.getClassLoader())));
        this.number = ((String) in.readValue((String.class.getClassLoader())));
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        this.altitude = ((String) in.readValue((String.class.getClassLoader())));
        this.angle = ((String) in.readValue((String.class.getClassLoader())));
        this.speed = ((String) in.readValue((String.class.getClassLoader())));
        this.ignition = ((String) in.readValue((String.class.getClassLoader())));
        this.supply = ((String) in.readValue((String.class.getClassLoader())));
        this.status = ((String) in.readValue((String.class.getClassLoader())));
        this.ac = ((String) in.readValue((String.class.getClassLoader())));
        this.icon = ((String) in.readValue((String.class.getClassLoader())));
        this.hours = ((String) in.readValue((String.class.getClassLoader())));
        this.address = ((String) in.readValue((String.class.getClassLoader())));
        this.params = ((Params) in.readValue((Params.class.getClassLoader())));
        this.locValid = ((String) in.readValue((String.class.getClassLoader())));
        this.additionalProperties = ((Map<String, Object> ) in.readValue((Map.class.getClassLoader())));
    }

    public Datum() {
    }

    @JsonProperty("imei")
    public String getImei() {
        return imei;
    }

    @JsonProperty("imei")
    public void setImei(String imei) {
        this.imei = imei;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("dt_server")
    public String getDtServer() {
        return dtServer;
    }

    @JsonProperty("dt_server")
    public void setDtServer(String dtServer) {
        this.dtServer = dtServer;
    }

    @JsonProperty("dt_tracker")
    public String getDtTracker() {
        return dtTracker;
    }

    @JsonProperty("dt_tracker")
    public void setDtTracker(String dtTracker) {
        this.dtTracker = dtTracker;
    }

    @JsonProperty("lat")
    public String getLat() {
        return lat;
    }

    @JsonProperty("lat")
    public void setLat(String lat) {
        this.lat = lat;
    }

    @JsonProperty("lng")
    public String getLng() {
        return lng;
    }

    @JsonProperty("lng")
    public void setLng(String lng) {
        this.lng = lng;
    }

    @JsonProperty("port")
    public String getPort() {
        return port;
    }

    @JsonProperty("port")
    public void setPort(String port) {
        this.port = port;
    }

    @JsonProperty("active")
    public String getActive() {
        return active;
    }

    @JsonProperty("active")
    public void setActive(String active) {
        this.active = active;
    }

    @JsonProperty("object_expire_dt")
    public String getObjectExpireDt() {
        return objectExpireDt;
    }

    @JsonProperty("object_expire_dt")
    public void setObjectExpireDt(String objectExpireDt) {
        this.objectExpireDt = objectExpireDt;
    }

    @JsonProperty("days")
    public String getDays() {
        return days;
    }

    @JsonProperty("days")
    public void setDays(String days) {
        this.days = days;
    }

    @JsonProperty("notice")
    public String getNotice() {
        return notice;
    }

    @JsonProperty("notice")
    public void setNotice(String notice) {
        this.notice = notice;
    }

    @JsonProperty("number")
    public String getNumber() {
        return number;
    }

    @JsonProperty("number")
    public void setNumber(String number) {
        this.number = number;
    }

    @JsonProperty("message")
    public String getMessage() {
        return message;
    }

    @JsonProperty("message")
    public void setMessage(String message) {
        this.message = message;
    }

    @JsonProperty("altitude")
    public String getAltitude() {
        return altitude;
    }

    @JsonProperty("altitude")
    public void setAltitude(String altitude) {
        this.altitude = altitude;
    }

    @JsonProperty("angle")
    public String getAngle() {
        return angle;
    }

    @JsonProperty("angle")
    public void setAngle(String angle) {
        this.angle = angle;
    }

    @JsonProperty("speed")
    public String getSpeed() {
        return speed;
    }

    @JsonProperty("speed")
    public void setSpeed(String speed) {
        this.speed = speed;
    }

    @JsonProperty("ignition")
    public String getIgnition() {
        return ignition;
    }

    @JsonProperty("ignition")
    public void setIgnition(String ignition) {
        this.ignition = ignition;
    }

    @JsonProperty("supply")
    public String getSupply() {
        return supply;
    }

    @JsonProperty("supply")
    public void setSupply(String supply) {
        this.supply = supply;
    }

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    @JsonProperty("ac")
    public String getAc() {
        return ac;
    }

    @JsonProperty("ac")
    public void setAc(String ac) {
        this.ac = ac;
    }

    @JsonProperty("icon")
    public String getIcon() {
        return icon;
    }

    @JsonProperty("icon")
    public void setIcon(String icon) {
        this.icon = icon;
    }

    @JsonProperty("hours")
    public String getHours() {
        return hours;
    }

    @JsonProperty("hours")
    public void setHours(String hours) {
        this.hours = hours;
    }

    @JsonProperty("address")
    public String getAddress() {
        return address;
    }

    @JsonProperty("address")
    public void setAddress(String address) {
        this.address = address;
    }

    @JsonProperty("params")
    public Params getParams() {
        return params;
    }

    @JsonProperty("params")
    public void setParams(Params params) {
        this.params = params;
    }

    @JsonProperty("loc_valid")
    public String getLocValid() {
        return locValid;
    }

    @JsonProperty("loc_valid")
    public void setLocValid(String locValid) {
        this.locValid = locValid;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(imei);
        dest.writeValue(name);
        dest.writeValue(dtServer);
        dest.writeValue(dtTracker);
        dest.writeValue(lat);
        dest.writeValue(lng);
        dest.writeValue(port);
        dest.writeValue(active);
        dest.writeValue(objectExpireDt);
        dest.writeValue(days);
        dest.writeValue(notice);
        dest.writeValue(number);
        dest.writeValue(message);
        dest.writeValue(altitude);
        dest.writeValue(angle);
        dest.writeValue(speed);
        dest.writeValue(ignition);
        dest.writeValue(supply);
        dest.writeValue(status);
        dest.writeValue(ac);
        dest.writeValue(icon);
        dest.writeValue(hours);
        dest.writeValue(address);
        dest.writeValue(params);
        dest.writeValue(locValid);
        dest.writeValue(additionalProperties);
    }

    public int describeContents() {
        return 0;
    }

    @Override
    public LatLng getPosition() {
        return new LatLng(Double.parseDouble(getLat()), Double.parseDouble(getLng()));
    }

    @Override
    public String getTitle() {
        return getName();
    }

    @Override
    public String getSnippet() {
        return getActive();
    }

}


