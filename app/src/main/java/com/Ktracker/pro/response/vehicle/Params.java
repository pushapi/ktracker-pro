
package com.Ktracker.pro.response.vehicle;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "di1",
        "acc",
        "di2",
        "di3",
        "di4",
        "di5",
        "di6",
        "di7",
        "alarm",
        "pump",
        "track",
        "bats",
        "batl",
        "defense",
        "gpslev"
})
public class Params implements Serializable, Parcelable
{

    @JsonProperty("di1")
    private String di1;
    @JsonProperty("acc")
    private String acc;
    @JsonProperty("di2")
    private String di2;
    @JsonProperty("di3")
    private String di3;
    @JsonProperty("di4")
    private String di4;
    @JsonProperty("di5")
    private String di5;
    @JsonProperty("di6")
    private String di6;
    @JsonProperty("di7")
    private String di7;
    @JsonProperty("alarm")
    private String alarm;
    @JsonProperty("pump")
    private String pump;
    @JsonProperty("track")
    private String track;
    @JsonProperty("bats")
    private String bats;
    @JsonProperty("batl")
    private String batl;
    @JsonProperty("defense")
    private String defense;
    @JsonProperty("gpslev")
    private String gpslev;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    public final static Creator<Params> CREATOR = new Creator<Params>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Params createFromParcel(Parcel in) {
            return new Params(in);
        }

        public Params[] newArray(int size) {
            return (new Params[size]);
        }

    }
            ;
    private final static long serialVersionUID = 4014972297957819164L;

    protected Params(Parcel in) {
        this.di1 = ((String) in.readValue((String.class.getClassLoader())));
        this.acc = ((String) in.readValue((String.class.getClassLoader())));
        this.di2 = ((String) in.readValue((String.class.getClassLoader())));
        this.di3 = ((String) in.readValue((String.class.getClassLoader())));
        this.di4 = ((String) in.readValue((String.class.getClassLoader())));
        this.di5 = ((String) in.readValue((String.class.getClassLoader())));
        this.di6 = ((String) in.readValue((String.class.getClassLoader())));
        this.di7 = ((String) in.readValue((String.class.getClassLoader())));
        this.alarm = ((String) in.readValue((String.class.getClassLoader())));
        this.pump = ((String) in.readValue((String.class.getClassLoader())));
        this.track = ((String) in.readValue((String.class.getClassLoader())));
        this.bats = ((String) in.readValue((String.class.getClassLoader())));
        this.batl = ((String) in.readValue((String.class.getClassLoader())));
        this.defense = ((String) in.readValue((String.class.getClassLoader())));
        this.gpslev = ((String) in.readValue((String.class.getClassLoader())));
        this.additionalProperties = ((Map<String, Object> ) in.readValue((Map.class.getClassLoader())));
    }

    public Params() {
    }

    @JsonProperty("di1")
    public String getDi1() {
        return di1;
    }

    @JsonProperty("di1")
    public void setDi1(String di1) {
        this.di1 = di1;
    }

    @JsonProperty("acc")
    public String getAcc() {
        return acc;
    }

    @JsonProperty("acc")
    public void setAcc(String acc) {
        this.acc = acc;
    }

    @JsonProperty("di2")
    public String getDi2() {
        return di2;
    }

    @JsonProperty("di2")
    public void setDi2(String di2) {
        this.di2 = di2;
    }

    @JsonProperty("di3")
    public String getDi3() {
        return di3;
    }

    @JsonProperty("di3")
    public void setDi3(String di3) {
        this.di3 = di3;
    }

    @JsonProperty("di4")
    public String getDi4() {
        return di4;
    }

    @JsonProperty("di4")
    public void setDi4(String di4) {
        this.di4 = di4;
    }

    @JsonProperty("di5")
    public String getDi5() {
        return di5;
    }

    @JsonProperty("di5")
    public void setDi5(String di5) {
        this.di5 = di5;
    }

    @JsonProperty("di6")
    public String getDi6() {
        return di6;
    }

    @JsonProperty("di6")
    public void setDi6(String di6) {
        this.di6 = di6;
    }

    @JsonProperty("di7")
    public String getDi7() {
        return di7;
    }

    @JsonProperty("di7")
    public void setDi7(String di7) {
        this.di7 = di7;
    }

    @JsonProperty("alarm")
    public String getAlarm() {
        return alarm;
    }

    @JsonProperty("alarm")
    public void setAlarm(String alarm) {
        this.alarm = alarm;
    }

    @JsonProperty("pump")
    public String getPump() {
        return pump;
    }

    @JsonProperty("pump")
    public void setPump(String pump) {
        this.pump = pump;
    }

    @JsonProperty("track")
    public String getTrack() {
        return track;
    }

    @JsonProperty("track")
    public void setTrack(String track) {
        this.track = track;
    }

    @JsonProperty("bats")
    public String getBats() {
        return bats;
    }

    @JsonProperty("bats")
    public void setBats(String bats) {
        this.bats = bats;
    }

    @JsonProperty("batl")
    public String getBatl() {
        return batl;
    }

    @JsonProperty("batl")
    public void setBatl(String batl) {
        this.batl = batl;
    }

    @JsonProperty("defense")
    public String getDefense() {
        return defense;
    }

    @JsonProperty("defense")
    public void setDefense(String defense) {
        this.defense = defense;
    }

    @JsonProperty("gpslev")
    public String getGpslev() {
        return gpslev;
    }

    @JsonProperty("gpslev")
    public void setGpslev(String gpslev) {
        this.gpslev = gpslev;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(di1);
        dest.writeValue(acc);
        dest.writeValue(di2);
        dest.writeValue(di3);
        dest.writeValue(di4);
        dest.writeValue(di5);
        dest.writeValue(di6);
        dest.writeValue(di7);
        dest.writeValue(alarm);
        dest.writeValue(pump);
        dest.writeValue(track);
        dest.writeValue(bats);
        dest.writeValue(batl);
        dest.writeValue(defense);
        dest.writeValue(gpslev);
        dest.writeValue(additionalProperties);
    }

    public int describeContents() {
        return  0;
    }

}
