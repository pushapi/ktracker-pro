
package com.Ktracker.pro.response.ignition;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "startlat",
    "startlng",
    "endlat",
    "endlng",
    "startdate",
    "enddate",
    "duration",
    "count",
    "total_duration"
})
public class Datum implements Serializable
{

    @JsonProperty("id")
    private int id;
    @JsonProperty("startlat")
    private String startlat;
    @JsonProperty("startlng")
    private String startlng;
    @JsonProperty("endlat")
    private String endlat;
    @JsonProperty("endlng")
    private String endlng;
    @JsonProperty("startdate")
    private String startdate;
    @JsonProperty("enddate")
    private String enddate;
    @JsonProperty("duration")
    private String duration;
    @JsonProperty("count")
    private int count;
    @JsonProperty("total_duration")
    private String totalDuration;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private final static long serialVersionUID = 7728529979133630269L;

    @JsonProperty("id")
    public int getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(int id) {
        this.id = id;
    }

    @JsonProperty("startlat")
    public String getStartlat() {
        return startlat;
    }

    @JsonProperty("startlat")
    public void setStartlat(String startlat) {
        this.startlat = startlat;
    }

    @JsonProperty("startlng")
    public String getStartlng() {
        return startlng;
    }

    @JsonProperty("startlng")
    public void setStartlng(String startlng) {
        this.startlng = startlng;
    }

    @JsonProperty("endlat")
    public String getEndlat() {
        return endlat;
    }

    @JsonProperty("endlat")
    public void setEndlat(String endlat) {
        this.endlat = endlat;
    }

    @JsonProperty("endlng")
    public String getEndlng() {
        return endlng;
    }

    @JsonProperty("endlng")
    public void setEndlng(String endlng) {
        this.endlng = endlng;
    }

    @JsonProperty("startdate")
    public String getStartdate() {
        return startdate;
    }

    @JsonProperty("startdate")
    public void setStartdate(String startdate) {
        this.startdate = startdate;
    }

    @JsonProperty("enddate")
    public String getEnddate() {
        return enddate;
    }

    @JsonProperty("enddate")
    public void setEnddate(String enddate) {
        this.enddate = enddate;
    }

    @JsonProperty("duration")
    public String getDuration() {
        return duration;
    }

    @JsonProperty("duration")
    public void setDuration(String duration) {
        this.duration = duration;
    }

    @JsonProperty("count")
    public int getCount() {
        return count;
    }

    @JsonProperty("count")
    public void setCount(int count) {
        this.count = count;
    }

    @JsonProperty("total_duration")
    public String getTotalDuration() {
        return totalDuration;
    }

    @JsonProperty("total_duration")
    public void setTotalDuration(String totalDuration) {
        this.totalDuration = totalDuration;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
