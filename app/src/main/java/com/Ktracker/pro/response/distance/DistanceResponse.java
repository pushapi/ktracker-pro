
package com.Ktracker.pro.response.distance;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "date",
    "total",
    "top_speed",
    "avg_speed",
    "fuel_consumption",
    "fuel_cost",
    "stops_duration_time",
    "stops_duration",
    "drives_duration_time",
    "drives_duration",
    "engine_work_time",
    "engine_idle_time",
    "engine_work",
    "engine_idle",
    "km",
    "fuel_consumption_per_100km",
    "fuel_consumption_mpg"
})
public class DistanceResponse implements Serializable
{

    @JsonProperty("date")
    private String date;
    @JsonProperty("total")
    private double total;
    @JsonProperty("top_speed")
    private int topSpeed;
    @JsonProperty("avg_speed")
    private int avgSpeed;
    @JsonProperty("fuel_consumption")
    private double fuelConsumption;
    @JsonProperty("fuel_cost")
    private double fuelCost;
    @JsonProperty("stops_duration_time")
    private int stopsDurationTime;
    @JsonProperty("stops_duration")
    private String stopsDuration;
    @JsonProperty("drives_duration_time")
    private int drivesDurationTime;
    @JsonProperty("drives_duration")
    private String drivesDuration;
    @JsonProperty("engine_work_time")
    private int engineWorkTime;
    @JsonProperty("engine_idle_time")
    private int engineIdleTime;
    @JsonProperty("engine_work")
    private String engineWork;
    @JsonProperty("engine_idle")
    private String engineIdle;
    @JsonProperty("km")
    private double km;
    @JsonProperty("fuel_consumption_per_100km")
    private double fuelConsumptionPer100km;
    @JsonProperty("fuel_consumption_mpg")
    private int fuelConsumptionMpg;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private final static long serialVersionUID = 7896973154435721407L;

    @JsonProperty("date")
    public String getDate() {
        return date;
    }

    @JsonProperty("date")
    public void setDate(String date) {
        this.date = date;
    }

    @JsonProperty("total")
    public double getTotal() {
        return total;
    }

    @JsonProperty("total")
    public void setTotal(double total) {
        this.total = total;
    }

    @JsonProperty("top_speed")
    public int getTopSpeed() {
        return topSpeed;
    }

    @JsonProperty("top_speed")
    public void setTopSpeed(int topSpeed) {
        this.topSpeed = topSpeed;
    }

    @JsonProperty("avg_speed")
    public int getAvgSpeed() {
        return avgSpeed;
    }

    @JsonProperty("avg_speed")
    public void setAvgSpeed(int avgSpeed) {
        this.avgSpeed = avgSpeed;
    }

    @JsonProperty("fuel_consumption")
    public double getFuelConsumption() {
        return fuelConsumption;
    }

    @JsonProperty("fuel_consumption")
    public void setFuelConsumption(double fuelConsumption) {
        this.fuelConsumption = fuelConsumption;
    }

    @JsonProperty("fuel_cost")
    public double getFuelCost() {
        return fuelCost;
    }

    @JsonProperty("fuel_cost")
    public void setFuelCost(double fuelCost) {
        this.fuelCost = fuelCost;
    }

    @JsonProperty("stops_duration_time")
    public int getStopsDurationTime() {
        return stopsDurationTime;
    }

    @JsonProperty("stops_duration_time")
    public void setStopsDurationTime(int stopsDurationTime) {
        this.stopsDurationTime = stopsDurationTime;
    }

    @JsonProperty("stops_duration")
    public String getStopsDuration() {
        return stopsDuration;
    }

    @JsonProperty("stops_duration")
    public void setStopsDuration(String stopsDuration) {
        this.stopsDuration = stopsDuration;
    }

    @JsonProperty("drives_duration_time")
    public int getDrivesDurationTime() {
        return drivesDurationTime;
    }

    @JsonProperty("drives_duration_time")
    public void setDrivesDurationTime(int drivesDurationTime) {
        this.drivesDurationTime = drivesDurationTime;
    }

    @JsonProperty("drives_duration")
    public String getDrivesDuration() {
        return drivesDuration;
    }

    @JsonProperty("drives_duration")
    public void setDrivesDuration(String drivesDuration) {
        this.drivesDuration = drivesDuration;
    }

    @JsonProperty("engine_work_time")
    public int getEngineWorkTime() {
        return engineWorkTime;
    }

    @JsonProperty("engine_work_time")
    public void setEngineWorkTime(int engineWorkTime) {
        this.engineWorkTime = engineWorkTime;
    }

    @JsonProperty("engine_idle_time")
    public int getEngineIdleTime() {
        return engineIdleTime;
    }

    @JsonProperty("engine_idle_time")
    public void setEngineIdleTime(int engineIdleTime) {
        this.engineIdleTime = engineIdleTime;
    }

    @JsonProperty("engine_work")
    public String getEngineWork() {
        return engineWork;
    }

    @JsonProperty("engine_work")
    public void setEngineWork(String engineWork) {
        this.engineWork = engineWork;
    }

    @JsonProperty("engine_idle")
    public String getEngineIdle() {
        return engineIdle;
    }

    @JsonProperty("engine_idle")
    public void setEngineIdle(String engineIdle) {
        this.engineIdle = engineIdle;
    }

    @JsonProperty("km")
    public double getKm() {
        return km;
    }

    @JsonProperty("km")
    public void setKm(double km) {
        this.km = km;
    }

    @JsonProperty("fuel_consumption_per_100km")
    public double getFuelConsumptionPer100km() {
        return fuelConsumptionPer100km;
    }

    @JsonProperty("fuel_consumption_per_100km")
    public void setFuelConsumptionPer100km(double fuelConsumptionPer100km) {
        this.fuelConsumptionPer100km = fuelConsumptionPer100km;
    }

    @JsonProperty("fuel_consumption_mpg")
    public int getFuelConsumptionMpg() {
        return fuelConsumptionMpg;
    }

    @JsonProperty("fuel_consumption_mpg")
    public void setFuelConsumptionMpg(int fuelConsumptionMpg) {
        this.fuelConsumptionMpg = fuelConsumptionMpg;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
