
package com.Ktracker.pro.response.service;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "imei",
    "name",
    "service_id",
    "remaining",
    "overdue",
    "service_name",
    "service_date"
})
public class Datum implements Serializable
{

    @JsonProperty("imei")
    private String imei;
    @JsonProperty("name")
    private String name;
    @JsonProperty("service_id")
    private String serviceId;
    @JsonProperty("remaining")
    private String remaining;
    @JsonProperty("overdue")
    private String overdue;
    @JsonProperty("service_name")
    private String serviceName;
    @JsonProperty("service_date")
    private String serviceDate;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private final static long serialVersionUID = -4207648301904854330L;

    @JsonProperty("imei")
    public String getImei() {
        return imei;
    }

    @JsonProperty("imei")
    public void setImei(String imei) {
        this.imei = imei;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("service_id")
    public String getServiceId() {
        return serviceId;
    }

    @JsonProperty("service_id")
    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    @JsonProperty("remaining")
    public String getRemaining() {
        return remaining;
    }

    @JsonProperty("remaining")
    public void setRemaining(String remaining) {
        this.remaining = remaining;
    }

    @JsonProperty("overdue")
    public String getOverdue() {
        return overdue;
    }

    @JsonProperty("overdue")
    public void setOverdue(String overdue) {
        this.overdue = overdue;
    }

    @JsonProperty("service_name")
    public String getServiceName() {
        return serviceName;
    }

    @JsonProperty("service_name")
    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    @JsonProperty("service_date")
    public String getServiceDate() {
        return serviceDate;
    }

    @JsonProperty("service_date")
    public void setServiceDate(String serviceDate) {
        this.serviceDate = serviceDate;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
