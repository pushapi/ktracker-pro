
package com.Ktracker.pro.response.expiry;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "imei",
    "ip",
    "port",
    "active",
    "object_expire",
    "object_expire_dt",
    "name"
})
public class ExpiryResponse implements Serializable, Parcelable
{

    @JsonProperty("imei")
    private String imei;
    @JsonProperty("ip")
    private String ip;
    @JsonProperty("port")
    private String port;
    @JsonProperty("active")
    private String active;
    @JsonProperty("object_expire")
    private String objectExpire;
    @JsonProperty("object_expire_dt")
    private String objectExpireDt;
    @JsonProperty("name")
    private String name;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    public final static Creator<ExpiryResponse> CREATOR = new Creator<ExpiryResponse>() {


        @SuppressWarnings({
            "unchecked"
        })
        public ExpiryResponse createFromParcel(Parcel in) {
            return new ExpiryResponse(in);
        }

        public ExpiryResponse[] newArray(int size) {
            return (new ExpiryResponse[size]);
        }

    }
    ;
    private final static long serialVersionUID = 1453997739665299240L;

    protected ExpiryResponse(Parcel in) {
        this.imei = ((String) in.readValue((String.class.getClassLoader())));
        this.ip = ((String) in.readValue((String.class.getClassLoader())));
        this.port = ((String) in.readValue((String.class.getClassLoader())));
        this.active = ((String) in.readValue((String.class.getClassLoader())));
        this.objectExpire = ((String) in.readValue((String.class.getClassLoader())));
        this.objectExpireDt = ((String) in.readValue((String.class.getClassLoader())));
        this.name = ((String) in.readValue((String.class.getClassLoader())));
        this.additionalProperties = ((Map<String, Object> ) in.readValue((Map.class.getClassLoader())));
    }

    public ExpiryResponse() {
    }

    @JsonProperty("imei")
    public String getImei() {
        return imei;
    }

    @JsonProperty("imei")
    public void setImei(String imei) {
        this.imei = imei;
    }

    @JsonProperty("ip")
    public String getIp() {
        return ip;
    }

    @JsonProperty("ip")
    public void setIp(String ip) {
        this.ip = ip;
    }

    @JsonProperty("port")
    public String getPort() {
        return port;
    }

    @JsonProperty("port")
    public void setPort(String port) {
        this.port = port;
    }

    @JsonProperty("active")
    public String getActive() {
        return active;
    }

    @JsonProperty("active")
    public void setActive(String active) {
        this.active = active;
    }

    @JsonProperty("object_expire")
    public String getObjectExpire() {
        return objectExpire;
    }

    @JsonProperty("object_expire")
    public void setObjectExpire(String objectExpire) {
        this.objectExpire = objectExpire;
    }

    @JsonProperty("object_expire_dt")
    public String getObjectExpireDt() {
        return objectExpireDt;
    }

    @JsonProperty("object_expire_dt")
    public void setObjectExpireDt(String objectExpireDt) {
        this.objectExpireDt = objectExpireDt;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(imei);
        dest.writeValue(ip);
        dest.writeValue(port);
        dest.writeValue(active);
        dest.writeValue(objectExpire);
        dest.writeValue(objectExpireDt);
        dest.writeValue(name);
        dest.writeValue(additionalProperties);
    }

    public int describeContents() {
        return  0;
    }

}
