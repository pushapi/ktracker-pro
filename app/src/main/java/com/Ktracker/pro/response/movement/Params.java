
package com.Ktracker.pro.response.movement;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "pump",
    "track",
    "bats",
    "acc",
    "batl",
    "gpslev",
    "alarm",
    "defense"
})
public class Params implements Serializable, Parcelable
{

    @JsonProperty("pump")
    private String pump;
    @JsonProperty("track")
    private String track;
    @JsonProperty("bats")
    private String bats;
    @JsonProperty("acc")
    private String acc;
    @JsonProperty("batl")
    private String batl;
    @JsonProperty("gpslev")
    private String gpslev;
    @JsonProperty("alarm")
    private String alarm;
    @JsonProperty("defense")
    private String defense;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    public final static Creator<Params> CREATOR = new Creator<Params>() {


        @SuppressWarnings({
            "unchecked"
        })
        public Params createFromParcel(Parcel in) {
            return new Params(in);
        }

        public Params[] newArray(int size) {
            return (new Params[size]);
        }

    }
    ;
    private final static long serialVersionUID = 8345271734785905730L;

    protected Params(Parcel in) {
        this.pump = ((String) in.readValue((String.class.getClassLoader())));
        this.track = ((String) in.readValue((String.class.getClassLoader())));
        this.bats = ((String) in.readValue((String.class.getClassLoader())));
        this.acc = ((String) in.readValue((String.class.getClassLoader())));
        this.batl = ((String) in.readValue((String.class.getClassLoader())));
        this.gpslev = ((String) in.readValue((String.class.getClassLoader())));
        this.alarm = ((String) in.readValue((String.class.getClassLoader())));
        this.defense = ((String) in.readValue((String.class.getClassLoader())));
        this.additionalProperties = ((Map<String, Object> ) in.readValue((Map.class.getClassLoader())));
    }

    public Params() {
    }

    @JsonProperty("pump")
    public String getPump() {
        return pump;
    }

    @JsonProperty("pump")
    public void setPump(String pump) {
        this.pump = pump;
    }

    @JsonProperty("track")
    public String getTrack() {
        return track;
    }

    @JsonProperty("track")
    public void setTrack(String track) {
        this.track = track;
    }

    @JsonProperty("bats")
    public String getBats() {
        return bats;
    }

    @JsonProperty("bats")
    public void setBats(String bats) {
        this.bats = bats;
    }

    @JsonProperty("acc")
    public String getAcc() {
        return acc;
    }

    @JsonProperty("acc")
    public void setAcc(String acc) {
        this.acc = acc;
    }

    @JsonProperty("batl")
    public String getBatl() {
        return batl;
    }

    @JsonProperty("batl")
    public void setBatl(String batl) {
        this.batl = batl;
    }

    @JsonProperty("gpslev")
    public String getGpslev() {
        return gpslev;
    }

    @JsonProperty("gpslev")
    public void setGpslev(String gpslev) {
        this.gpslev = gpslev;
    }

    @JsonProperty("alarm")
    public String getAlarm() {
        return alarm;
    }

    @JsonProperty("alarm")
    public void setAlarm(String alarm) {
        this.alarm = alarm;
    }

    @JsonProperty("defense")
    public String getDefense() {
        return defense;
    }

    @JsonProperty("defense")
    public void setDefense(String defense) {
        this.defense = defense;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(pump);
        dest.writeValue(track);
        dest.writeValue(bats);
        dest.writeValue(acc);
        dest.writeValue(batl);
        dest.writeValue(gpslev);
        dest.writeValue(alarm);
        dest.writeValue(defense);
        dest.writeValue(additionalProperties);
    }

    public int describeContents() {
        return  0;
    }

}
