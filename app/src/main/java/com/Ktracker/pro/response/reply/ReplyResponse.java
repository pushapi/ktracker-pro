
package com.Ktracker.pro.response.reply;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "data"
})
public class ReplyResponse implements Serializable, Parcelable
{

    @JsonProperty("data")
    private List<Datum> data = new ArrayList<Datum>();
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    public final static Creator<ReplyResponse> CREATOR = new Creator<ReplyResponse>() {


        @SuppressWarnings({
            "unchecked"
        })
        public ReplyResponse createFromParcel(Parcel in) {
            return new ReplyResponse(in);
        }

        public ReplyResponse[] newArray(int size) {
            return (new ReplyResponse[size]);
        }

    }
    ;
    private final static long serialVersionUID = 7116712558119994768L;

    protected ReplyResponse(Parcel in) {
        in.readList(this.data, (Datum.class.getClassLoader()));
        this.additionalProperties = ((Map<String, Object> ) in.readValue((Map.class.getClassLoader())));
    }

    public ReplyResponse() {
    }

    @JsonProperty("data")
    public List<Datum> getData() {
        return data;
    }

    @JsonProperty("data")
    public void setData(List<Datum> data) {
        this.data = data;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(data);
        dest.writeValue(additionalProperties);
    }

    public int describeContents() {
        return  0;
    }

}
