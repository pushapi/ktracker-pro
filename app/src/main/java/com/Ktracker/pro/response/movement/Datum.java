
package com.Ktracker.pro.response.movement;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "dt_start_s",
    "dt_start",
    "dt_end",
    "moving_duration",
    "route_length",
    "lat",
    "lng",
    "top_speed",
    "avg_speed",
    "count",
    "total_duration",
    "route_length_count"
})
public class Datum implements Serializable, Parcelable
{

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("dt_start_s")
    private String dtStartS;
    @JsonProperty("dt_start")
    private String dtStart;
    @JsonProperty("dt_end")
    private String dtEnd;
    @JsonProperty("moving_duration")
    private String movingDuration;
    @JsonProperty("route_length")
    private String routeLength;
    @JsonProperty("lat")
    private String lat;
    @JsonProperty("lng")
    private String lng;
    @JsonProperty("top_speed")
    private String topSpeed;
    @JsonProperty("avg_speed")
    private Integer avgSpeed;
    @JsonProperty("count")
    private Integer count;
    @JsonProperty("total_duration")
    private String totalDuration;
    @JsonProperty("route_length_count")
    private Float routeLengthCount;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    public final static Creator<Datum> CREATOR = new Creator<Datum>() {


        @SuppressWarnings({
            "unchecked"
        })
        public Datum createFromParcel(Parcel in) {
            return new Datum(in);
        }

        public Datum[] newArray(int size) {
            return (new Datum[size]);
        }

    }
    ;
    private final static long serialVersionUID = -1468430205286104570L;

    protected Datum(Parcel in) {
        this.id = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.dtStartS = ((String) in.readValue((String.class.getClassLoader())));
        this.dtStart = ((String) in.readValue((String.class.getClassLoader())));
        this.dtEnd = ((String) in.readValue((String.class.getClassLoader())));
        this.movingDuration = ((String) in.readValue((String.class.getClassLoader())));
        this.routeLength = ((String) in.readValue((String.class.getClassLoader())));
        this.lat = ((String) in.readValue((String.class.getClassLoader())));
        this.lng = ((String) in.readValue((String.class.getClassLoader())));
        this.topSpeed = ((String) in.readValue((String.class.getClassLoader())));
        this.avgSpeed = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.count = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.totalDuration = ((String) in.readValue((String.class.getClassLoader())));
        this.routeLengthCount = ((Float) in.readValue((Float.class.getClassLoader())));
        this.additionalProperties = ((Map<String, Object> ) in.readValue((Map.class.getClassLoader())));
    }

    public Datum() {
    }

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    @JsonProperty("dt_start_s")
    public String getDtStartS() {
        return dtStartS;
    }

    @JsonProperty("dt_start_s")
    public void setDtStartS(String dtStartS) {
        this.dtStartS = dtStartS;
    }

    @JsonProperty("dt_start")
    public String getDtStart() {
        return dtStart;
    }

    @JsonProperty("dt_start")
    public void setDtStart(String dtStart) {
        this.dtStart = dtStart;
    }

    @JsonProperty("dt_end")
    public String getDtEnd() {
        return dtEnd;
    }

    @JsonProperty("dt_end")
    public void setDtEnd(String dtEnd) {
        this.dtEnd = dtEnd;
    }

    @JsonProperty("moving_duration")
    public String getMovingDuration() {
        return movingDuration;
    }

    @JsonProperty("moving_duration")
    public void setMovingDuration(String movingDuration) {
        this.movingDuration = movingDuration;
    }

    @JsonProperty("route_length")
    public String getRouteLength() {
        return routeLength;
    }

    @JsonProperty("route_length")
    public void setRouteLength(String routeLength) {
        this.routeLength = routeLength;
    }

    @JsonProperty("lat")
    public String getLat() {
        return lat;
    }

    @JsonProperty("lat")
    public void setLat(String lat) {
        this.lat = lat;
    }

    @JsonProperty("lng")
    public String getLng() {
        return lng;
    }

    @JsonProperty("lng")
    public void setLng(String lng) {
        this.lng = lng;
    }

    @JsonProperty("top_speed")
    public String getTopSpeed() {
        return topSpeed;
    }

    @JsonProperty("top_speed")
    public void setTopSpeed(String topSpeed) {
        this.topSpeed = topSpeed;
    }

    @JsonProperty("avg_speed")
    public Integer getAvgSpeed() {
        return avgSpeed;
    }

    @JsonProperty("avg_speed")
    public void setAvgSpeed(Integer avgSpeed) {
        this.avgSpeed = avgSpeed;
    }

    @JsonProperty("count")
    public Integer getCount() {
        return count;
    }

    @JsonProperty("count")
    public void setCount(Integer count) {
        this.count = count;
    }

    @JsonProperty("total_duration")
    public String getTotalDuration() {
        return totalDuration;
    }

    @JsonProperty("total_duration")
    public void setTotalDuration(String totalDuration) {
        this.totalDuration = totalDuration;
    }

    @JsonProperty("route_length_count")
    public Float getRouteLengthCount() {
        return routeLengthCount;
    }

    @JsonProperty("route_length_count")
    public void setRouteLengthCount(Float routeLengthCount) {
        this.routeLengthCount = routeLengthCount;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(dtStartS);
        dest.writeValue(dtStart);
        dest.writeValue(dtEnd);
        dest.writeValue(movingDuration);
        dest.writeValue(routeLength);
        dest.writeValue(lat);
        dest.writeValue(lng);
        dest.writeValue(topSpeed);
        dest.writeValue(avgSpeed);
        dest.writeValue(count);
        dest.writeValue(totalDuration);
        dest.writeValue(routeLengthCount);
        dest.writeValue(additionalProperties);
    }

    public int describeContents() {
        return  0;
    }

}
