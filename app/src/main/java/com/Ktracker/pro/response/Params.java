
package com.Ktracker.pro.response;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "pump",
    "track",
    "bats",
    "acc",
    "batl",
    "gpslev",
    "defense"
})
public class Params implements Serializable
{

    @JsonProperty("pump")
    private String pump;
    @JsonProperty("track")
    private String track;
    @JsonProperty("bats")
    private String bats;
    @JsonProperty("acc")
    private String acc;
    @JsonProperty("batl")
    private String batl;
    @JsonProperty("gpslev")
    private String gpslev;
    @JsonProperty("defense")
    private String defense;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private final static long serialVersionUID = 992164466839706722L;

    @JsonProperty("pump")
    public String getPump() {
        return pump;
    }

    @JsonProperty("pump")
    public void setPump(String pump) {
        this.pump = pump;
    }

    @JsonProperty("track")
    public String getTrack() {
        return track;
    }

    @JsonProperty("track")
    public void setTrack(String track) {
        this.track = track;
    }

    @JsonProperty("bats")
    public String getBats() {
        return bats;
    }

    @JsonProperty("bats")
    public void setBats(String bats) {
        this.bats = bats;
    }

    @JsonProperty("acc")
    public String getAcc() {
        return acc;
    }

    @JsonProperty("acc")
    public void setAcc(String acc) {
        this.acc = acc;
    }

    @JsonProperty("batl")
    public String getBatl() {
        return batl;
    }

    @JsonProperty("batl")
    public void setBatl(String batl) {
        this.batl = batl;
    }

    @JsonProperty("gpslev")
    public String getGpslev() {
        return gpslev;
    }

    @JsonProperty("gpslev")
    public void setGpslev(String gpslev) {
        this.gpslev = gpslev;
    }

    @JsonProperty("defense")
    public String getDefense() {
        return defense;
    }

    @JsonProperty("defense")
    public void setDefense(String defense) {
        this.defense = defense;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
