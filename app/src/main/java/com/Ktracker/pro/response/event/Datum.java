
package com.Ktracker.pro.response.event;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "event_desc",
    "imei",
    "name",
    "dt_tracker",
    "lat",
    "long",
    "speed"
})
public class Datum implements Serializable, Parcelable
{

    @JsonProperty("event_desc")
    private String eventDesc;
    @JsonProperty("imei")
    private String imei;
    @JsonProperty("name")
    private String name;
    @JsonProperty("dt_tracker")
    private String dtTracker;
    @JsonProperty("lat")
    private String lat;
    @JsonProperty("long")
    private String _long;
    @JsonProperty("speed")
    private String speed;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    public final static Creator<Datum> CREATOR = new Creator<Datum>() {


        @SuppressWarnings({
            "unchecked"
        })
        public Datum createFromParcel(Parcel in) {
            return new Datum(in);
        }

        public Datum[] newArray(int size) {
            return (new Datum[size]);
        }

    }
    ;
    private final static long serialVersionUID = -3744119394833806979L;

    protected Datum(Parcel in) {
        this.eventDesc = ((String) in.readValue((String.class.getClassLoader())));
        this.imei = ((String) in.readValue((String.class.getClassLoader())));
        this.name = ((String) in.readValue((String.class.getClassLoader())));
        this.dtTracker = ((String) in.readValue((String.class.getClassLoader())));
        this.lat = ((String) in.readValue((String.class.getClassLoader())));
        this._long = ((String) in.readValue((String.class.getClassLoader())));
        this.speed = ((String) in.readValue((String.class.getClassLoader())));
        this.additionalProperties = ((Map<String, Object> ) in.readValue((Map.class.getClassLoader())));
    }

    public Datum() {
    }

    @JsonProperty("event_desc")
    public String getEventDesc() {
        return eventDesc;
    }

    @JsonProperty("event_desc")
    public void setEventDesc(String eventDesc) {
        this.eventDesc = eventDesc;
    }

    @JsonProperty("imei")
    public String getImei() {
        return imei;
    }

    @JsonProperty("imei")
    public void setImei(String imei) {
        this.imei = imei;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("dt_tracker")
    public String getDtTracker() {
        return dtTracker;
    }

    @JsonProperty("dt_tracker")
    public void setDtTracker(String dtTracker) {
        this.dtTracker = dtTracker;
    }

    @JsonProperty("lat")
    public String getLat() {
        return lat;
    }

    @JsonProperty("lat")
    public void setLat(String lat) {
        this.lat = lat;
    }

    @JsonProperty("long")
    public String getLong() {
        return _long;
    }

    @JsonProperty("long")
    public void setLong(String _long) {
        this._long = _long;
    }

    @JsonProperty("speed")
    public String getSpeed() {
        return speed;
    }

    @JsonProperty("speed")
    public void setSpeed(String speed) {
        this.speed = speed;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(eventDesc);
        dest.writeValue(imei);
        dest.writeValue(name);
        dest.writeValue(dtTracker);
        dest.writeValue(lat);
        dest.writeValue(_long);
        dest.writeValue(speed);
        dest.writeValue(additionalProperties);
    }

    public int describeContents() {
        return  0;
    }

}
