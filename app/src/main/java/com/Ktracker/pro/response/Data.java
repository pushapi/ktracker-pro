
package com.Ktracker.pro.response;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "username",
    "password",
    "imei",
    "userkey",
    "time_to",
    "time_from",
    "url"
})
public class Data implements Serializable
{

    @JsonProperty("username")
    private String username;
    @JsonProperty("password")
    private String password;
    @JsonProperty("imei")
    private String imei;
    @JsonProperty("userkey")
    private String userkey;
    @JsonProperty("time_to")
    private String timeTo;
    @JsonProperty("time_from")
    private String timeFrom;
    @JsonProperty("url")
    private String url;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private final static long serialVersionUID = -1926003604846140896L;

    @JsonProperty("username")
    public String getUsername() {
        return username;
    }

    @JsonProperty("username")
    public void setUsername(String username) {
        this.username = username;
    }

    @JsonProperty("password")
    public String getPassword() {
        return password;
    }

    @JsonProperty("password")
    public void setPassword(String password) {
        this.password = password;
    }

    @JsonProperty("imei")
    public String getImei() {
        return imei;
    }

    @JsonProperty("imei")
    public void setImei(String imei) {
        this.imei = imei;
    }

    @JsonProperty("userkey")
    public String getUserkey() {
        return userkey;
    }

    @JsonProperty("userkey")
    public void setUserkey(String userkey) {
        this.userkey = userkey;
    }

    @JsonProperty("time_to")
    public String getTimeTo() {
        return timeTo;
    }

    @JsonProperty("time_to")
    public void setTimeTo(String timeTo) {
        this.timeTo = timeTo;
    }

    @JsonProperty("time_from")
    public String getTimeFrom() {
        return timeFrom;
    }

    @JsonProperty("time_from")
    public void setTimeFrom(String timeFrom) {
        this.timeFrom = timeFrom;
    }

    @JsonProperty("url")
    public String getUrl() {
        return url;
    }

    @JsonProperty("url")
    public void setUrl(String url) {
        this.url = url;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
