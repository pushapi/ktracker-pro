
package com.Ktracker.pro.response;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "imei",
    "name",
    "dt_server",
    "dt_tracker",
    "lat",
    "lng",
    "port",
    "active",
    "altitude",
    "angle",
    "speed",
    "ignition",
    "supply",
    "status",
        "ac",
    "hours",
    "odometer",
    "odometer_type",
    "url",
    "address",
    "since",
    "params",
    "loc_valid"
})
public class Track implements Serializable
{

    @JsonProperty("imei")
    private String imei;
    @JsonProperty("name")
    private String name;
    @JsonProperty("dt_server")
    private String dtServer;
    @JsonProperty("dt_tracker")
    private String dtTracker;
    @JsonProperty("lat")
    private String lat;
    @JsonProperty("lng")
    private String lng;
    @JsonProperty("port")
    private String port;
    @JsonProperty("active")
    private String active;
    @JsonProperty("altitude")
    private String altitude;
    @JsonProperty("angle")
    private String angle;
    @JsonProperty("speed")
    private String speed;
    @JsonProperty("ignition")
    private String ignition;
    @JsonProperty("supply")
    private String supply;
    @JsonProperty("status")
    private String status;
    @JsonProperty("ac")
    private String ac;
    @JsonProperty("icon")
    private String icon;
    @JsonProperty("hours")
    private String hours;
    @JsonProperty("odometer")
    private String odometer;
    @JsonProperty("odometer_type")
    private String odometerType;
    @JsonProperty("url")
    private String url;
    @JsonProperty("address")
    private String address;
    @JsonProperty("since")
    private Since since;
    @JsonProperty("params")
    private Params params;
    @JsonProperty("loc_valid")
    private String locValid;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private final static long serialVersionUID = 3420575405921928394L;

    @JsonProperty("imei")
    public String getImei() {
        return imei;
    }

    @JsonProperty("imei")
    public void setImei(String imei) {
        this.imei = imei;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("dt_server")
    public String getDtServer() {
        return dtServer;
    }

    @JsonProperty("dt_server")
    public void setDtServer(String dtServer) {
        this.dtServer = dtServer;
    }

    @JsonProperty("dt_tracker")
    public String getDtTracker() {
        return dtTracker;
    }

    @JsonProperty("dt_tracker")
    public void setDtTracker(String dtTracker) {
        this.dtTracker = dtTracker;
    }

    @JsonProperty("lat")
    public String getLat() {
        return lat;
    }

    @JsonProperty("lat")
    public void setLat(String lat) {
        this.lat = lat;
    }

    @JsonProperty("lng")
    public String getLng() {
        return lng;
    }

    @JsonProperty("lng")
    public void setLng(String lng) {
        this.lng = lng;
    }

    @JsonProperty("port")
    public String getPort() {
        return port;
    }

    @JsonProperty("port")
    public void setPort(String port) {
        this.port = port;
    }

    @JsonProperty("active")
    public String getActive() {
        return active;
    }

    @JsonProperty("active")
    public void setActive(String active) {
        this.active = active;
    }

    @JsonProperty("altitude")
    public String getAltitude() {
        return altitude;
    }

    @JsonProperty("altitude")
    public void setAltitude(String altitude) {
        this.altitude = altitude;
    }

    @JsonProperty("angle")
    public String getAngle() {
        return angle;
    }

    @JsonProperty("angle")
    public void setAngle(String angle) {
        this.angle = angle;
    }

    @JsonProperty("speed")
    public String getSpeed() {
        return speed;
    }

    @JsonProperty("speed")
    public void setSpeed(String speed) {
        this.speed = speed;
    }

    @JsonProperty("ignition")
    public String getIgnition() {
        return ignition;
    }

    @JsonProperty("ignition")
    public void setIgnition(String ignition) {
        this.ignition = ignition;
    }

    @JsonProperty("supply")
    public String getSupply() {
        return supply;
    }

    @JsonProperty("supply")
    public void setSupply(String supply) {
        this.supply = supply;
    }

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    @JsonProperty("ac")
    public String getAc() {
        return ac;
    }

    @JsonProperty("ac")
    public void setAc(String ac) {
        this.ac = ac;
    }

    @JsonProperty("icon")
    public String getIcon() {
        return icon;
    }

    @JsonProperty("icon")
    public void setIcon(String icon) {
        this.icon = icon;
    }

    @JsonProperty("hours")
    public String getHours() {
        return hours;
    }

    @JsonProperty("hours")
    public void setHours(String hours) {
        this.hours = hours;
    }

    @JsonProperty("odometer")
    public String getOdometer() {
        return odometer;
    }

    @JsonProperty("odometer")
    public void setOdometer(String odometer) {
        this.odometer = odometer;
    }

    @JsonProperty("odometer_type")
    public String getOdometerType() {
        return odometerType;
    }

    @JsonProperty("odometer_type")
    public void setOdometerType(String odometerType) {
        this.odometerType = odometerType;
    }

    @JsonProperty("url")
    public String getUrl() {
        return url;
    }

    @JsonProperty("url")
    public void setUrl(String url) {
        this.url = url;
    }

    @JsonProperty("address")
    public String getAddress() {
        return address;
    }

    @JsonProperty("address")
    public void setAddress(String address) {
        this.address = address;
    }

    @JsonProperty("since")
    public Since getSince() {
        return since;
    }

    @JsonProperty("since")
    public void setSince(Since since) {
        this.since = since;
    }

    @JsonProperty("params")
    public Params getParams() {
        return params;
    }

    @JsonProperty("params")
    public void setParams(Params params) {
        this.params = params;
    }

    @JsonProperty("loc_valid")
    public String getLocValid() {
        return locValid;
    }

    @JsonProperty("loc_valid")
    public void setLocValid(String locValid) {
        this.locValid = locValid;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
