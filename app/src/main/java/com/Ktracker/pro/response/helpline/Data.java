
package com.Ktracker.pro.response.helpline;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "1",
    "2",
    "3"
})
public class Data implements Serializable
{

    @JsonProperty("1")
    private _1 _1;
    @JsonProperty("2")
    private _2 _2;
    @JsonProperty("3")
    private _3 _3;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private final static long serialVersionUID = -4574108874049303768L;

    @JsonProperty("1")
    public _1 get1() {
        return _1;
    }

    @JsonProperty("1")
    public void set1(_1 _1) {
        this._1 = _1;
    }

    @JsonProperty("2")
    public _2 get2() {
        return _2;
    }

    @JsonProperty("2")
    public void set2(_2 _2) {
        this._2 = _2;
    }

    @JsonProperty("3")
    public _3 get3() {
        return _3;
    }

    @JsonProperty("3")
    public void set3(_3 _3) {
        this._3 = _3;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
