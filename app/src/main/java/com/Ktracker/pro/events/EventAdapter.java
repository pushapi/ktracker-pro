package com.Ktracker.pro.events;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import com.Ktracker.pro.Constants;
import com.Ktracker.pro.R;
import com.Ktracker.pro.Utill;
import com.Ktracker.pro.location.CustomVO;
import com.Ktracker.pro.location.LocationActivity;
import com.Ktracker.pro.response.event.Datum;

public class EventAdapter extends RecyclerView.Adapter<EventAdapter.ViewHolder> {
    Context context;
    List<Datum> itemList;
    EventsActivity activity;


    public EventAdapter(Context context, List<Datum> itemList) {
        this.context = context;
        this.itemList = itemList;
        activity = (EventsActivity) context;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_list_event_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

        final Datum item = itemList.get(position);

        if (position == 0) {
            holder.name.setText("Vehicle");
            holder.date.setText("Date");
            holder.event.setText("Event");
            holder.location.setText("Map");
            holder.linearLayoutHeader.setVisibility(View.VISIBLE);
            holder.linearLayout.setVisibility(View.GONE);
       } else {
            holder.name.setText(item.getName());
            holder.date.setText(item.getDtTracker());
            holder.event.setText(item.getEventDesc());
            holder.location.setText("View");
            holder.linearLayoutHeader.setVisibility(View.GONE);
            holder.linearLayout.setVisibility(View.VISIBLE);
        }

        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(position!=0 && position+1 !=itemList.size()) {
                    CustomVO customVO = new CustomVO();
                    customVO.setLat(Utill.convertStringToDouble(item.getLat()));
                    customVO.setLng(Utill.convertStringToDouble(item.getLong()));
                    String styledText = "" +
                            "<font color='#008CA7'>Vehicle No:  </font>"+ item.getName() + " " +
                            "<br><font color='#008CA7'>Event:  </font>" + item.getEventDesc() +
                            "<br><font color='#008CA7'>Date:  </font>" + item.getDtTracker();
                    customVO.setText(styledText);
                    Intent intent = new Intent(context, LocationActivity.class);
                    intent.putExtra(Constants.INTENT_OBJECT, customVO);
                    context.startActivity(intent);
                }
            }
        });




    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView event,name,date,location;
        private LinearLayout linearLayout,linearLayoutHeader;

        public ViewHolder(View itemView) {
            super(itemView);
            date = itemView.findViewById(R.id.date);
            event = itemView.findViewById(R.id.event);
            name = itemView.findViewById(R.id.name);
            location = itemView.findViewById(R.id.location);
            linearLayout = itemView.findViewById(R.id.linearLayout);
            linearLayoutHeader = itemView.findViewById(R.id.linearLayoutHeader);

        }

    }
}
