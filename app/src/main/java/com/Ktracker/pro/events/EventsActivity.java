package com.Ktracker.pro.events;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import androidx.appcompat.widget.AppCompatButton;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import com.Ktracker.pro.Constants;
import com.Ktracker.pro.Http.HttpCallback;
import com.Ktracker.pro.Http.HttpRequestSingleton;
import com.Ktracker.pro.NetworkUtil;
import com.Ktracker.pro.R;
import com.Ktracker.pro.base.BaseActivity;
import com.Ktracker.pro.response.event.Datum;
import com.Ktracker.pro.response.event.EventResponse;
import com.Ktracker.pro.storage.SharedPreferenceUtil;

public class EventsActivity extends BaseActivity implements View.OnClickListener {
    private TextView   txtStartDate;
    private DatePickerDialog.OnDateSetListener date;
    final Calendar myCalendar = Calendar.getInstance();
    private boolean isStartDateSelected = false;
    private String selectedVehicleIMEINO="";
    private EditText edtlive;
    private Context context;
    private LayoutInflater inflater;
    private View view;
    private RecyclerView recyclerViewList;
    private LinearLayoutManager linearLayoutManager;
    private EventAdapter adapter;
    private AppCompatButton btnWatchReplay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);
        context = this;
        assignView();
        setListeners();
        callEventApi();
        setActionBar(true);
        setBackButtonVisibility(true);


    }



    private void callEventApi() {
        if (NetworkUtil.isOnline(context)) {
            showProgress("Loading...");
            HashMap<String, String> params = new HashMap<>();
            params.put("id", SharedPreferenceUtil.getString(Constants.SP_USER_ID,""));
            params.put("date",  new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date()));
//            params.put("limit", edtlive.getText().toString());
            new HttpRequestSingleton(context,getString(R.string.api_base_url)+ getString(R.string.api_event_api), params, 100, new HttpCallback() {
                @Override
                public void onResponse(String response, int action) {
                    stopProgress();
                    if (response != null) {
                        Log.i("response", "" + response.toString());
                        switch (action) {
                            case 100:
                                ObjectMapper mapper = new ObjectMapper();
                                try {
                                    EventResponse vehicleResponse = mapper.readValue(response, EventResponse.class);
                                    setAdapter(vehicleResponse.getData());


                                } catch (JsonProcessingException e) {
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }

                                break;
                            default:
                        }

                    } else {
                        Toast.makeText(context, "Something Went Wrong", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onError(VolleyError error, int action) {

                }
            });

        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_LONG).show();
        }
    }





    private DatePickerDialog.OnDateSetListener setDateListener(final TextView textView) {
        date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                updateLabel(textView);

            }

        };
        return date;

    }

    private void setListeners() {

        txtStartDate.setOnClickListener(this);
        btnWatchReplay.setOnClickListener(this);
    }

    private void assignView() {
        txtStartDate = (TextView) findViewById(R.id.txtStartDate);
        btnWatchReplay = (AppCompatButton) findViewById(R.id.btnWatchReplay);
        recyclerViewList = (RecyclerView) findViewById(R.id.recyclerViewList);
        edtlive = (EditText) findViewById(R.id.edtlive);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtStartDate:
                DatePickerDialog start = new DatePickerDialog(EventsActivity.this, setDateListener(txtStartDate), myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH));
                start.getDatePicker().setMaxDate(System.currentTimeMillis());
                start.show();
                break;

            case R.id.btnWatchReplay:
                { callEventApi();}

                break;



            default:
                break;
        }

    }
    private boolean isValidFields()
    {
       int a= Integer.valueOf(edtlive.getText().toString());
        {
            if(txtStartDate.getText().toString().equalsIgnoreCase("select date"))
            {
                Toast.makeText(context,"Please select date",Toast.LENGTH_LONG).show();
                return false;
            }
            if(a>=100 && a<=500)
            {
                return true;
            }
            else
            {
                Toast.makeText(context,"Limit should be between 100 to 500",Toast.LENGTH_LONG).show();
                return false;
            }

        }
    }



    private void updateLabel(TextView textView) {
        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        textView.setText(sdf.format(myCalendar.getTime()));
    }




    private void setAdapter(List<Datum> list) {
        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerViewList.setLayoutManager(linearLayoutManager);
        recyclerViewList.setItemAnimator(new DefaultItemAnimator());
        adapter = new EventAdapter(context, list);
        recyclerViewList.setAdapter(adapter);
    }




}
