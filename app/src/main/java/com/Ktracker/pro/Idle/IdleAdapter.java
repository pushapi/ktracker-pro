package com.Ktracker.pro.Idle;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import com.Ktracker.pro.Constants;
import com.Ktracker.pro.R;
import com.Ktracker.pro.Utill;
import com.Ktracker.pro.location.CustomVO;
import com.Ktracker.pro.location.LocationActivity;
import com.Ktracker.pro.response.idle.Datum;

public class IdleAdapter extends RecyclerView.Adapter<IdleAdapter.ViewHolder> {
    Context context;
    List<Datum> itemList;
    IdleActivity activity;


    public IdleAdapter(Context context, List<Datum> itemList) {
        this.context = context;
        this.itemList = itemList;
        activity = (IdleActivity) context;
    }



    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_list_idle_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

        final Datum item = itemList.get(position);

        if(position==0)
        {
            holder.date.setText("Date");
            holder.duration.setText("Duration");
            changeColor(holder,context.getResources().getColor(R.color.black));
            holder.linearLayoutHeader.setVisibility(View.VISIBLE);
            holder.linearLayout.setVisibility(View.GONE);
        }

      else if(position+1 == itemList.size())
      {
          holder.date.setText("Total="+item.getCount());
          holder.duration.setText(""+item.getTotalDuration());
          holder.linearLayoutHeader.setVisibility(View.GONE);
          holder.linearLayout.setVisibility(View.VISIBLE);
          changeColor(holder,context.getResources().getColor(R.color.colorAccent));
      }
      else
      {
          holder.date.setText(""+item.getDate());
          holder.duration.setText(""+item.getEngineWork());
          holder.linearLayoutHeader.setVisibility(View.GONE);
          holder.linearLayout.setVisibility(View.VISIBLE);
          changeColor(holder,context.getResources().getColor(R.color.black));
      }
        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(position!=0 && position+1 !=itemList.size()) {
                    CustomVO customVO = new CustomVO();
                    customVO.setLat(Utill.convertStringToDouble(item.getLat()));
                    customVO.setLng(Utill.convertStringToDouble(item.getLng()));

                    String styledText = "" +
                            "<font color='#008CA7'>Date:  </font>"+ item.getDate() + " " +
                            "<br><font color='#008CA7'>Duration:  </font>" + item.getEngineWork();
                    customVO.setText(styledText);
                    Intent intent = new Intent(context, LocationActivity.class);
                    intent.putExtra(Constants.INTENT_OBJECT, customVO);
                    context.startActivity(intent);
                }
            }
        });



    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private TextView date,duration;
        private LinearLayout linearLayout,linearLayoutHeader;

        public ViewHolder(View itemView) {
            super(itemView);
            date = itemView.findViewById(R.id.date);
            duration = itemView.findViewById(R.id.duration);
            linearLayout = itemView.findViewById(R.id.linearLayout);
            linearLayoutHeader = itemView.findViewById(R.id.linearLayoutHeader);

        }

    }


    public void changeColor(ViewHolder holder,int color)

    {
        holder.date.setTextColor(color);
        holder.duration.setTextColor(color);


    }
}
