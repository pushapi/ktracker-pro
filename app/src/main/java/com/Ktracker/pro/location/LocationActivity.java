package com.Ktracker.pro.location;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import androidx.fragment.app.FragmentActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.Ktracker.pro.Constants;
import com.Ktracker.pro.Http.HttpCallback;
import com.Ktracker.pro.Http.HttpRequestSingleton;
import com.Ktracker.pro.NetworkUtil;
import com.Ktracker.pro.R;

public class LocationActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    CustomVO item;
    List<CustomVO> customVOList = new ArrayList<>();
    LatLng latLng = new LatLng(0,0);
    LatLng latLng2 = new LatLng(0,0);
    private Context context;
    private TextView txtView;
    private RelativeLayout relativeMapType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);
        context = this;
        txtView = (TextView)findViewById(R.id.txtView);
        relativeMapType = (RelativeLayout) findViewById(R.id.relativeMapType);

        if(getIntent().hasExtra(Constants.INTENT_OBJECT)) {

            item = (CustomVO) getIntent().getSerializableExtra(Constants.INTENT_OBJECT);

            if (item.getText() != null && !item.getText().isEmpty()) {
                txtView.setText(Html.fromHtml(item.getText()), TextView.BufferType.SPANNABLE);
            }
            latLng = new LatLng(item.getLat(), item.getLng());
        }

        else if(getIntent().hasExtra(Constants.INTENT_LIST))
        {
            customVOList = (List<CustomVO>) getIntent().getSerializableExtra(Constants.INTENT_LIST);

            for (int i = 0; i <customVOList.size() ; i++) {
                if (customVOList.get(i).getText() != null && !customVOList.get(i).getText().isEmpty()) {
                    txtView.setText(Html.fromHtml(customVOList.get(i).getText()), TextView.BufferType.SPANNABLE);
                }
                if(i==0)
                latLng = new LatLng(customVOList.get(i).getLat(), customVOList.get(i).getLng());
                else
                    latLng2 = new LatLng(customVOList.get(i).getLat(), customVOList.get(i).getLng());
            }
        }

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        relativeMapType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(LocationActivity.this, "");

            }
        });
    }



    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng,15));
        mMap.setTrafficEnabled(false);
        callLocationApi(latLng,false);
        if(latLng2.latitude!=0)

        callLocationApi(latLng2 ,true);

    }

    private void callLocationApi(final LatLng latLng, final boolean b)
    {
        if (NetworkUtil.isOnline(context)) {
            HashMap<String, String> params = new HashMap<>();
            params.put("lat",String.valueOf( latLng.latitude));
            params.put("long",String.valueOf( latLng.longitude));
            new HttpRequestSingleton(context, getString(R.string.api_base_url)+getString(R.string.api_live_api), params, 100, new HttpCallback() {
                @Override
                public void onResponse(String response, int action) {
                    if (response != null) {
                        Log.i("response", "" + response.toString());
                        if(b)
                        {

                            Marker marker = mMap.addMarker(new MarkerOptions().position(latLng).title(response.toString()).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
                            mMap.setInfoWindowAdapter(new MyInfoWindowAdapter());
                            marker.showInfoWindow();
                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng,10));

                        }
                        else {

                            Marker marker = mMap.addMarker(new MarkerOptions().position(latLng).title(response.toString()));
                            mMap.setInfoWindowAdapter(new MyInfoWindowAdapter());
                            marker.showInfoWindow();
                        }


                    } else {
                        Toast.makeText(context, "Something Went Wrong", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onError(VolleyError error, int action) {

                }
            });

        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_LONG).show();
        }
    }

    public class MyInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

        private final View myContentsView;

        MyInfoWindowAdapter(){
            myContentsView = getLayoutInflater().inflate(R.layout.custom_marker_info, null);
        }

        @Override
        public View getInfoContents(Marker marker) {

            TextView text = ((TextView)myContentsView.findViewById(R.id.text));
           text.setText(marker.getTitle());

            return myContentsView;
        }

        @Override
        public View getInfoWindow(Marker marker) {
            // TODO Auto-generated method stub
            return null;
        }

    }

    public void showDialog(Activity activity, String msg) {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        Window window = dialog.getWindow();
        window.setGravity(Gravity.TOP);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.layout_custom);
        RadioGroup radioGroup;

        radioGroup = (RadioGroup) dialog.findViewById(R.id.radioGroup);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton rb = (RadioButton) group.findViewById(checkedId);
                if (null != rb && checkedId > -1) {

                    switch (rb.getText().toString().toLowerCase()) {
                        case "normal":
                            setCustomMap(GoogleMap.MAP_TYPE_NORMAL);
                            break;
                        case "satellite":
                            setCustomMap(GoogleMap.MAP_TYPE_SATELLITE);
                            break;
                        case "hybrid":
                            setCustomMap(GoogleMap.MAP_TYPE_HYBRID);
                            break;
                        default:
                            setCustomMap(GoogleMap.MAP_TYPE_NORMAL);
                            break;
                    }
                    dialog.cancel();
//                    Toast.makeText(MainActivity.this, rb.getText(), Toast.LENGTH_SHORT).show();
                }

            }
        });


        dialog.show();

    }

    private void setCustomMap(int mapType) {
        if (mMap != null) {


//            mMap.moveCamera(CameraUpdateFactory.newLatLng(lastknownLocation));



            mMap.setMapType(mapType);

        }

    }

}
