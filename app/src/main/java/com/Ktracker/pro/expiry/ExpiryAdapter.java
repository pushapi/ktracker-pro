package com.Ktracker.pro.expiry;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import com.Ktracker.pro.R;
import com.Ktracker.pro.response.expiry.ExpiryResponse;

public class ExpiryAdapter extends RecyclerView.Adapter<ExpiryAdapter.ViewHolder> {
    Context context;
    List<ExpiryResponse> itemList;
    ExpiryActivity activity;


    public ExpiryAdapter(Context context, List<ExpiryResponse> itemList) {
        this.context = context;
        this.itemList = itemList;
        activity = (ExpiryActivity) context;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_expiry_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {

        final ExpiryResponse item = itemList.get(position);
        holder.textName.setText(item.getName());
        holder.date.setText("Expiry "+item.getObjectExpireDt());

            if(item.getActive().equalsIgnoreCase("false"))
            {
                holder.imgchecked.setVisibility(View.GONE);
                holder.imgExpired.setVisibility(View.VISIBLE);


            }
            else
            {
                holder.imgchecked.setVisibility(View.VISIBLE);
                holder.imgExpired.setVisibility(View.GONE);
                holder.date.setVisibility(View.VISIBLE);

            }


    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private CardView card_view;
        private TextView date, textName;
        private ImageView imgExpired, imgchecked;

        public ViewHolder(View itemView) {
            super(itemView);

            card_view = (CardView) itemView.findViewById(R.id.card_view);
            date = (TextView) itemView.findViewById(R.id.date);
            textName = (TextView) itemView.findViewById(R.id.textName);
            imgchecked = (ImageView) itemView.findViewById(R.id.imgchecked);
            imgExpired = (ImageView) itemView.findViewById(R.id.imgExpired);

        }

    }
}
