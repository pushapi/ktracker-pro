package com.Ktracker.pro.expiry;

import android.content.Context;
import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.Ktracker.pro.Constants;
import com.Ktracker.pro.Http.HttpCallback;
import com.Ktracker.pro.Http.HttpRequestSingleton;
import com.Ktracker.pro.NetworkUtil;
import com.Ktracker.pro.R;
import com.Ktracker.pro.base.BaseActivity;
import com.Ktracker.pro.response.expiry.ExpiryResponse;
import com.Ktracker.pro.storage.SharedPreferenceUtil;

public class ExpiryActivity extends BaseActivity {
    private RecyclerView recyclerViewList;
    private LinearLayoutManager linearLayoutManager;
    private ExpiryAdapter childAdapter;
    private Context context;
    private ArrayList<ExpiryResponse> list= new ArrayList<>();




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expiry);
        context = this;
        assignViews();
        setActionBar(true);
        setBackButtonVisibility(true);
        callExpiryApi();
    }
    private void assignViews() {
        recyclerViewList = (RecyclerView)findViewById(R.id.recyclerViewList);
    }

    private void setAdapter(List<ExpiryResponse> childList) {
        linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        recyclerViewList.setLayoutManager(linearLayoutManager);
        recyclerViewList.setItemAnimator(new DefaultItemAnimator());
        childAdapter = new ExpiryAdapter(context, childList);
        recyclerViewList.setAdapter(childAdapter);
    }
    private void callExpiryApi() {
        if (NetworkUtil.isOnline(context)) {
            showProgress("Loading...");
            HashMap<String, String> params = new HashMap<>();
            params.put("key", SharedPreferenceUtil.getString(Constants.SP_USER_KEY, ""));
            params.put("api", "user");
            params.put("ver", "1.0");
            params.put("cmd", "USER_GET_OBJECTS");
            new HttpRequestSingleton(context, getString(R.string.api_base_url)+getString(R.string.api_expiry_api), params, 100, new HttpCallback() {
                @Override
                public void onResponse(String response, int action) {
                    stopProgress();
                    if (response != null) {
                        Log.i("response", "" + response.toString());
                        switch (action) {
                            case 100:
                                ObjectMapper mapper = new ObjectMapper();
                                try {
                                    JSONArray array= new JSONArray(response);

                                    for (int i = 0; i <array.length() ; i++) {
                                        ExpiryResponse vehicleResponse = mapper.readValue(array.get(i).toString(), ExpiryResponse.class);
                                        list.add(vehicleResponse);

                                    }

                                    sortResponse(list);


                                } catch (JsonProcessingException e) {
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                break;
                            default:
                        }

                    } else {
                        Toast.makeText(context, "Something Went Wrong", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onError(VolleyError error, int action) {

                }
            });

        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_LONG).show();
        }
    }

    private void sortResponse(List<ExpiryResponse> allList) {
        List<ExpiryResponse> list = new ArrayList<>();

        for (ExpiryResponse vehicle : allList
                ) {
            if (vehicle.getActive().equalsIgnoreCase("true")) {
                list.add(vehicle);
            } else {

            }

        }

        setAdapter(allList);
    }
}
