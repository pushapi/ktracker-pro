package com.Ktracker.pro.overspeed;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import com.Ktracker.pro.Constants;
import com.Ktracker.pro.R;
import com.Ktracker.pro.Utill;
import com.Ktracker.pro.location.CustomVO;
import com.Ktracker.pro.location.LocationActivity;
import com.Ktracker.pro.response.overspeed.Datum;

public class OverSpeedAdapter extends RecyclerView.Adapter<OverSpeedAdapter.ViewHolder> {
    Context context;
    List<Datum> itemList;
    OverSpeedActivity activity;


    public OverSpeedAdapter(Context context, List<Datum> itemList) {
        this.context = context;
        this.itemList = itemList;
        activity = (OverSpeedActivity) context;
    }



    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_list_over_speed_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {

        final Datum item = itemList.get(position);

        if(position==0)
        {
            holder.date.setText("Time");
            holder.speed.setText("Speed");
            holder.linearLayoutHeader.setVisibility(View.VISIBLE);
            holder.linearLayout.setVisibility(View.GONE);
        }


      else
      {
          holder.date.setText(""+item.getDtTracker());
          holder.speed.setText(""+item.getSpeed());
          holder.linearLayoutHeader.setVisibility(View.GONE);
          holder.linearLayout.setVisibility(View.VISIBLE);
      }

      holder.linearLayout.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {


              CustomVO customVO = new CustomVO();
              customVO.setLat(Utill.convertStringToDouble(item.getLat()));
              customVO.setLng(Utill.convertStringToDouble(item.getLng()));
              String styledText = "" +
                      "<font color='#008CA7'>Vehicle No:  </font>"+ activity.selectedVehicle + " " +
                      "<br><font color='#008CA7'>Date:  </font>" + item.getDtTracker() +
                      "<br><font color='#008CA7'>Speed:  </font>" + item.getSpeed();
              customVO.setText(styledText);
              Intent intent = new Intent(context,LocationActivity.class);
              intent.putExtra(Constants.INTENT_OBJECT,customVO);
              context.startActivity(intent);
          }
      });




    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private TextView date,speed;
        private LinearLayout linearLayout,linearLayoutHeader;

        public ViewHolder(View itemView) {
            super(itemView);
            date = itemView.findViewById(R.id.date);
            speed = itemView.findViewById(R.id.speed);
            linearLayout = itemView.findViewById(R.id.linearLayout);
            linearLayoutHeader = itemView.findViewById(R.id.linearLayoutHeader);

        }

    }
}
