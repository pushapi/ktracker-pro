package com.Ktracker.pro;

import android.widget.TextView;

import com.Ktracker.pro.settings.Icon;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class Utill {

    public static Double convertStringToDouble(String value) {
        Double returnValue = 0d;
        try {
            returnValue = Double.parseDouble(value);
            return returnValue;
        } catch (Exception e) {
            e.printStackTrace();
            return returnValue;
        }
    }

//    "icon":"ambulance",
//            "icon":"arrow",
//            "icon":"bike",
//            "icon":"bus",
//            "icon":"car",
//            "icon":"jcb",
//            "icon":"person",
//            "icon":"tanker",
//            "icon":"taxi",
//            "icon":"truck",

    public static int getIcon(String name) {
        int returnValue;
        switch (name) {
            case "ambulance":
                returnValue = R.drawable.ambulance_running;
                break;
            case "arrow":
                returnValue = R.drawable.arrow_running;
                break;
            case "bike":
                returnValue = R.drawable.bike_running;
                break;

            case "bus":
                returnValue = R.drawable.bus_running;
                break;

            case "car":
                returnValue = R.drawable.car_running;
                break;

            case "jcb":
                returnValue = R.drawable.jcb_running;
                break;

            case "truck":
                returnValue = R.drawable.truck_running;
                break;

            case "person":
                returnValue = R.drawable.person_running;
                break;

            case "tanker":
                returnValue = R.drawable.tanker_running;
                break;
            case "taxi":
                returnValue = R.drawable.taxi_running;
                break;
            case "boat":
                returnValue = R.drawable.boat_running;
                break;
            case "crane":
                returnValue = R.drawable.crane_running;
                break;
            case "cycle":
                returnValue = R.drawable.cycle_running;
                break;
            case "pavor":
                returnValue = R.drawable.pavor_running;
                break;
            case "pickup":
                returnValue = R.drawable.pickup_running;
                break;
            case "rickshaw":
                returnValue = R.drawable.rickshaw_running;
                break;
            case "roller":
                returnValue = R.drawable.roller_running;
                break;
            case "tractor":
                returnValue = R.drawable.tractor_running;
                break;


            default:
                returnValue = R.drawable.idle;
        }
        return returnValue;
    }

    public static int getIconAndColor(String icon, String status) {

        int returnValue = R.drawable.idle;
        switch (icon) {
            case "ambulance":

                if (status.equalsIgnoreCase("running")) {
                    returnValue = R.drawable.ambulance_running;
                } else if (status.equalsIgnoreCase("stopped")) {
                    returnValue = R.drawable.ambulance_stop;

                } else if (status.equalsIgnoreCase("offline")) {
                    returnValue = R.drawable.ambulance_offline;

                } else {
                    returnValue = R.drawable.ambulance_idle;
                }

                break;
            case "arrow":

                if (status.equalsIgnoreCase("running")) {
                    returnValue = R.drawable.arrow_running;
                } else if (status.equalsIgnoreCase("stopped")) {
                    returnValue = R.drawable.arrow_stop;

                } else if (status.equalsIgnoreCase("offline")) {
                    returnValue = R.drawable.arrow_offline;

                } else {
                    returnValue = R.drawable.arrow_idle;
                }

                break;
            case "bike":

                if (status.equalsIgnoreCase("running")) {
                    returnValue = R.drawable.bike_running;
                } else if (status.equalsIgnoreCase("stopped")) {
                    returnValue = R.drawable.bike_stop;

                } else if (status.equalsIgnoreCase("offline")) {
                    returnValue = R.drawable.bike_offline;

                } else {
                    returnValue = R.drawable.bike_idle;
                }

                break;

            case "bus":

                if (status.equalsIgnoreCase("running")) {
                    returnValue = R.drawable.bus_running;
                } else if (status.equalsIgnoreCase("stopped")) {
                    returnValue = R.drawable.bus_stop;

                } else if (status.equalsIgnoreCase("offline")) {
                    returnValue = R.drawable.bus_offline;

                } else {
                    returnValue = R.drawable.bus_idle;
                }

                break;

            case "car":

                if (status.equalsIgnoreCase("running")) {
                    returnValue = R.drawable.car_running;
                } else if (status.equalsIgnoreCase("stopped")) {
                    returnValue = R.drawable.car_stop;

                } else if (status.equalsIgnoreCase("offline")) {
                    returnValue = R.drawable.car_offline;

                } else {
                    returnValue = R.drawable.car_idle;
                }

                break;

            case "jcb":

                if (status.equalsIgnoreCase("running")) {
                    returnValue = R.drawable.jcb_running;
                } else if (status.equalsIgnoreCase("stopped")) {
                    returnValue = R.drawable.jcb_stop;

                } else if (status.equalsIgnoreCase("offline")) {
                    returnValue = R.drawable.jcb_offline;

                } else {
                    returnValue = R.drawable.jcb_idle;
                }

                break;

            case "person":

                if (status.equalsIgnoreCase("running")) {
                    returnValue = R.drawable.person_running;
                } else if (status.equalsIgnoreCase("stopped")) {
                    returnValue = R.drawable.person_stop;

                } else if (status.equalsIgnoreCase("offline")) {
                    returnValue = R.drawable.person_offline;

                } else {
                    returnValue = R.drawable.person_idle;
                }

                break;

            case "tanker":

                if (status.equalsIgnoreCase("running")) {
                    returnValue = R.drawable.tanker_running;
                } else if (status.equalsIgnoreCase("stopped")) {
                    returnValue = R.drawable.tanker_stop;

                } else if (status.equalsIgnoreCase("offline")) {
                    returnValue = R.drawable.tanker_offline;

                } else {
                    returnValue = R.drawable.tanker_idle;
                }

                break;

            case "taxi":

                if (status.equalsIgnoreCase("running")) {
                    returnValue = R.drawable.taxi_running;
                } else if (status.equalsIgnoreCase("stopped")) {
                    returnValue = R.drawable.taxi_stop;

                } else if (status.equalsIgnoreCase("offline")) {
                    returnValue = R.drawable.taxi_offline;

                } else {
                    returnValue = R.drawable.taxi_idle;
                }

                break;

            case "truck":

                if (status.equalsIgnoreCase("running")) {
                    returnValue = R.drawable.truck_running;
                } else if (status.equalsIgnoreCase("stopped")) {
                    returnValue = R.drawable.truck_stop;

                } else if (status.equalsIgnoreCase("offline")) {
                    returnValue = R.drawable.truck_offline;

                } else {
                    returnValue = R.drawable.truck_idle;
                }

                break;

            case "boat":
                if (status.equalsIgnoreCase("running")) {
                    returnValue = R.drawable.boat_running;
                } else if (status.equalsIgnoreCase("stopped")) {
                    returnValue = R.drawable.boat_stop;

                } else if (status.equalsIgnoreCase("offline")) {
                    returnValue = R.drawable.boat_offline;

                } else {
                    returnValue = R.drawable.boat_idle;
                }
                break;
            case "crane":
                if (status.equalsIgnoreCase("running")) {
                    returnValue = R.drawable.crane_running;
                } else if (status.equalsIgnoreCase("stopped")) {
                    returnValue = R.drawable.crane_stop;

                } else if (status.equalsIgnoreCase("offline")) {
                    returnValue = R.drawable.crane_offline;

                } else {
                    returnValue = R.drawable.crane_idle;
                }
                break;
            case "cycle":
                if (status.equalsIgnoreCase("running")) {
                    returnValue = R.drawable.cycle_running;
                } else if (status.equalsIgnoreCase("stopped")) {
                    returnValue = R.drawable.cycle_stop;

                } else if (status.equalsIgnoreCase("offline")) {
                    returnValue = R.drawable.cycle_offline;

                } else {
                    returnValue = R.drawable.cycle_idle;
                }
                break;
            case "pavor":
                if (status.equalsIgnoreCase("running")) {
                    returnValue = R.drawable.pavor_running;
                } else if (status.equalsIgnoreCase("stopped")) {
                    returnValue = R.drawable.pavor_stop;

                } else if (status.equalsIgnoreCase("offline")) {
                    returnValue = R.drawable.pavor_offline;

                } else {
                    returnValue = R.drawable.pavor_idle;
                }
                break;
            case "pickup":
                if (status.equalsIgnoreCase("running")) {
                    returnValue = R.drawable.pickup_running;
                } else if (status.equalsIgnoreCase("stopped")) {
                    returnValue = R.drawable.pickup_stop;

                } else if (status.equalsIgnoreCase("offline")) {
                    returnValue = R.drawable.pickup_offline;

                } else {
                    returnValue = R.drawable.pickup_idle;
                }
                break;
            case "rickshaw":
                if (status.equalsIgnoreCase("running")) {
                    returnValue = R.drawable.rickshaw_running;
                } else if (status.equalsIgnoreCase("stopped")) {
                    returnValue = R.drawable.rickshaw_stop;

                } else if (status.equalsIgnoreCase("offline")) {
                    returnValue = R.drawable.rickshaw_offline;

                } else {
                    returnValue = R.drawable.rickshaw_idle;
                }
                break;
            case "roller":
                if (status.equalsIgnoreCase("running")) {
                    returnValue = R.drawable.roller_running;
                } else if (status.equalsIgnoreCase("stopped")) {
                    returnValue = R.drawable.roller_stop;

                } else if (status.equalsIgnoreCase("offline")) {
                    returnValue = R.drawable.roller_offline;

                } else {
                    returnValue = R.drawable.roller_idle;
                }
                break;
            case "tractor":
                if (status.equalsIgnoreCase("running")) {
                    returnValue = R.drawable.tractor_running;
                } else if (status.equalsIgnoreCase("stopped")) {
                    returnValue = R.drawable.tractor_stop;

                } else if (status.equalsIgnoreCase("offline")) {
                    returnValue = R.drawable.tractor_offline;

                } else {
                    returnValue = R.drawable.tractor_idle;
                }
                break;


            default:
                returnValue = R.drawable.idle;
        }
        return returnValue;


    }

    public static int getMapIconAndColor(String icon, String status) {

        int returnValue = R.drawable.idle_live;
        switch (icon) {
            case "ambulance":

                if (status.equalsIgnoreCase("running")) {
                    returnValue = R.drawable.live_ambulance_running;
                } else if (status.equalsIgnoreCase("stopped")) {
                    returnValue = R.drawable.live_ambulance_stop;

                } else if (status.equalsIgnoreCase("offline")) {
                    returnValue = R.drawable.live_ambulance_offline;

                } else {
                    returnValue = R.drawable.live_ambulance_idle;
                }

                break;
            case "arrow":

                if (status.equalsIgnoreCase("running")) {
                    returnValue = R.drawable.live_arrow_running;
                } else if (status.equalsIgnoreCase("stopped")) {
                    returnValue = R.drawable.live_arrow_stop;

                } else if (status.equalsIgnoreCase("offline")) {
                    returnValue = R.drawable.live_arrow_offline;

                } else {
                    returnValue = R.drawable.live_arrow_idle;
                }

                break;
            case "bike":

                if (status.equalsIgnoreCase("running")) {
                    returnValue = R.drawable.live_bike_running;
                } else if (status.equalsIgnoreCase("stopped")) {
                    returnValue = R.drawable.live_bike_stop;

                } else if (status.equalsIgnoreCase("offline")) {
                    returnValue = R.drawable.live_bike_offline;

                } else {
                    returnValue = R.drawable.live_bike_idle;
                }

                break;

            case "bus":

                if (status.equalsIgnoreCase("running")) {
                    returnValue = R.drawable.live_bus_running;
                } else if (status.equalsIgnoreCase("stopped")) {
                    returnValue = R.drawable.live_bus_stop;

                } else if (status.equalsIgnoreCase("offline")) {
                    returnValue = R.drawable.live_bus_offline;

                } else {
                    returnValue = R.drawable.live_bus_idle;
                }

                break;

            case "car":

                if (status.equalsIgnoreCase("running")) {
                    returnValue = R.drawable.live_car_running;
                } else if (status.equalsIgnoreCase("stopped")) {
                    returnValue = R.drawable.live_car_stop;

                } else if (status.equalsIgnoreCase("offline")) {
                    returnValue = R.drawable.live_car_offline;

                } else {
                    returnValue = R.drawable.live_car_idle;
                }

                break;

            case "jcb":

                if (status.equalsIgnoreCase("running")) {
                    returnValue = R.drawable.live_jcb_running;
                } else if (status.equalsIgnoreCase("stopped")) {
                    returnValue = R.drawable.live_jcb_stop;

                } else if (status.equalsIgnoreCase("offline")) {
                    returnValue = R.drawable.live_jcb_offline;

                } else {
                    returnValue = R.drawable.live_jcb_idle;
                }

                break;

            case "person":

                if (status.equalsIgnoreCase("running")) {
                    returnValue = R.drawable.live_person_running;
                } else if (status.equalsIgnoreCase("stopped")) {
                    returnValue = R.drawable.live_person_stop;

                } else if (status.equalsIgnoreCase("offline")) {
                    returnValue = R.drawable.live_person_offline;

                } else {
                    returnValue = R.drawable.live_person_idle;
                }

                break;

            case "tanker":

                if (status.equalsIgnoreCase("running")) {
                    returnValue = R.drawable.live_tanker_running;
                } else if (status.equalsIgnoreCase("stopped")) {
                    returnValue = R.drawable.live_tanker_stop;

                } else if (status.equalsIgnoreCase("offline")) {
                    returnValue = R.drawable.live_tanker_offline;

                } else {
                    returnValue = R.drawable.live_tanker_idle;
                }

                break;

            case "taxi":

                if (status.equalsIgnoreCase("running")) {
                    returnValue = R.drawable.live_taxi_running;
                } else if (status.equalsIgnoreCase("stopped")) {
                    returnValue = R.drawable.live_taxi_stop;

                } else if (status.equalsIgnoreCase("offline")) {
                    returnValue = R.drawable.live_taxi_offline;

                } else {
                    returnValue = R.drawable.live_taxi_idle;
                }

                break;

            case "truck":

                if (status.equalsIgnoreCase("running")) {
                    returnValue = R.drawable.live_truck_running;
                } else if (status.equalsIgnoreCase("stopped")) {
                    returnValue = R.drawable.live_truck_stop;

                } else if (status.equalsIgnoreCase("offline")) {
                    returnValue = R.drawable.live_truck_offline;

                } else {
                    returnValue = R.drawable.live_truck_idle;
                }

                break;


            case "boat":
                if (status.equalsIgnoreCase("running")) {
                    returnValue = R.drawable.live_boat_running;
                } else if (status.equalsIgnoreCase("stopped")) {
                    returnValue = R.drawable.live_boat_stop;

                } else if (status.equalsIgnoreCase("offline")) {
                    returnValue = R.drawable.live_boat_offline;

                } else {
                    returnValue = R.drawable.live_boat_idle;
                }
                break;
            case "crane":
                if (status.equalsIgnoreCase("running")) {
                    returnValue = R.drawable.live_crane_running;
                } else if (status.equalsIgnoreCase("stopped")) {
                    returnValue = R.drawable.live_crane_stop;

                } else if (status.equalsIgnoreCase("offline")) {
                    returnValue = R.drawable.live_crane_offline;

                } else {
                    returnValue = R.drawable.live_crane_idle;
                }
                break;
            case "cycle":
                if (status.equalsIgnoreCase("running")) {
                    returnValue = R.drawable.live_cycle_running;
                } else if (status.equalsIgnoreCase("stopped")) {
                    returnValue = R.drawable.live_cycle_stop;

                } else if (status.equalsIgnoreCase("offline")) {
                    returnValue = R.drawable.live_cycle_offline;

                } else {
                    returnValue = R.drawable.live_cycle_idle;
                }
                break;
            case "pavor":
                if (status.equalsIgnoreCase("running")) {
                    returnValue = R.drawable.live_pavor_running;
                } else if (status.equalsIgnoreCase("stopped")) {
                    returnValue = R.drawable.live_pavor_stop;

                } else if (status.equalsIgnoreCase("offline")) {
                    returnValue = R.drawable.live_pavor_offline;

                } else {
                    returnValue = R.drawable.live_pavor_idle;
                }
                break;
            case "pickup":
                if (status.equalsIgnoreCase("running")) {
                    returnValue = R.drawable.live_pickup_running;
                } else if (status.equalsIgnoreCase("stopped")) {
                    returnValue = R.drawable.live_pickup_stop;

                } else if (status.equalsIgnoreCase("offline")) {
                    returnValue = R.drawable.live_pickup_offline;

                } else {
                    returnValue = R.drawable.live_pickup_idle;
                }
                break;
            case "rickshaw":
                if (status.equalsIgnoreCase("running")) {
                    returnValue = R.drawable.live_rickshaw_running;
                } else if (status.equalsIgnoreCase("stopped")) {
                    returnValue = R.drawable.live_rickshaw_stop;

                } else if (status.equalsIgnoreCase("offline")) {
                    returnValue = R.drawable.live_rickshaw_offline;

                } else {
                    returnValue = R.drawable.live_rickshaw_idle;
                }
                break;
            case "roller":
                if (status.equalsIgnoreCase("running")) {
                    returnValue = R.drawable.live_roller_running;
                } else if (status.equalsIgnoreCase("stopped")) {
                    returnValue = R.drawable.live_roller_stop;

                } else if (status.equalsIgnoreCase("offline")) {
                    returnValue = R.drawable.live_roller_offline;

                } else {
                    returnValue = R.drawable.live_roller_idle;
                }
                break;
            case "tractor":
                if (status.equalsIgnoreCase("running")) {
                    returnValue = R.drawable.live_tractor_running;
                } else if (status.equalsIgnoreCase("stopped")) {
                    returnValue = R.drawable.live_tractor_stop;

                } else if (status.equalsIgnoreCase("offline")) {
                    returnValue = R.drawable.live_tractor_offline;

                } else {
                    returnValue = R.drawable.live_tractor_idle;
                }
                break;


            default:
                returnValue = R.drawable.idle_live;
        }
        return returnValue;


    }

    public static void setToday(TextView textView) {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
        textView.setText(format1.format(cal.getTime()));
    }

    public static String convertIntoDigitalClockFormat(String str) {
        try {
            str = str.replaceAll("\\s", "");
            str = str.substring(0, str.indexOf("min"));
            str = str.replace("h", ":");
            if (str.indexOf(":") == 1)
                str = "0" + str;
            return str;
        } catch (Exception e) {
            return str;
        }

    }

    public static String convertIntoDigitalClockFormat(Integer seconds) {
        Integer p1 = seconds % 60;
        Integer p2 = seconds / 60;
        Integer p3 = p2 % 60;
        p2 = p2 / 60;


        return String.format("%02d", p2) + ":" + String.format("%02d", p3) + ":" + String.format("%02d", p1);

    }

    public static List<Icon> getIconNameList() {


        List<Icon> list = new ArrayList<>();
        Icon icon = new Icon();
        icon.setName("Bike");
        icon.setIcon(R.drawable.bike_running);
        list.add(icon);
        icon = new Icon();

        icon.setName("Car");
        icon.setIcon(R.drawable.car_running);
        list.add(icon);
        icon = new Icon();

        icon.setName("Truck");
        icon.setIcon(R.drawable.truck_running);
        list.add(icon);
        icon = new Icon();

        icon.setName("Bus");
        icon.setIcon(R.drawable.bus_running);
        list.add(icon);
        icon = new Icon();

        icon.setName("Taxi");
        icon.setIcon(R.drawable.taxi_running);
        list.add(icon);
        icon = new Icon();

        icon.setName("Tanker");
        icon.setIcon(R.drawable.tanker_running);
        list.add(icon);
        icon = new Icon();

        icon.setName("Jcb");
        icon.setIcon(R.drawable.jcb_running);
        list.add(icon);
        icon = new Icon();

        icon.setName("Ambulance");
        icon.setIcon(R.drawable.ambulance_running);
        list.add(icon);
        icon = new Icon();

        icon.setName("Person");
        icon.setIcon(R.drawable.person_running);
        list.add(icon);
        icon = new Icon();

        icon.setName("Arrow");
        icon.setIcon(R.drawable.arrow_running);
        list.add(icon);
        icon = new Icon();

        icon.setName("Boat");
        icon.setIcon(R.drawable.boat_running);
        list.add(icon);
        icon = new Icon();

        icon.setName("Crane");
        icon.setIcon(R.drawable.crane_running);
        list.add(icon);
        icon = new Icon();

        icon.setName("Cycle");
        icon.setIcon(R.drawable.cycle_running);
        list.add(icon);
        icon = new Icon();

        icon.setName("Pavor");
        icon.setIcon(R.drawable.pavor_running);
        list.add(icon);
        icon = new Icon();

        icon.setName("Pickup");
        icon.setIcon(R.drawable.pickup_running);
        list.add(icon);
        icon = new Icon();

        icon.setName("Rickshaw");
        icon.setIcon(R.drawable.rickshaw_running);
        list.add(icon);
        icon = new Icon();

        icon.setName("Roller");
        icon.setIcon(R.drawable.roller_running);
        list.add(icon);
        icon = new Icon();

        icon.setName("Tractor");
        icon.setIcon(R.drawable.tractor_running);
        list.add(icon);
        icon = new Icon();

        icon.setName("Default");
        icon.setIcon(R.drawable.idle);
        list.add(icon);

        return list;
    }

    public String convertTwoDecimal(String s) {
        try {
            Double value = Double.valueOf(s);
            return String.format("%.2f", value);
        } catch (NumberFormatException e) {
            return s;
        }

    }
    public static String convertTwoDecimal(double d) {
        try {
            return String.format("%.2f", d);
        } catch (NumberFormatException e) {
            return String.valueOf(d);
        }

    }
}
