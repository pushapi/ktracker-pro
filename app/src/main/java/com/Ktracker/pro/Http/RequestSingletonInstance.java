/*
 * Copyright (c) 2018.
 * Author Ashish Patel
 * created at 3/4/18 11:17 AM
 */

package com.Ktracker.pro.Http;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by Trushit on 23/11/15.
 */
public class RequestSingletonInstance {
    private static RequestSingletonInstance mInstance;
    private RequestQueue mRequestQueue;
    private static Context mCtx;

    private RequestSingletonInstance(Context context) {
        mCtx = context;
        mRequestQueue = getRequestQueue();
    }

    public static synchronized RequestSingletonInstance getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new RequestSingletonInstance(context);
        }
        return mInstance;
    }

    private RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            // getApplicationContext() is key, it keeps you from leaking the
            // Activity or BroadcastReceiver if someone passes one in.
            mRequestQueue = Volley.newRequestQueue(mCtx.getApplicationContext());
        }
        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req) {
        getRequestQueue().add(req);
    }
}
