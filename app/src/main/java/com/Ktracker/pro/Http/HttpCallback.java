/*
 * Copyright (c) 2018.
 * Author Ashish Patel
 * created at 3/4/18 11:17 AM
 */

package com.Ktracker.pro.Http;


import com.android.volley.VolleyError;

/**
 * HttpCallBack Interface for Api
 */
@SuppressWarnings("ALL")
public interface HttpCallback {
   
	/**
	 * Callback method for http response. called when an http response is received.
	 * @param response
	 * @param action
	 */
    public void onResponse(String response, int action);
    public void onError(VolleyError error, int action);
    
}