package com.Ktracker.pro.home;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.core.view.GravityCompat;
import androidx.core.view.MenuItemCompat;
import androidx.viewpager.widget.ViewPager;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.Ktracker.pro.Constants;
import com.Ktracker.pro.Http.HttpCallback;
import com.Ktracker.pro.Http.HttpRequestSingleton;
import com.Ktracker.pro.Http.HttpRequestSingletonPost;
import com.Ktracker.pro.Idle.IdleActivity;
import com.Ktracker.pro.LoginActivity;
import com.Ktracker.pro.NetworkUtil;
import com.Ktracker.pro.R;
import com.Ktracker.pro.ac.AcActivity;
import com.Ktracker.pro.distance.DistanceActivity;
import com.Ktracker.pro.engine.EngineActivity;
import com.Ktracker.pro.events.EventsActivity;
import com.Ktracker.pro.expiry.ExpiryActivity;
import com.Ktracker.pro.ignition.IgnitionActivity;
import com.Ktracker.pro.move.MovementActivity;
import com.Ktracker.pro.overspeed.OverSpeedActivity;
import com.Ktracker.pro.replay.TimeSelctionActivity;
import com.Ktracker.pro.response.helpline.HelpResponse;
import com.Ktracker.pro.response.vehicle.Datum;
import com.Ktracker.pro.response.vehicle.VehicleResponse;
import com.Ktracker.pro.service.ServiceActivity;
import com.Ktracker.pro.settings.SettingActivity;
import com.Ktracker.pro.stop.StopActivity;
import com.Ktracker.pro.storage.SharedPreferenceUtil;
import com.Ktracker.pro.summary.SummaryActivity;
import com.Ktracker.pro.tracking.MapsClusterActivity;
import com.android.volley.VolleyError;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private TextView txtViewUser;
    private PopupWindow pw;

    private Context context;
    private ArrayList<Datum> All = new ArrayList<>();
    private ArrayList<Datum> running = new ArrayList<>();
    private ArrayList<Datum> ideal = new ArrayList<>();
    private ArrayList<Datum> stopped = new ArrayList<>();
    private ArrayList<Datum> offline = new ArrayList<>();
    private ArrayList<Datum> searchList = new ArrayList<>();
    private int selectedTabPosition = 0;
    private boolean isSearchRequested = false;
    private static int TIME_INTERVAL;
    private static Timer myTimer;
    private boolean isStarted = false;
    private boolean isScheduled = false;
    private Handler handler = new Handler();
    private TimerTask timerTask;
    private Runnable runnable;
    private SwipeRefreshLayout swiperefresh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        myTimer = new Timer();


        TIME_INTERVAL = SharedPreferenceUtil.getInt(Constants.SP_IS_USER_PREFERENCE_DASHBOARD, Constants.PREFERENCE_DASHBOARD_DEFAULT) * 1000;
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        context = this;
        runnable = new Runnable() {
            @Override
            public void run() {
                if (!isSearchRequested)
                    callVehicleApiFirstTime();
                handler.postDelayed(this, TIME_INTERVAL);
            }
        };
        timerTask = new TimerTask() {
            @Override
            public void run() {
                isStarted = true;
                if (!isSearchRequested)
                    callVehicleApiFirstTime();
            }

        };

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        swiperefresh = (SwipeRefreshLayout) findViewById(R.id.swiperefresh);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View headerView = LayoutInflater.from(this).inflate(R.layout.nav_header_main, navigationView, false);
        txtViewUser = (TextView) navigationView.getHeaderView(0).findViewById(R.id.txtViewUser);
        txtViewUser.setText("User:" + SharedPreferenceUtil.getString(Constants.SP_USERNAME, ""));
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);
        startTimer(TIME_INTERVAL);

        swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (!isSearchRequested) {
                    callVehicleApiFirstTime();
                }
            }
        });


    }


    private void startTimer(int timeInterval) {
        if (!isStarted) {
            isStarted = true;
            handler.postDelayed(runnable, timeInterval);
        }
    }

    private void createTabIcons(List<Datum> list) {


        View view = LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        TextView tabOne = (TextView) view.findViewById(R.id.tab);
        tabOne.setText("TOTAL\n" + list.size());
        tabLayout.getTabAt(0).setCustomView(view);

        View view2 = LayoutInflater.from(this).inflate(R.layout.custom_tab_green, null);

        TextView tabTwo = (TextView) view2.findViewById(R.id.tab);
        tabTwo.setText("RUNNING\n" + running.size());
        tabLayout.getTabAt(1).setCustomView(view2);

        View view3 = LayoutInflater.from(this).inflate(R.layout.custom_tab_yellow, null);

        TextView tabThree = (TextView) view3.findViewById(R.id.tab);
        tabThree.setText("IDLE\n" + ideal.size());
        tabLayout.getTabAt(2).setCustomView(view3);

        View view4 = LayoutInflater.from(this).inflate(R.layout.custom_tab_red, null);

        TextView tabFour = (TextView) view4.findViewById(R.id.tab);
        tabFour.setText("STOPPED\n" + stopped.size());
        tabLayout.getTabAt(3).setCustomView(view4);

        View view5 = LayoutInflater.from(this).inflate(R.layout.custom_tab_black, null);

        TextView tabFive = (TextView) view5.findViewById(R.id.tab);
        tabFive.setText("OFFLINE\n" + offline.size());
        tabLayout.getTabAt(4).setCustomView(view5);
    }


    private void createViewPager(ViewPager viewPager, List<Datum> list) {
        All.clear();
        All.addAll(list);

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        Fragment fragmentAll = new TotalFragment();
        Bundle bundle = new Bundle();
        ArrayList<Datum> data = new ArrayList<>();
        data.addAll(list);
        bundle.putSerializable("list", All);
        fragmentAll.setArguments(bundle);
        adapter.addFrag(fragmentAll, "T=" + data.size());

        Fragment fragmentRunning = new RunningFragment();
        bundle = new Bundle();
        bundle.putSerializable("list", running);
        fragmentRunning.setArguments(bundle);
        adapter.addFrag(fragmentRunning, "R=" + data.size());

        Fragment fragmentIdeal = new IdealFragment();
        bundle = new Bundle();
        bundle.putSerializable("list", ideal);
        fragmentIdeal.setArguments(bundle);
        adapter.addFrag(fragmentIdeal, "I=" + data.size());


        Fragment fragmentStopped = new StoppedFragment();
        bundle = new Bundle();
        bundle.putSerializable("list", stopped);
        fragmentStopped.setArguments(bundle);
        adapter.addFrag(fragmentStopped, "S=" + data.size());


        Fragment fragmentOffline = new OfflineFragment();
        bundle = new Bundle();
        bundle.putSerializable("list", offline);
        fragmentOffline.setArguments(bundle);
        adapter.addFrag(fragmentOffline, "O=" + data.size());


        viewPager.setAdapter(adapter);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {

            confirmDialog();
        }
    }

    private void confirmDialog() {
        new AlertDialog.Builder(this)
                .setMessage("Are you sure you want to exit?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        MainActivity.super.onBackPressed();
                    }
                })
                .setNegativeButton("No", null)
                .show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_item_one) {
            onSearchRequested();
            return true;
        }
        if (id == R.id.action_item_two) {
            saveStateAndCallApi();
            isSearchRequested = false;
        }

        return super.onOptionsItemSelected(item);
    }

    private void saveStateAndCallApi() {
        selectedTabPosition = tabLayout.getSelectedTabPosition();
        callVehicleApiFirstTime();
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_vehicle) {
            // Handle the camera action
            Intent intent = new Intent(context, MapsClusterActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_distance) {

            Intent intent = new Intent(context, DistanceActivity.class);
            startActivity(intent);


        } else if (id == R.id.nav_summary) {

            Intent intent = new Intent(context, SummaryActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_replay) {
            Intent intent = new Intent(context, TimeSelctionActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_settings) {
            Intent intent = new Intent(context, SettingActivity.class);
            startActivity(intent);


        } else if (id == R.id.nav_expiry) {

            Intent intent = new Intent(context, ExpiryActivity.class);
            startActivity(intent);


        } else if (id == R.id.nav_engine) {

            Intent intent = new Intent(context, EngineActivity.class);
            startActivity(intent);


        } else if (id == R.id.nav_ac) {

            Intent intent = new Intent(context, AcActivity.class);
            startActivity(intent);


        } else if (id == R.id.nav_idle) {

            Intent intent = new Intent(context, IdleActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_event) {

            Intent intent = new Intent(context, EventsActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_overSpeed) {

            Intent intent = new Intent(context, OverSpeedActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_logout) {

            deviceTokenApiOff();

        } else if (id == R.id.nav_stop) {
            Intent intent = new Intent(context, StopActivity.class);
            startActivity(intent);


        } else if (id == R.id.nav_move) {
            Intent intent = new Intent(context, MovementActivity.class);
            startActivity(intent);


        } else if (id == R.id.nav_ignition) {
            Intent intent = new Intent(context, IgnitionActivity.class);
            startActivity(intent);


        } else if (id == R.id.nav_service) {
            Intent intent = new Intent(context, ServiceActivity.class);
            startActivity(intent);


        } else if (id == R.id.nav_help) {

            infoDialog();


        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void callLogoutApi() {

        if (NetworkUtil.isOnline(context)) {

            HashMap<String, String> params = new HashMap<>();
            params.put(getString(R.string.api_request_param_function_name), "notify");
            params.put(getString(R.string.api_request_param_firebase_token), SharedPreferenceUtil.getString(Constants.SP_FIREBASE_TOKEN, ""));


            new HttpRequestSingleton(context, getString(R.string.api_token_api), params, Constants.API_SIGN_IN, new HttpCallback() {
                @Override
                public void onResponse(String response, int action) {
                    Log.i("logout", "" + response.toString());


                }


                @Override
                public void onError(VolleyError error, int action) {

                }
            });
        } else {
            Toast.makeText(context, getString(R.string.internet_not_available), Toast.LENGTH_SHORT);

        }
    }

    private void deviceTokenApiOff() {
        if (NetworkUtil.isOnline(context)) {

            HashMap<String, String> params = new HashMap<>();

            params.put("firebase_token", SharedPreferenceUtil.getString(Constants.SP_FIREBASE_TOKEN, ""));
            params.put("function_name", "notify");


            new HttpRequestSingletonPost(context, getString(R.string.api_token_api), params, Constants.API_TOKEN, new HttpCallback() {
                @Override
                public void onResponse(String response, int action) {

                    Log.i("response", response.toString());
                    doLogoutProcess();

                }


                @Override
                public void onError(VolleyError error, int action) {

                }
            });
        } else {
            Toast.makeText(context, getString(R.string.internet_not_available), Toast.LENGTH_SHORT);

        }

    }

    private void infoDialog() {

        final Dialog dialog = new Dialog(context);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        dialog.setContentView(R.layout.popup);

        ImageView imgClose = (ImageView) dialog.findViewById(R.id.imgClose);
        final TextView textViewemail = (TextView) dialog.findViewById(R.id.textViewemail);
        final TextView textViewContactNo = (TextView) dialog.findViewById(R.id.textViewContactNo);
        final TextView textViewCompanyName = (TextView) dialog.findViewById(R.id.textViewCompanyName);

//        txtInfo.setText(Html.fromHtml(getString(R.string.intro_text)));

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                dialog.cancel();
            }
        });


        dialog.show();
        if (NetworkUtil.isOnline(context)) {

            HashMap<String, String> params = new HashMap<>();
            params.put(getString(R.string.api_request_param_username), SharedPreferenceUtil.getString(Constants.SP_USERNAME, ""));
//            params.put(getString(R.string.api_request_param_firebase_token), SharedPreferenceUtil.getString(Constants.SP_FIREBASE_TOKEN, ""));


            new HttpRequestSingleton(context, getString(R.string.api_base_url) + getString(R.string.api_contact), params, Constants.API_SIGN_IN, new HttpCallback() {
                @Override
                public void onResponse(String response, int action) {

                    ObjectMapper mapper = new ObjectMapper();
                    try {
                        HelpResponse helpResponse = mapper.readValue(response, HelpResponse.class);
                        textViewCompanyName.setText(helpResponse.getData().get1().getCompany());
                        textViewContactNo.setText(helpResponse.getData().get2().getMobile());
                        textViewemail.setText(helpResponse.getData().get3().getEmail());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }


                @Override
                public void onError(VolleyError error, int action) {

                }
            });
        } else {
            Toast.makeText(context, getString(R.string.internet_not_available), Toast.LENGTH_SHORT);

        }

    }

    private View.OnClickListener cancel_click = new View.OnClickListener() {
        public void onClick(View v) {
            pw.dismiss();

        }
    };

    private void doLogoutProcess() {
        String token = SharedPreferenceUtil.getString(Constants.SP_FIREBASE_TOKEN, "");
        SharedPreferenceUtil.clear();
        SharedPreferenceUtil.save();
        SharedPreferenceUtil.putValue(Constants.SP_FIREBASE_TOKEN, token);
        SharedPreferenceUtil.save();
        Intent intent = new Intent(context, LoginActivity.class);
        if (Build.VERSION.SDK_INT >= 11) {
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        } else {
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        }
        startActivity(intent);
    }

    private void callVehicleApiFirstTime() {
        if (NetworkUtil.isOnline(context)) {
            HashMap<String, String> params = new HashMap<>();
            params.put("key", SharedPreferenceUtil.getString(Constants.SP_USER_KEY, ""));
            params.put("cmd", "ALL,*");
            new HttpRequestSingleton(context, getString(R.string.api_base_url) + getString(R.string.api_vehicle_api), params, 100, new HttpCallback() {
                @Override
                public void onResponse(String response, int action) {
                    if (response != null) {
                        Log.i("response", "" + response.toString());
                        switch (action) {
                            case 100:
                                ObjectMapper mapper = new ObjectMapper();
                                try {
                                    VehicleResponse vehicleResponse = mapper.readValue(response, VehicleResponse.class);
                                    sortResponse(vehicleResponse.getData());


                                } catch (JsonProcessingException e) {
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }

                                break;
                            default:
                        }

                    } else {
//                        Toast.makeText(context, "Something Went Wrong", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onError(VolleyError error, int action) {

                }
            });

        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_LONG).show();
        }
    }

    private void sortResponse(List<Datum> allList) {
        List<Datum> list = new ArrayList<>();
        selectedTabPosition = tabLayout.getSelectedTabPosition();

        list.addAll(allList);
        applyFilterByStatus(list);
        tabLayout.setupWithViewPager(viewPager);
        createViewPager(viewPager, list);
        createTabIcons(list);
        if (tabLayout.getTabAt(selectedTabPosition) != null)
            tabLayout.getTabAt(selectedTabPosition).select();

        swiperefresh.setRefreshing(false);


    }

    private void applyFilterByStatus(List<Datum> list) {
        stopped.clear();
        ideal.clear();
        offline.clear();
        running.clear();
        for (Datum vehicle : list
        ) {
            switch (vehicle.getStatus()) {
                case "running":

                    running.add(vehicle);
                    break;

                case "offline":

                    offline.add(vehicle);
                    break;
                case "idle":

                    ideal.add(vehicle);
                    break;
                case "stopped":

                    stopped.add(vehicle);
                    break;

            }


        }


    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem itemMenu = menu.findItem(R.id.action_item_one);
        itemMenu.setVisible(true);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        MenuItemCompat.setOnActionExpandListener(itemMenu, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {

                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                return true;
            }
        });

        final androidx.appcompat.widget.SearchView searchView = (androidx.appcompat.widget.SearchView) itemMenu.getActionView();

        EditText txtSearch = ((EditText) searchView.findViewById(R.id.search_src_text));


        txtSearch.setTextColor(getResources().getColor(R.color.white));

        searchManager.setOnCancelListener(new SearchManager.OnCancelListener() {
            @Override
            public void onCancel() {
                isSearchRequested = false;
                callVehicleApiFirstTime();
            }
        });
        searchView.setOnQueryTextListener(new androidx.appcompat.widget.SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                query = query.toLowerCase().trim();
                selectedTabPosition = tabLayout.getSelectedTabPosition();
                searchList.clear();
                for (Datum vehicle : All
                ) {
                    if (vehicle.getName().toLowerCase().contains(query)) {
                        searchList.add(vehicle);
                    }

                }
                isSearchRequested = true;
                sortResponse(searchList);


                return true;


            }

            @Override
            public boolean onQueryTextChange(String newText) {


                if (newText.equalsIgnoreCase("")) {
                    isSearchRequested = false;
                    callVehicleApiFirstTime();

                } else {
                    newText = newText.toLowerCase().trim();
                    selectedTabPosition = tabLayout.getSelectedTabPosition();
                    searchList.clear();
                    for (Datum vehicle : All
                    ) {
                        if (vehicle.getName().toLowerCase().contains(newText)) {
                            searchList.add(vehicle);
                        }

                    }
                    isSearchRequested = true;
                    sortResponse(searchList);


                }

                return true;


            }


        });
        return super.onPrepareOptionsMenu(menu);
    }


    @Override
    protected void onResume() {
        super.onResume();
        startTimer(TIME_INTERVAL);
        if (!isSearchRequested)
            callVehicleApiFirstTime();
    }

    @Override
    protected void onPause() {
        super.onPause();
        isStarted = false;
        handler.removeCallbacks(runnable);
    }
}
