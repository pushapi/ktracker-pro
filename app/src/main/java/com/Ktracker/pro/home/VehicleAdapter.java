package com.Ktracker.pro.home;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import com.Ktracker.pro.Constants;
import com.Ktracker.pro.MapsActivity;
import com.Ktracker.pro.R;
import com.Ktracker.pro.Utill;
import com.Ktracker.pro.response.vehicle.Datum;
import com.Ktracker.pro.storage.SharedPreferenceUtil;

public class VehicleAdapter extends RecyclerView.Adapter<VehicleAdapter.ViewHolder> {
    Context context;
    List<Datum> itemList;
    MainActivity activity;


    public VehicleAdapter(Context context, List<Datum> itemList) {
        this.context = context;
        this.itemList = itemList;
        activity = (MainActivity) context;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_vehicle_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {

        final Datum item = itemList.get(position);
        holder.textAdress.setText(item.getAddress());
        holder.offline.setText(item.getDtTracker());
        holder.textName.setText(item.getName());
        holder.textSpeed.setText(item.getSpeed() + " km/h");
        if (item.getStatus().equalsIgnoreCase("offline")) {
            holder.imgLoc.setImageResource(R.drawable.loc_valid_0);
        } else if (item.getLocValid().equalsIgnoreCase("1")) {

            holder.imgLoc.setImageResource(R.drawable.loc_valid_1);
        } else {
            holder.imgLoc.setImageResource(R.drawable.loc_valid_1);

        }

        if(item.getStatus().equalsIgnoreCase("idle"))
        {
            holder.imgIgnition.setImageResource(R.drawable.ignition_idle);
        }
        else

        if (item.getIgnition().equalsIgnoreCase("on")) {
            holder.imgIgnition.setImageResource(R.drawable.ignition_on);
        } else if (item.getIgnition().equalsIgnoreCase("idle")) {
            holder.imgIgnition.setImageResource(R.drawable.ignition_idle);

        } else {
            holder.imgIgnition.setImageResource(R.drawable.ignition_off);

        }

        if (item.getSupply().equalsIgnoreCase("on")) {
            holder.imgSupply.setImageResource(R.drawable.supply_on);
        } else if (item.getSupply().equalsIgnoreCase("off")) {

            holder.imgSupply.setImageResource(R.drawable.supply_off);
        } else {
            holder.imgSupply.setImageResource(R.drawable.supply_null);


        }

        if (item.getAc().equalsIgnoreCase("on")) {
            holder.imgAc.setImageResource(R.drawable.ac_on);
        } else if (item.getAc().equalsIgnoreCase("off")) {

            holder.imgAc.setImageResource(R.drawable.ac_off);
        } else {
            holder.imgAc.setImageResource(R.drawable.ac_null);


        }
        holder.imgcar.setImageResource(Utill.getIconAndColor(item.getIcon(),item.getStatus()));

        //DEVICE EXPIRED

        if(item.getActive().equalsIgnoreCase("false") )
        {
            holder.expired.setVisibility(View.VISIBLE);
            holder.toExpired.setVisibility(View.GONE);
            if(item.getNotice().equalsIgnoreCase("true"))
            holder.txtExpired.setText(item.getMessage());
            holder.card_view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(context,item.getMessage(),Toast.LENGTH_SHORT).show();
                }
            });
        }
        //ABOUT TO EXPIRE

        else if(item.getActive().equalsIgnoreCase("true") && item.getNotice().equalsIgnoreCase("true"))
        {
            holder.expired.setVisibility(View.GONE);
            holder.toExpired.setVisibility(View.VISIBLE);
            holder.txtToExpired.setText(item.getMessage());
            holder.card_view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SharedPreferenceUtil.putValue(Constants.SP_IMEI, item.getImei());
                    SharedPreferenceUtil.save();
                    Intent intent = new Intent(context, MapsActivity.class);
                    context.startActivity(intent);
                }
            });

            holder.imgCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.toExpired.setVisibility(View.GONE);
                }
            });
        }
        else
        {
            holder.expired.setVisibility(View.GONE);
            holder.toExpired.setVisibility(View.GONE);
            holder.card_view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SharedPreferenceUtil.putValue(Constants.SP_IMEI, item.getImei());
                    SharedPreferenceUtil.save();
                    Intent intent = new Intent(context, MapsActivity.class);
                    context.startActivity(intent);
                }
            });
        }




    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private CardView card_view;
        private TextView textAdress, textName, textSpeed, offline,txtExpired,txtToExpired;
        private RelativeLayout expired,toExpired;
        private ImageView imgSupply, imgIgnition, imgLoc, imgcar,imgAc,imgCancel;

        public ViewHolder(View itemView) {
            super(itemView);

            card_view = (CardView) itemView.findViewById(R.id.card_view);
            textAdress = (TextView) itemView.findViewById(R.id.textAdress);
            txtToExpired = (TextView) itemView.findViewById(R.id.txtToExpired);
            textName = (TextView) itemView.findViewById(R.id.textName);
            textSpeed = (TextView) itemView.findViewById(R.id.textSpeed);
            offline = (TextView) itemView.findViewById(R.id.offline);
            txtExpired = (TextView) itemView.findViewById(R.id.txtExpired);
            imgSupply = (ImageView) itemView.findViewById(R.id.imgSupply);
            imgIgnition = (ImageView) itemView.findViewById(R.id.imgIgnition);
            imgLoc = (ImageView) itemView.findViewById(R.id.imgLoc);
            imgcar = (ImageView) itemView.findViewById(R.id.imgcar);
            imgAc = (ImageView) itemView.findViewById(R.id.imgAc);
            imgCancel = (ImageView) itemView.findViewById(R.id.imgCancel);
            expired = (RelativeLayout) itemView.findViewById(R.id.expired);
            toExpired = (RelativeLayout) itemView.findViewById(R.id.toExpired);

        }

    }
}
