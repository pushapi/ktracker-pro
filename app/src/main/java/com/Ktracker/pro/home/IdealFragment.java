package com.Ktracker.pro.home;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import com.Ktracker.pro.R;
import com.Ktracker.pro.response.vehicle.Datum;

public class IdealFragment extends Fragment {

    private RecyclerView recyclerViewList;
    private LinearLayoutManager linearLayoutManager;
    private VehicleAdapter childAdapter;
    private Context context;
    private ArrayList<Datum> list;

    public IdealFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = this.getArguments();
        list = (ArrayList<Datum>) bundle.getSerializable("list");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        context = getContext();

        View view = inflater.inflate(R.layout.fragment_total, container, false);
        recyclerViewList = (RecyclerView) view.findViewById(R.id.recyclerViewList);
        setAdapter(populateNewsList(list));

        // Inflate the layout for this fragment
        return view;
    }

    private void setAdapter(List<Datum> childList) {
        linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        recyclerViewList.setLayoutManager(linearLayoutManager);
        recyclerViewList.setItemAnimator(new DefaultItemAnimator());
        childAdapter = new VehicleAdapter(context, childList);
        recyclerViewList.setAdapter(childAdapter);
    }

    private List<Datum> populateNewsList(ArrayList<Datum> list) {


        return list;

    }
}
